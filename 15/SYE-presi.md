---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Projektpräsentationen

+++

HIER WERDEN DIE PROJEKTERGEBNISSE IN EINER TABELLE VERLINKT, SO DASS DIE PRÄSENTATION FLÜSSIG ABLAUFEN KANN - DIESE FINDEN SICH IM ORDNER `/teach/pr/` (nicht synchronisiert auf gitlab).

+++

Die Präsentationen finden an den in ilias angemeldeten Terminen statt.

+++

## Daten aussagekräftig darstellen

+++

Dazu finden Sie Tipps in LINK EINFÜGEN - UND ALLES ANDERE HIER NACH DORT VERSCHIEBEN

+++

Mit python lassen sich sehr viele unterschiedliche Arten von Diagrammen erstellen. Für die Präsentation kommt es darauf an, die Daten und Ergebnisse so zu präsentieren, dass es leicht verständlich wird. Hierzu ein paar Tipps im Entwurf des jupyter Notebook nb/SYE-Diagramme.ipynb LINK AKTUALISIEREN UND IM ANHANG ANZEIGEN.

+++

- [ ] noch ergänzen im SYE-Diagramme: {cite:p}`university_of_queensland_creating_2021`

+++

- [ ] TODO Link zu Entwurf Beispiel-Datenanalyse verschieben in python-Intro Beispiel_Datenanalyse VERLINKEN Beispiel_Datenanalyse.ipynb
- [ ] TODO Link zu Entwurf Datenanalyse pandas verschieben in python-Intro Datenanalyse_pandas VERLINKEN Datenanalyse_pandas.ipynb
- [ ] TODO Link zu Entwurf hochaufgelöste Daten auswerten verschieben in python-Intro Hochaufgeloeste_Daten_downsamplen.ipynb VERLINKEN
