---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Daten aus Projekten öffentlich zur Verfügung stellen

+++

In jedem Jahr messen Studierende elektrische Verbrauchsprofile. Diese können auch für andere Studien relevant sein. Sie haben nun selbst auch von open data profitieren können, als Sie Ihre Energiedatenanalysen durchgeführt haben. Nun können Sie der Gemeinschaft etwas zurückgeben, indem Sie Ihre eigenen Daten auch zur Verfügung stellen.

+++

Folgende Möglichkeiten empfehle ich:

+++

1. Sie legen sich ein eigenes github oder gitlab repository an und laden die Daten als csv-Datei hoch und ergänzen als .md Datei alle Metadaten, die Sie haben: 
 - Art und Alter des Geräts
 - Typenschild
 - Seriennummer
 - Bezeichnung
 - Marke
 - Art des Betriebs (z. B. welches Programm mit welcher Füllung in einer Waschmaschine lief)
 - sonstige Auffälligkeiten (z. B. wenn schon Beschädigungen sichtbar waren)
 - wenn Sie das wollen, geben Sie auch Ihren Namen an und die gewünschte Zitierweise für die Daten
2. Sie laden die Daten auf zenodo oder figshare hoch und gehen wie oben vor.
3. Sie laden die Daten auf der open energy platform hoch. Bisher existiert dazu meines Wissens noch keine Anleitung. Falls Sie eine finden, lassen Sie es mich wissen, damit ich das hier ergänzen kann.
4. Sie finden ein anderes repository für solche Daten und lassen mich davon wissen, gerne verlinke ich das ggf. auch hier.

+++

Bitte geben Sie mir auch den Link zu Ihren veröffentlichten Daten. Ich pflege den dann in meine Sammlung HIER LINK EINFÜGEN ein.

+++

Das Zurverfügungstellen der Daten ist freiwillig und beeinflusst nicht Ihre Note.
