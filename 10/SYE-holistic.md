---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Kritik und Politik

+++

Ein einseitiger Fokus auf Energieeffizienz lässt außen vor, dass für unseren Planeten auch der Ressourcenverbrauch problematisch ist. Ebenfalls sind lokale Optimierungen, die dann an anderer Stelle höhere Energiebedarfe verursachen, nicht im Sinne der Klimafreundlichkeit.

+++

Dies effektiv zu adressieren ist häufig Sache der Politik.

+++

In diesem Kapitel geht es daher um
- Lebenszyklusbetrachtungen aus ökologischer und ökonomischer Perspektive
- globale vs. lokale Optimierungen
- Ebenen der Energieeffizienz
- politische Maßnahmen und Ergebnisse

Außerdem enthält dieses Kapitel erste Informationen zur Organisation der mündlichen Prüfung, da es so weit am Ende des Semesters steht, dass es wichtig ist, das zu thematisieren.

+++

## Ebenen der Energieeffizienz

+++

### Systemgrenzen: regional, lokal, geräteintern, etc.

```{code-cell} ipython3
try: 
    print('Bereits heruntergeladen')
    f = open("../data/ENTSOEdata.xlsx")
except FileNotFoundError:#IOError:
    print('Herunterladen (dauert einen Moment)')
    url = 'https://eepublicdownloads.blob.core.windows.net/public-cdn-container/clean-documents/Publications/Statistics/MHLV_data-2015-2017.xlsx'
    r = requests.get(url, allow_redirects=True)
    open('../data/ENTSOEdata.xlsx','wb').write(r.content)
ENTSOEDE2015bis2017 = pd.read_excel('../data/ENTSOEdata.xlsx',sheet_name='2015-2017')
ENTSOEDE2018bis2019 = pd.read_excel('../data/ENTSOEdata.xlsx',sheet_name='2018-2019')
ENTSOEDE = pd.concat([ENTSOEDE2015bis2017,ENTSOEDE2018bis2019],ignore_index=True)
ENTSOEDE.plot(x = 'DateUTC', y = 'Value') # in GW
```

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Was kann man in dieser Darstellung erkennen? Was lässt sich hier schließen? Was ist plausibel? Wie könnte man die Darstellung verbessern?
</div>

+++

*Antwort:...*

+++

### Ebenen bezogen auf die Energieversorgung und den Verbrauch

+++

- [ ] TODO Ebenen der Energieeffizienz ../img/ebenen-energieeffizienz.svg

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Diskutieren Sie: Warum gibt es für den Verbrauch verschiedene Sichtweisen?
</div>

+++

*Antwort:...*

+++

## Rebound-Effekt

+++

<a title="The original uploader was Radeksz at English Wikipedia., Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Jevons.jpeg"><img width="256" alt="Jevons" src="https://upload.wikimedia.org/wikipedia/commons/f/f8/Jevons.jpeg"></a>

+++

1865 William Stanley Jevons: durch die Einführung der Wattschen Dampfmaschine (dreimal effizienter als Vorgängermaschine) deutlicher Anstieg des britischen Kohleverbrauchs, es wurden dreimal so viele Maschinen eingesetzt! \cite{martin_pehnt_energieeffizienz_2010}

+++

**Direkter Rebound**: effizienter angebotene Dienstleistung wird billiger und dadurch stärker nachgefragt

+++

**Indirekter Rebound**: erhöhte Nachfrage nach anderen Gütern

+++

**Backfire**: wenn durch Effizienz Energiebedarf erhöht wird

+++

**Höhe**: ca. 0 .. 30%

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Sammeln Sie mindestens 5 Beispiele für Rebound-Effekte. Überlegen Sie auch mindestens 2, die für Ihre Projekte relevant sein können. Bewerten Sie später Ihre Energieeffizienzmaßnahmen daraufhin, ob Rebound-Effekte eine Rolle spielen könnten.
</div>

+++

*Antwort: ...*

+++

### Moralisches Lizensieren

+++

\cite{judith_braun_wie_2021} zeigt auf, dass Menschen dazu tendieren, gute Taten aufzurechnen, z. B.: _Wenn ich Wasser spare, darf ich Strom verschwenden_ \cite{tiefenbeck_for_2013} - diejenigen Nutzer, die wöchentlich über ihren Wasserverbrauch informiert wurden, sparten 6,0% ein - und erhöhten ihren Stromverbrauch um 5,6%.

+++

## Energieeffizienz ist nicht alles - Vorsicht vor einseitigem Fokus!

+++

| ![woher Strom?](../img/DSC_0154.JPG) | ![erneuerbare Flächen begrenzt](../img/DSC_0091.JPG) | ![überhaupt nötig?](../img/DSC_0155.JPG) |
|-|-|-|
| effiziente Produkte nicht zwangsläufig nachhaltiger (woher kommt der Strom hier?) | auch erneuerbare Energie ist nicht unbegrenzt verfügbar (Fläche begrenzt, nachts, ...) | Suffizienz: "Wendet dieses System eine Not?" |

+++

## Was bringen die Energieeffizienz-Bemühungen der EU?

+++

{cite:p}``marie_rousselot_energy_2018`` [](#cite-marie_rousselot_energy_2018) zeigt auf:
- 2016 konsumieren Gebäude 41% oder 458 Mtoe der europäischen Endenergie (EU-28)
- Zwei Drittel davon sind für Wohngebäude
- marginale Änderungsraten zwischen -0,6%/Jahr und +0,7%/Jahr, seit 2008 stetig fallend (mehr Haushalte, mehr Ein-Personen-Haushalte, mehr Geräte in größeren Haushalten konterkariert Energieeffizienz-Bemühungen)
- durchschnittlich 200 kWh/m²
 - Dienstleistungsgebäude 300 kWh/m²
 - Wohngebäude 170 kWh/m²
 - In Spanien 25% weniger als der EU Durchschnitt
 - In Schweden 5% mehr als der EU Durchschnitt
- Haushalte verbesserten sich seit 2000 um durchschnittlich 28% (effizientere Heizungen und Gebäude)

+++

{cite:p}``marie_rousselot_energy_2018`` begründet:
- effizientere Heizsysteme
 - Italien 75% Wärmepumpen in 2015, in Schweden und Finnland über 25%
 - Niederlande über 80% Brennwertthermen, UK über 40%
 - Pelletheizungen in Italien 11,5% der Haushalte und Österreich 5,1%
- Niedrigenergiegebäude v.a. relevant für Neubauten, daher begrenzt
- Energieverbrauch großer Haushaltsgeräte sank um 1,2% / Jahr seit 2000
- allerdings starker Anstieg spezifischer Verbräuche kleiner Geräte um 3%/Jahr zwischen 2000 und 2007, danach stabiler
- in 2015 zeigen Kleingeräte den höchsten Anteil am Verbrauch (60%)
- Großgeräte: u.a. Kühlschränke, Gefriergeräte, Waschmaschinen, Geschirrspüler, Trockner, Fernseher, wobei Kühlgeräte und Spülmaschinen fast 60% des Stromverbrauchs der Großgeräte verursachen
- Ökodesign hat Effizienzverbesserungen von über 35% bei Kühlgeräten, Wasch- und Spülmaschinen gebracht, bei Trocknern etwa 15%

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Ermitteln Sie, mit dem <a href="http://www.indicators.odyssee-mure.eu/decomposition.html">Werkzeug von Odyssee</a>, welche Effekte sich positiv und negativ auf die Energieeffizienz von Großgeräten auswirken.
</div>

+++

*Antwort:...*

+++

## Reicht Energieeffizienz für nachhaltiges Ökodesign?

+++

Normungsgremien beginnen nun auch damit, **Materialeffizienz** bei Produkten zu betrachten. 

+++

Was ist Materialeffizienz?

+++

[wikipedia-Eintrag zu Materialeffizienz:](https://de.wikipedia.org/wiki/Materialeffizienz) {cite:p}``wikipedia_materialeffizienz_2019`` <br><br>
*Die Materialeffizienz stellt das Verhältnis der hergestellten Produkte zur Menge der eingesetzten Materialien dar.*
\begin{equation}
\text{Materialeffizienz} = \frac{\text{Produktoutput}}{\text{Materialinput}}   
\end{equation}

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Wie hängen Materialeffizienz und Energieeffizienz zusammen?
</div>

+++

*Antwort:...*

+++

## Label sind international unterschiedlich

+++

Wirtschaftspolitik

+++

## Relevante Normen für Materialeffizienz

+++

- durability $\to$ EN45552 general methods for the assessment of the durability of energy-related products
 - largely product specific
- repairability / reuse / upgrade $\to$ EN 45554 general methods for the assessment of the ability to repair, reuse and upgrade energy-related products
 - priority parts?
 - common parts for appliances?
 - delete personal data from products?
- recyclates $\to$ EN 45557 recycled content, JTC10 standard
 - separate pre consumer material from post consumer material?
 - how differentiate both?
- recyclability $\to$ EN 45555, EN50625 end of life treatment scenarios, grouped according to WEEE categories (data from WEEE monitoring)
- reliability
- critical raw materials
- remanufacturing

+++

## Politischer Rahmen

+++

Roadmap "New Circular Action Plan" (CEAP) as a pillar of the **European Green Deal**

+++

![Ebenen der zirkulären wertschöpfung nach SRU](../img/ebenen-zirkulaerewertschoepfung-nach-sru.svg)

+++

Grafik adaptiert nach den Empfehlungen des deutschen Sachverständigenrats für Umweltfragen {cite:p}``kristine_sperlich_open_2020``

+++

# Literatur

+++

```{bibliography}
:filter: docname in docnames
```
