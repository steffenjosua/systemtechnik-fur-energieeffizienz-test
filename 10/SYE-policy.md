---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Politische Maßnahmen für mehr Energieeffizienz

+++

**AUFGABE**
Welche Zielkonflikte treten bei der Optimierung der Energieeffizienz auf? Sammeln Sie Beispiele in Ihren Projektteams.

+++

*Antwort:*

+++

**Weiterführender Tipp (margin)**

Das europäische Projekt [Odyssee-Mure](https://www.odyssee-mure.eu/) erstellt eine Datenbank mit politischen Energieeffizienz-Maßnahmen in Europa und bietet aktuelle Webinare an, die Energieeffizienz-Indikatoren präsentieren.

+++

Der folgende Code ist inspiriert von [einem python excel Tutorial](https://pythonbasics.org/read-excel/) {cite:p}`pythonbasicsorg_read_2020`

+++

### Weniger $CO_2$ emittieren!

```{code-cell} ipython3
import pandas as pd
import matplotlib.pyplot as plt
# Daten einlesen aus Internetquelle
EUenergydata2020 = pd.read_excel('https://ec.europa.eu/energy/sites/ener/files/energy_statistical_countrydatasheets.xlsx',sheet_name=2)
EUenergydata2020.columns = EUenergydata2020.iloc[6] # Jahreszahlen als Spaltentitel
EUenergydata2020GDP2015 = EUenergydata2020.iloc[540,3:] # Bruttoinlandsprodukt
EUenergydata2020GHG = EUenergydata2020.iloc[495,3:] # Treibhausgase
EUenergydata2020Mtoe = EUenergydata2020.iloc[403,3:] # Energieverbrauch
fig = plt.figure()
ax = plt.subplot(111)
ax.plot(1e-3*EUenergydata2020GDP2015,label='Bruttoinlandsprodukt 2015 Marktwert [Bio. €]')
ax.plot(1e-3*EUenergydata2020GHG,label='Treibhausgasemissionen [Mia. t $CO_2$]')
ax.plot(0.011630*EUenergydata2020Mtoe,label='Energieverbrauch [PWh]')
#ax.title('Motivation Klimaschutz: weniger $CO_2$!')
ax.text(2020,5,'Quelle: EU Energy in Figures Statistical Pocketbook 2020',color='gray')
ax.axis([1990,2018,0,25])
box = ax.get_position() # um Legende außerhalb zu machen, einige Tricks
ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
ax.legend(loc='center left',bbox_to_anchor=(1,0.5))
```

### Beispiel für die Umsetzung von Umwelt- und Klimaschutz an der TH Köln: Beschaffungsrichtlinien

+++

![THvorschriftenergieeffizienz](../img/THvorschriftenergieeffizienz.png)

+++

### Energie sparen bedeutet (nicht immer) Kosten sparen

+++

Beispiel: [Energiespartipps für das HomeOffice von der Verbraucherzentrale](https://verbraucherzentrale-energieberatung.de/energie-sparen/energie-sparen-im-homeoffice/) {cite:p}`verbraucherzentrale_energie_2020`

+++

![Deutschlands Energieproduktivität steigt](http://wiki.energytransition.org/files/2018/10/BL_ET_update_2018_Germany-continues-to-produce-more-GDP-with-less-energy-.png)

+++

### Nationaler Aktionsplan Energieeffizienz

+++

die [*Elektrische Energieeffizienz Deutsche Normungs-Roadmap Version 2*, VDE 2018](https://www.dke.de/resource/blob/950182/9d6b9b67cf0c967fb1e7d9e417c009e0/nr-energieeffizienz-v2-de-data.pdf) {cite:p}`vde_verband_der_elektrotechnik_elektronik_informationstechnik_e_v_als_trager_der_elektronik_informationstechnik_in_din_und_vde_elektrische_2018` fasst zusammen:
- Abhängigkeit von Energieimporten vermindern
- statt Verzicht intelligenter und sparsamer mit Energie umgehen
- bis 2050
 - Stromverbrauch um 10-25% (bezogen auf 2008) senken
 - Verkehrsenergieverbrauch um 40% (bezogen auf 2005) senken
 - Gebäude: Primärenergiebedarf um 80% bis 2050 senken

+++

### Potenziale im Gebäudesektor

+++

![sehr große Potenziale im Gebäudesektor!](https://wiki.energytransition.org/files/2018/10/BL_ET_update_2018_The-housing-sector-offers-large-potential-for-energy-savings--1024x787.png)

+++

### Kraft-Wärme-Kopplung - am besten erneuerbar!

+++

![Potenziale der Kraft-Wärme-Kopplung](https://wiki.energytransition.org/files/2019/01/BL_ET_update_2018_Why-cogeneration-is-more-efficient-than-conventional-coal-power-plants--1024x787.png)

+++

### Standby-Verbrauch und Ökodesign

+++

die [*Elektrische Energieeffizienz Deutsche Normungs-Roadmap Version 2*, VDE 2018](https://www.dke.de/resource/blob/950182/9d6b9b67cf0c967fb1e7d9e417c009e0/nr-energieeffizienz-v2-de-data.pdf) {cite:p}`vde_verband_der_elektrotechnik_elektronik_informationstechnik_e_v_als_trager_der_elektronik_informationstechnik_in_din_und_vde_elektrische_2018` nennt die **Ökodesign-Richtlinie** als Mittel gegen Standby-Verbräuche:
- zwischen 2004 und 2006 ca. 4% des Bruttostromverbrauchs
- Ökodesign-Richtlinie (Verordnung 1275/2008) begrenzt Standby auf 0,5 W bzw. 1 W
- allerdings gilt die Richtlinie nicht für alle Verbraucher

+++

### Energieeffizienz in Europa

+++

die [*Elektrische Energieeffizienz Deutsche Normungs-Roadmap Version 2*, VDE 2018](https://www.dke.de/resource/blob/950182/9d6b9b67cf0c967fb1e7d9e417c009e0/nr-energieeffizienz-v2-de-data.pdf) {cite:p}`vde_verband_der_elektrotechnik_elektronik_informationstechnik_e_v_als_trager_der_elektronik_informationstechnik_in_din_und_vde_elektrische_2018` nennt folgende
<br><br>
Rechtsakte:
- Ökodesign
- Energieverbrauchskennzeichnung
- Gebäudebereich
<br><br>
Richtlinien:
- Richtlinie EDL-RL 2006: Endenergieverbrauch in EU reduzieren und Markt für Energiedienstleistungen fördern
- Energieeffizienzrichtlinie 2012:
 - jährliche Einsparverpflichtung 1,5% bzw. alternativ politische Maßnahmen mit gleicher Wirkung
 - jährliche Sanierungsverpflichtung 3% der Bundesgebäude
 - Einführung indikativer nationaler Energieeffizienzziele
 - Förderung / Verpflichtung zu Energieaudits, Verbrauchsmessung und -abrechnung, Kraft-Wärme-Kopplung, u.v.m.

```{code-cell} ipython3

```
