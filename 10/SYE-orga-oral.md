---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Organisation der mündlichen Prüfung

+++

Die mündliche Prüfung zählt, wie schon im ersten Kapitel erklärt, 50% der Note. Wenn das Projekt nicht bestanden ist oder die mündliche Prüfung nicht bestanden ist, dann ist das gesamte Modul nicht bestanden.

+++

Bei Nicht-Erscheinen zum regulären Termin im Wintersemester ist der nächstmögliche Prüfungstermin im Prüfungszeitraum im Sommersemester. 

+++

Eine bestandene Projektnote bleibt bestehen. Bitte beachten Sie jedoch, dass sich die Projektthemen in jedem Jahr ändern. Auch die Kursunterlagen entwickeln sich weiter. Daher ändern sich die Fragen in der Prüfung zu jedem Wintersemester. Ich empfehle daher stark, die mündliche Prüfung im selben Semester wahrzunehmen wie das Projekt.

+++

In der mündlichen Prüfung werden auch Diagramme und Darstellungen zu den Projekten gefragt werden. Es lohnt sich also doppelt, bei den Projektpräsentationen aufzupassen. 

+++

Den Ablauf der mündlichen Prüfung finden Sie im Notebook `SYE-oral-exam.ipynb` im Ordner `teach`. 

+++

Um sich für einen konkreten Termin für die mündliche Prüfung anzumelden, nutzen Sie bitte in ILIAS das Sprechstundenwerkzeug:
1. In ILIAS in den Kurs SYE gehen.
2. Dort unterhalb vom Minikalender auf _Sprechstunde für Prof. May_ klicken.
3. Dort einen Termin mit `SYE` und `Prüfung` im Namen auswählen und buchen.
4. Buchung überprüfen: Automatisch ist ein Ort oder ein zoom-Link im Termin eingetragen. Falls nicht, haben Sie in Ihrem privaten ILIAS Kalender gebucht, den ich nicht sehen kann. Dann bitte nochmal probieren.
5. Falls es so aussieht, dass Sie nicht buchen können: 
 - entweder ist der Termin schon von jemand anderem reserviert
 - oder Sie sind so spät dran, dass die Buchungsfrist abgelaufen ist
 - dann bitte das nächste freie Zeitfenster buchen
 - oder Sie versuchen gerade, einen zweiten Prüfungstermin zu reservieren
 - dann bitte erst den ersten wieder freigeben (können Sie selbst) und dann den zweiten buchen
6. Anmeldefrist ist **spätestens 7 Tage vor der Prüfung** - wie auch bei schriftlichen Prüfungen.

+++

Bitte beachten Sie auch, dass die korrekte Anmeldung in PSSO für dieses Modul Ihre Verantwortung ist.

+++

Wenn Sie nun die Organisation geschafft haben, dann bleibt mir nur noch, Ihnen viel Erfolg für die Vorbereitung zu wünschen!
