---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Energieeffiziente Haushaltsgeräte

+++

`{only} html [![DOI](https://zenodo.org/badge/DOI/10.0000/zenodo.0000000.svg)](https://doi.org/10.0000/zenodo.0000000)`

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen - auch im pdf!!

+++

- [ ] TODO Version xy auf zenodo hochladen, DOI erhalten und dann hier Zitat angeben

+++

- [ ] TODO in JOSE publizieren und dann ebenfalls Zitat hier angeben

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import plotly.express as px
import sys
import os
import locale
from distutils.spawn import find_executable

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

- [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

+++

### Funktionen definieren

+++

## Lernziele

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
    <ul>
        <li> was </li>
        <li> womit </li>
        <li> wozu </li>
    </ul>
</div>

+++

## Haushaltsgeräte als Beispiele für Energieeffizienzmaßnahmen

+++

An Haushaltsgeräten lassen sich alle Energieeffizienzmaßnahmen aus HIER EINFÜGEN exemplarisch untersuchen. Da es einfach ist, Messdaten selbst zu erzeugen, sind in den Projekten immer wieder Messungen an Haushaltsgeräten durchzuführen.

+++

## Kühl- und Gefriergeräte

+++

![Kühlschrank](https://images.unsplash.com/photo-1536353284924-9220c464e262?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1051&q=80) Photo by <a href="https://unsplash.com/@nicotitto?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">nrd</a> on <a href="https://unsplash.com/s/photos/fridge?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

+++

Ein Kühl- und Gefriergerät muss desto mehr arbeiten, je stärker es herunterkühlen muss (z. B. neben dem Herd oder anderen Wärmequellen). Es lässt sich also modellieren, wie sehr der Energieverbrauch von der Außentemperatur (am Aufstellort des Geräts) und von der Kühltemperatur abhängt:

+++

- [ ] TODO relevante Gleichung einfügen und Codebeispiel

+++

Verhindert man an einem Kühl-/Gefriergerät die Entwärmung, indem man z. B. die Lüftungsgitter verdeckt oder keinen ausreichenden Abstand zur Wand einhält, so verändert sich der Arbeitspunkt des Kompressors. Im Extremfall überhitzt er und fällt (eines Tages) aus. Im nicht ganz so extremen Fall, arbeitet das Gerät in einem ineffizienten Arbeitspunkt und benötigt wesentlich mehr Energie. Auch die Nutzung als Einbaugerät ist daher weniger effizient als ein Standgerät.

+++

- [ ] TODO Formel und Codebeispiel

+++

Es hängt von der Füllung des Gerätes ab, wie kritisch ein längeres Türöffnen sich auf den Energiebedarf auswirkt: Enthält ein Kühlgerät viele wasserhaltige Dinge, so wärmt sich dieses wegen der hohen Wärmekapazität nur langsam auf. Wenn hingegen viel Luft enthalten ist, wirkt sich eine längere Türöffnung ungünstiger auf die Innentemperatur aus:

+++

- [ ] TODO Formel und Codebeispiel

+++

![Vereistes Gefriergerät](https://images.unsplash.com/photo-1487560227971-981afbe1c020?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=967&q=80) Photo by <a href="https://unsplash.com/@dev_irl?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Dev Benjamin</a> on <a href="https://unsplash.com/s/photos/fridge?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

+++

Wird ein Gefriergerät nicht ausreichend oft abgetaut, so bildet sich eine Schicht Eis im Inneren. Diese entsteht, weil beim Öffnen warme, feuchte Luft in das Gefriergerät eintritt, die beim Abkühlen kondensiert. Sobald Eis die Oberfläche des inneren Wärmetauschers bedeckt oder das Schließen der Tür behindert, steigt der Strombedarf.

+++

- [ ] TODO Formel und Codebeispiel

+++

Einige technische Tricks verringern den Abtaubedarf {cite:p}`albert-seifried_besonders_2021`:
- *No-Frost*: keine Eisbildung mehr, da ein kleines Gebläse die Luft im Inneren permanent zirkulieren lässt (benötigt aber auch Strom)
- *Low-Frost* oder *Stop-Frost*: verringern den Abtaubedarf

+++

Ob und wieviel solche Tricks bringen lässt sich häufig nur mit Messungen herausfinden.

+++

- [ ] TODO Formel und Codebeispiel

+++

Nicht zuletzt wirkt sich auch die Dimensionierung aus: ein zu groß dimensioniertes Kühlgerät enthält verhältnismäßig mehr Luft (oder mehr unnötige Lebensmittel). Richtwerte für ein bis zwei Personen sind 100 bis 140 l Kühlinhalt und 50 l zusätzlich für jede weitere Person. Bei Gefriergeräten reichen 50 bis 80 l weniger und 100 bis 130 l bei intensiver Vorratshaltung {cite:p}`melanie_schleputz_haushaltsgerate_2021`.

+++

- [ ] TODO Formel und Codebeispiel

+++

Altgeräte enthalten als Kühlmittel leider häufig noch stark klimaschädliche Fluorchlorkohlenwasserstoffe (FCKW). Eine nicht fachgerechte Entsorgung belastet also das Klima eventuell mehr als die Energieeinsparung durch ein effizienteres Neugerät.

+++

- [ ] TODO Formel und Codebeispiel

+++

## Waschmaschinen

+++

![Waschmaschine](https://images.unsplash.com/photo-1610557892470-55d9e80c0bce?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1023&q=80) Photo by <a href="https://unsplash.com/@enginakyurt?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">engin akyurt</a> on <a href="https://unsplash.com/s/photos/washing-machine?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

+++

Eine gut dimensionierte Waschmaschine läuft meist vollbeladen (außer bei Fein- und Wollwäsche). Eine Mengenautomatik, die den Wasser- und Stromeinsatz anpasst, bringt eine bessere Effizienz im Teilbeladungsbereich.

+++

- [ ] TODO Formel und Codebeispiel

+++

Eco-Waschprogramme sind auf verringerten Energiebedarf optimiert. Wichtig ist, zu überprüfen, ob die Wäsche auch damit ausreichend sauber wird. Viele dieser Programme reduzieren die Waschtemperatur zumindest über weite Teile der Waschzeit und nutzen stärker passive Effekte wie das Einweichen der Wäsche in Waschlauge.

+++

- [ ] TODO Formel und Codebeispiel Waschtemperatur bzw. Schleuderzahl

+++

Das Warmwasser für die Waschmaschine kann unterschiedlich erzeugt werden: wird z. B. solarthermisch erwärmtes Wasser verwendet, so ...

+++

- [ ] TODO Formel und Codebeispiel

+++

## Wäschetrockner

+++

Falls die Wäsche anschließend maschinell getrocknet werden soll, ist eine hohe Schleuderzahl zu empfehlen, da sich hiermit die Trocknungszeit verringert und somit der Strombedarf für den Wäschetrockner kleiner wird. Gegenzurechnen ist allerdings, dass sensible Textilien bei höherer Schleuderzahl stärker Schaden nehmen und häufiger bzw. rascher kaputtgehen. Außerdem ist zu berücksichtigen, dass Wäsche, die mit Heizluft getrocknet wird, ebenfalls indirekt zum Energiebedarf beiträgt (denn dann ist es nötig, die feuchte Luft wegen der Schimmelgefahr aus der Wohnung zu entfernen und das geschieht meist durch Lüften). Draußen auf Balkon oder im Garten getrocknete Wäsche hingegen benötigt keine zusätzliche Energie.

+++

- [ ] TODO Formel und Codebeispiel

+++

![Wäscheleine](https://images.unsplash.com/photo-1601486500465-3d5f59a2a269?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NXx8Y2xvdGhlcyUyMGRyeWVyfGVufDB8MHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60) Photo by <a href="https://unsplash.com/@ekarchmit?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Ernest Karchmit</a> on <a href="https://unsplash.com/s/photos/clothes-dryer?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

+++

Wäschetrockner verbrauchen generell verhältnismäßig viel Strom. Die Dimensionierung (d.h. Voll-Beladung) wirkt sich daher besonders stark aus.

+++

- [ ] TODO Formel und Codebeispiel

+++

Eine verschlechterte Wärmeabfuhr, z. B. durch ein verstopftes Flusensieb, verringert auch die Effizienz des Gerätes.

```{raw-cell}
- [ ] TODO Formel und Codebeispiel
```

Wärmepumpentrockner arbeiten nach dem Prinzip ... und benötigen daher ... Energie als konventionelle Trockner, die nach dem Prinzip ... arbeiten.

+++

- [ ] TODO Formel und Codebeispiel

+++

Es kommt beim Trocknen auf den Feuchtegrad an. Steuert man den Trockner nach der Trockenzeit, kann daher der Energiebedarf höher sein als bei der Steuerung nach der Wäschefeuchte.

+++

- [ ] TODO Formel und Codebeispiel

+++

## Geschirrspüler

+++

Die Wassermenge beim Spülen von Hand hängt davon ab, wie das Abwaschen organisiert wird: Wenn in einem Spülbecken eine große Menge Geschirr mit einer Wassermenge gespült wird und zudem zunächst die Gläser im saubereren Wasser drankommen, bevor im dann schon verschmutzteren Wasser die Töpfe und das Besteck gesäubert wird, benötigt der Handwaschprozess ähnlich viel Energie wie ein Geschirrspüler (PROJEKT ZITIEREN).

+++

- [ ] Formel und Codebeispiel

+++

Wird "nach Bedarf" sofort per Hand gespült, d.h. für jede Tasse, jeden Löffel, separat Wasser erhitzt, so erhöht sich die benötigte Wassermenge und damit auch die Energiemenge zum Aufheizen des Wassers. Dann sind sparsame Geschirrspüler effizienter.

+++

![Spülmaschine](https://images.unsplash.com/photo-1620568400263-6f1cf95b9e30?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80) Photo by <a href="https://unsplash.com/@mohammadesmaili?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Mohammad Esmaili</a> on <a href="https://unsplash.com/s/photos/dishwasher?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

+++

Auch Geschirrspüler profitieren von einer optimalen Dimensionierung, d.h. wenn sie voll beladen laufen. Auch der Verzicht auf Vorspülvorgänge verbessert den Energieverbrauch.

+++

- [ ] TODO Formel und Codebeispiel

+++

Das Eco-Programm von Geschirrspülern verringert die Wassertemperatur und nutzt stärker Einweicheffekte für das Geschirr.

+++

- [ ] TODO Formel und Codebeispiel

+++

## Staubsauger

+++

![Staubsauger](https://images.unsplash.com/photo-1619440321869-1feb57eb12cb?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80) Photo by <a href="https://unsplash.com/@immorenovation?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Daniela Gisin-Krumsick</a> on <a href="https://unsplash.com/s/photos/vacuum-cleaner?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

+++

- [ ] TODO Stiftung Warentest: Wattzahl sagt nichts aus über Saugleistung finden und darstellen, dass Gesamtkonstruktion inkl. Düse, Luftstromführung, Filtertechnik, Gehäusedichtheit wichtig

+++

Stromverbrauch abschätzen mithilfe der Nennleistung:

```{code-cell} ipython3
PN = np.linspace(500,2000,6) # W
t_use = 1 # usage hours on a usage day
d_use = 52 # days of usage
W_vacuum_cleaner = 0.001*PN*t_use*d_use # kWh
fig = px.bar(W_vacuum_cleaner, x=PN, y=W_vacuum_cleaner,
            labels={"x": "Nennleistung [W]",
                   "y": "jährlicher Energiebedarf [kWh]"})
fig.show()
```

Mit dem Energielogger können Sie ermitteln, ob und wie häufig die Nennleistung abgerufen wird, bei welcher Verschmutzungsart und welchem Untergrund (Parkett, Teppich) dies der Fall ist. So werden die oben ermittelten Werte realistischer.

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Der Energielogger misst minütliche Mittelwerte. Wie lange muss eine Messung mindestens dauern, um eine relevante Aussage zu erhalten? Welche Staubsaug-Aufgabe eignet sich hierfür?
</div>

+++

*Antwort:*...

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Schätzen Sie ab, ob ein Staubsauger mit Akku energieeffizienter ist als einer mit Kabel.
</div>

+++

*Antwort:*...

+++

Übrigens sind Energielabel seit Anfang 2019 für Staubsauger gerichtlich festgestellt nicht mehr nötig, weil die Tests nicht realistisch genug waren. QUELLE EINFÜGEN

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
