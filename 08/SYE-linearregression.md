---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Lineare Regression

+++

`{only} html [![DOI](https://zenodo.org/badge/DOI/10.0000/zenodo.0000000.svg)](https://doi.org/10.0000/zenodo.0000000)`

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen - auch im pdf!!

+++

- [ ] TODO Version xy auf zenodo hochladen, DOI erhalten und dann hier Zitat angeben

+++

- [ ] TODO in JOSE publizieren und dann ebenfalls Zitat hier angeben

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import plotly.express as px
import sys
import os
import locale
from distutils.spawn import find_executable

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

### Funktionen definieren

+++

## Lernziele

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
    <ul>
        <li> was </li>
        <li> womit </li>
        <li> wozu </li>
    </ul>
</div>

+++

## Lineare Regression

+++

Lineare Regression bedeutet, dass man annimmt, dass sich eine Datenreihe $y_i(x_i)$ mit einer Gerade modellieren lässt:

+++

\begin{equation}
y = m\cdot x + b
\end{equation}

+++

Die Gerade $y(x)$ hat die Steigung $m$ und den $y$-Achsen-Abschnitt $b$.

```{code-cell} ipython3
:tags: [hide-input]

x = np.linspace(-5,5,1000)
m = 5
b = 3
y = m*x+b
xi = np.linspace(-5,5,20)
yi = m*xi+b+5*np.random.randn(len(xi))
plt.plot(x,y,label='$y$')
plt.plot(xi,yi,'ro',label='$y_i$')
plt.axhline(color='black', lw=0.5)
plt.axvline(color='black', lw=0.5)
plt.text(-1,b*1.5,'b='+str(b))
plt.plot([np.min(x),np.max(x)],[b,b],'k--')
plt.text(1,4*m,'dy/dx $\propto$ m')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
```

## Wie man die Gerade findet

+++

- [ ] scipy curve fit Beispiel hier einbauen und mit xi und yi von oben durchführen

+++

## Nutzung für die Modellierung von Energiedaten

+++

- [ ] Beispiel einfügen, wo aus zwei Waschmaschinenmessungen bei verschiedenen Temperaturen eine Gerade ermittelt wird, wie diese Messung sich dann bei anderen Temperaturen verhält

+++

## Nutzung für die Klärung von Zusammenhängen

+++

- [ ] Zwei Beispiele einfügen, eins, wo die lineare Regression funktioniert und es einen Zusammenhang gibt zwischen den Daten und der Gerade und eins, wo sie nicht funktioniert, müssen auch Energiedatenbeispiele sein

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
