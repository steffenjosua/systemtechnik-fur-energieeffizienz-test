---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Einführung und Kursorganisation

Dieses Kapitel beinhaltet organisatorische Informationen (was wird benotet, welche Termine finden wo statt) und eine Einführung in das Thema Systemtechnik für Energieeffizienz (SYE).

+++

![SYEstruktur](../img/SYEstruktur.png)
