---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Verhalten im Labor

+++

## Lernziele für diesen Abschnitt

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>Lernziele:</b></div>
    <ul>
        <li> Verhaltensregeln im Labor kennen </li>
        <li> mithilfe wichtigen Hinweisen </li>
        <li> um sich selbst und andere vor auftretenden Gefahren zu schützen und die Laborgeräte pfleglich zu behandeln </li>
    </ul>
</div>

+++

## SARS-CoV-2 Besonderheiten

+++

- Beachten Sie die **aktuell geltenden** Regeln an der TH Köln, zu finden unter [th-koeln.de/corona](https://www.th-koeln.de/corona). Diese haben Vorrang vor allen hier folgenden Informationen.

+++

- Die TH gibt Armbänder an diejenigen aus, die GGG erfüllen, am Campus Deutz findet dies im Hauptgebäude statt. Geimpfte und Genesene benötigen wöchentlich ein neues Armband, Getestete täglich. **Planen Sie für den Erhalt des Armbandes eine Wartezeit ein und seien Sie trotzdem pünktlich beim Praktikum - ohne Sicherheitsunterweisung ist keine Teilnahme möglich! Ohne gültiges Armband ist ebenfalls keine Teilnahme möglich!**

+++

- Wir müssen engmaschig stoßlüften oder dauerlüften, spätestens alle 60 Minuten für 3-5 Minuten. **Bringen Sie sich warme, trockene Kleidung und ggf. eine Decke mit**.

+++

- Es besteht die Verpflichtung, eine medizinische OP-Maske oder FFP2-Maske zu tragen.

+++

- Fassen Sie gemeinsam genutzte Flächen / Gegenstände wie Stifte, Tastatur, Maus, Touchscreen, Messgeräte mit Handschuhen an (werden bereitgestellt). Für Touchscreen-Bedienung stehen Touchstifte zur Verfügung.

+++

## Sicherheitshinweise für das Praktikum

+++

<div class="admonition warning" style="background: #fff9e5; padding: 10px">
<div class="title"><b>Remote-Versuch:</b></div>
Sollte ein Praktikumsversuch digital stattfinden müssen: Auch, wenn Sie remote nicht in Gefahr sind, gehören Sicherheitshinweise auch zum Inhalt der Laborausbildung. Sie werden ja sicher als Ingenieur später auch selbst in Präsenz in Labors arbeiten. Daher werde ich auch remote eine Unterweisung durchführen.
</div>

+++

<div class="admonition warning" style="background: #fff9e5; padding: 10px">
<div class="title"><b>Präsenz-Versuch:</b></div>
Ohne Sicherheitsunterweisung kann kein Praktikum stattfinden!</div>

+++

### Pünktlich mit Unterschrift

+++

- das Praktikum beginnt **pünktlich** mit der Sicherheitsunterweisung im Praktikumsraum (siehe Stundenplan des aktuellen Semesters)
- Sie bestätigen mit Ihrer Unterschrift, dass Sie unterwiesen wurden und die Sicherheitshinweise verstanden haben.

+++

### Bei Krankheit

+++

- Bei plötzlicher Krankheit, wenn möglich, abmelden (z. B. Info an Mitstudierende, Mail an Dozentin)
- Nachholtermin: Priorität auf dijenigen, die einen Termin aufgrund nachgewiesener Krankheit (Attest) nicht wahrnehmen konnten

+++

### Verhalten im Labor

+++

- Rauchen, essen und trinken sind nicht erlaubt. Bitte gehen Sie dafür in einer Pause nach draußen.
- Notausgang: Labortür, diese freihalten (auch von Jacken und Taschen)
- Alarm: Verlassen Sie bei **anhaltendem Klingelzeichen** sofort das Gebäude und begeben Sie sich zum Sammelpunkt auf dem Parkplatz vor dem Hochschulgebäude.
- Abstellfläche für Jacken und Taschen: Nutzen Sie die Stuhllehnen (Jacken) und stellen Sie Ihre Taschen unter die Labortische, damit niemand darüber stolpert.
- Benutzen Sie **nicht** den im Labor HW2-42 vorhandenen Wasserhahn (Defekt im Abfluss)
- Achten Sie auf eng anliegende Kleidung und Schmuck, die sich nicht an Versuchseinrichtungen verfangen können.
- Verändern Sie auf keinen Fall die Sicherheitseinrichtungen. 
- Halten Sie sich an die Laborordnung, die Regelungen in der Sicherheitseinweisung und an die Bedienungsanleitungen für die Messgeräte und Prüflinge
- Beachten Sie vorhandene Warn- und Hinweisschilder
- Rechner, Geräte und Werkzeuge sind sorgfältig zu behandeln und deren Kenndaten zu beachten, um eine Zerstörung zu vermeiden. Für grob fahrlässige und vorsätzlich verursachte Schäden ist der Benutzer voll ersatzpflichtig.
- Nur an unterwiesenen Geräten arbeiten.
- Meiden Sie defekte oder auffällige Geräte umgehend, verwenden Sie diese nicht und entziehen Sie sie der Benutzung durch andere Personen. Reparaturen sind nicht Aufgabe der Praktikumsteilnehmer.

+++

### Not-Aus

+++

- Not-Aus: **Im Notfall** an den Versuchstischen betätigen.
- Not-Aus: **Nicht ohne Grund** testen.

+++

### Bei Unfällen oder Brand

+++

- **Elektrounfall**: immer dem Durchgangsarzt vorstellen (siehe [Rubrik Notfallorganisation, Abschnitt Wichtige Telefon- und Notrufnummern](http://fh-koeln.agu-hochschulen.de))
- **leichte Unfälle**: mit leichten Verletzungen, Unwohlsein oder Hautreaktionen Arzt aufsuchen
- **schwerwiegende und nicht einschätzbare Verletzungen durch Unfälle**: Notarzt alarmieren und Ersthelfer (z. B. Prof. May) rufen. Ortskundige Personen am Eingang des Gebäudes postieren, die den Notarzt auf direktem Weg zum Verletzten führen.
- Verbandkästen befinden sich in den angrenzenden Laboren. Defibrillatoren befinden sich in Halle West Ebene 1 gegenüber HW01-53 und im Zentralbereich des Hochhauses in Ebene 1 und Ebene 5. 
- Brand: Der nächstgelegene Feuerlöscher befindet sich im Gang. Informieren Sie die Feuerwehr (Tel 112) und die Leitwarte (0221 8275 2000) unter Nennung der Raumnummer (HW2-47 großer Raum bzw. HW2-42 kleiner Raum). Wenn möglich, informieren Sie Anwesende und verlassen Sie das Gebäude auf den gekennzeichneten Fluchtwegen. Leisten Sie den Anweisungen der Feuerwehr Folge.

+++

### Während der Versuchsdurchführung

+++

- **Vor jedem Versuch und vor dem Einschalten:** Machen Sie sich mit der elektrischen Anordnung, deren Aufbau und Arbeitsweise sowie mit der Bedienung der verwendeten Geräte vertraut. 
- **Vor jedem Versuch:** Prüfen Sie, ob **augenscheinlich** der Versuchsaufbau einwandfrei ist. Insbesondere dürfen keine nassen elektrischen Geräte benutzt und keine nassen elektrischen Anlagen bedient werden.
- **Während den Versuchen:** Bei Störungen von Geräten ist deren Spannungsversorgung sofort abzuschalten, notfalls mit Not-Aus.
- Installieren oder Deinstallieren von Software auf den Rechnern ist untersagt, sofern dies nicht mit dem Laboringenieur oder Dozenten abgesprochen worden ist.
- Die Internetnutzung unterliegt den Bestimmungen des Wissenschaftsnetzes (WIN). Das Aufrufen oder Herunterladen von Netzinhalten, die gesetzeswidrig oder unsittlich sind, ist verboten.
- Werden extern erstellte Dateien in der Hochschule weiterbearbeitet, so sind sie unbedingt vorher mit einem aktuellen Viren-Scanner zu überprüfen.
- **Versuch elektrische Energie messen**: Umstecken von Verbrauchern am Leistungsmessgerät nur durch Dozenten, solange Steckdosenadapter nicht mechanisch am Versuchstisch befestigt!
- **Versuch elektrische Energie messen und Versuch Thermografie**: Wechsel der Leuchtmittel nur bei ausgesteckter Lampe!
- **Versuch Thermografie**: Warmhalteplatte maximal auf 55°C erhitzen (mit Kontaktthermometer messen)!
- **Versuch elektrische Energie messen und Versuch Thermografie**: Zum Anfassen heißer Leuchtmittel stehen Hitzeschutzhandschuhe zur Verfügung.
- **Versuch Energielogger**: Beachten Sie die Bedienhinweise in der Bedienungsanleitung. Insbesondere dürfen Sie die Energielogger nicht bei extremer Hitze, Kälte, chemischen Einflüssen oder auch in feuer- bzw. explosionsgefährdeten Bereichen einsetzen.
- **Versuch Thermografie**: Verwenden Sie den Temperatur-Tastfühler nicht an spannungsführenden Teilen!
- Räumen Sie nach den Versuchen auf: Schalten Sie Prüflinge und Messgeräte ab. Legen Sie einzelne Teile in ordentlicher Anordnung wieder zurück an ihre Plätze. Schließen Sie die Fenster und löschen Sie das Licht.

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
