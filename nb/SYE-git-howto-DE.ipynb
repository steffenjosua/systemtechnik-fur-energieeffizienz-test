{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Leitfaden zur Zusammenarbeit mit git"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dieses Notebook soll als Leitfaden zur Zusammenarbeit mit git dienen. Hierbei dienen zwei fiktive Personen A und B als Beispiel. Das gezeigte Vorgehen lässt sich aber auch auf beliebig viele Personen erweitern."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition warning\" style=\"background: #fff9e5; padding: 10px\">\n",
    "<div class=\"title\"><b>Achtung:</b></div>\n",
    "Dieser Leitfaden bezieht sich auf git unter Windows\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modell für die Zusammenarbeit mit git"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Generell gibt es viele denkbare Modelle, wie die zusammenarbeit über git organisiert werden kann, siehe z.B. [hier](https://www.atlassian.com/git/tutorials/comparing-workflows).\n",
    "\n",
    "In diesem Beispiel wird ein Modell mit einem zentrale Repository verwendet."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition hint\" style=\"background: #e9f6ec; padding: 10px\">\n",
    "<div class=\"title\"><b>Hinweis:</b></div>\n",
    "git ist ein verteiltes Versionskontrollsystem (das bedeutet, jeder Mitarbeiter hat eine vollständige Kopie des Projekts und der zugehörigen Historie), dennoch sind mit git auch zentralisierte Abläufe der Zusammenarbeit möglich.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Person A ist \"Chef\", weil sie einen GitLab-Projektordner hat, wo alle Änderungen zusammenlaufen - das Haupt-Repository (oder auch upstream genannt). Person A ist dafür zuständig, dieses Haupt-Repository zu pflegen. Person B ist \"Mitarbeiter\", sie schlägt Änderungen am Haupt-Repository vor. Dafür erzeugt sich B einen fork (oder auch origin genannt) vom Haupt-Repository und klont diesen fork auf ihren Rechner. In dieser lokalen Kopie fügt B Änderungen in einem eigenen branch ein und pusht sie in das geforkte Repo. Dort stellt B einen pull request, der alle Änderungen enthält, an A. Gefallen A diese Änderungen, werden die Änderungen in das Haupt-Repository übernommen.\n",
    "\n",
    "Die folgende Abbildung zeigt dieses  Modell aus Sicht von B:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Model](../img/git-Model.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition hint\" style=\"background: #e9f6ec; padding: 10px\">\n",
    "<div class=\"title\"><b>Hinweis:</b></div>\n",
    "In git hat sich eine bestimmte Nomenklatur etabliert. Die beiden Namen \"upstream\" und \"origin\" sind reine Konvention und könnten auch anders lauten. Allerdings macht es Sinn, gebräuchliche Namen zu verwenden und sich direkt daran zu gewöhnen.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Der Ablauf der Zusammenarbeit lässt sich in die folgenden Punkte für A und B unterteilen:\n",
    "\n",
    "_Arbeitsschritte für B:_\n",
    "* B forkt Repo von A\n",
    "* B cloned das geforkte Repo\n",
    "* B nimmt Änderungen an der lokalen Kopie vor\n",
    " - erzeugt eigenen branch\n",
    " - addet Änderungen\n",
    " - erzeugt einen commit\n",
    "* B pusht die Änderungen in das geforkte Repo\n",
    "* B stellt einen pull request an A\n",
    "* B aktualisiert sein Repo (sobald A den pull request akzeptiert hat)\n",
    "\n",
    "_Arbeitsschritte für A:_\n",
    "* A überprüft die Änderungen\n",
    "* A akzeptiert den merge request (oder auch nicht)\n",
    "\n",
    "Dabei werden die Schritte der Reihe nach bearbeitet"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__B forkt Repo von A:__\n",
    "\n",
    "Um das Repo von A zu forken, muss B in GitLab zu diesem Repo navigieren und auf den Punkt ``Fork`` klicken. Dadurch wird eine serverseitige Kopie des Repos von A im Profil von B erstellt. Eine genauere Anleitung zum forken gibt es auch [hier](https://pages.cms.hu-berlin.de/cms-webtech/gitlab-documentation/docs/projekt-forken/). Dies ist in folgender Abbildung dargestellt:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![forken](../img/forken.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__B cloned das geforkte Repo:__\n",
    "\n",
    "Um das geforkte Repo zu klonen, muss B zunächst auf den Punkt ``Clone`` klicken und dann den Text unter dem Punkt \"Clone with HTTPS\" kopieren. Jetzt muss B auf seinem Rechner in dasjenige Verzeichnis navigieren, in welches das Repo gecloned werden soll.\n",
    "\n",
    "Hier muss B in git bash den Befehl ``git clone <URL von geforktem Repo>`` ausführen. Die folgende Abbildung zeigt das:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![clone](../img/clone.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__B nimmt Änderungen an der lokalen Kopie vor:__\n",
    "\n",
    "Um sicherzustellen, dass das geforkte (und somit auch das geclonte Repo) immer auf dem aktuellen stand sind, kann das Repo von A mit dem Befehl ``git remote add upstream <URL zum Repo von A>`` als remote Repository festgelegt werden __(Das vielleicht lieber am Ende erwähnen?)__.\n",
    "\n",
    "Mit ``git fetch upstream`` und ``git rebase upstream/master``, kann B sein lokales Repo ab jetzt immer up-to-date halten (siehe auch [hier](https://stackoverflow.com/questions/7244321/how-do-i-update-or-sync-a-forked-repository-on-github) oder [hier](https://medium.com/@sahoosunilkumar/how-to-update-a-fork-in-git-95a7daadc14e)).\n",
    "\n",
    "Jetzt kann B mit ``git checkout -b <Name vom branch>`` einen neuen branch erstellen, in dem er seine Änderungen vornimmt. Mit ``git add .`` werden alle Änderungen von git erfasst und dann mit ``git commit -m \"<Erläuterungen>\"`` übernommen."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__B pusht die Änderungen in das geforkte Repo:__\n",
    "\n",
    "Die Änderungen sind bis jetzt nur in dem branch der lokalen Kopie von B enthalten. Mit dem Befehl ``git push origin`` kann B die Änderungen in das geforkte Repo pushen. Damit sind die Änderungen auch im geforkte Repo ersichtlich."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__B stellt einen pull request an A:__\n",
    "\n",
    "Jetzt, da die Änderungen in dem geforkten Repo sind, kann B von diesem geforkten Repo aus einen pull request an A stellen. Das geht direkt in GitLab. Dafür muss B auf den Punkt ``Create merge request`` klicken. Die folgende Abbildung zeigt dies:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![pull request](../img/pull_request.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__A überprüft die Änderungen:__\n",
    "\n",
    "A bekommt jetzt in GitLab unter dem Punkt `Merge requests` angezeigt, dass ein neuer pull request von B verfügbar ist. Mit dem grünen Knopf `Merge` könnte A direkt den pull request akzeptieren. Der Punkt `Check out branch` zeigt, wie man einen lokalen branch erzeugt, um so zunächst mal die Änderungen anzuschauen. Die folgende Abbildung zeigt dies:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![pull request available](../img/pull_request_da.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ein Klick auf ``Check out branch`` öffnet ein Fenster wie in der folgenden Abbildung:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![check out branch locally](../img/check_out_branch.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Zunächst kann A mit ``git fetch origin`` die im pull request von B vorgeschlagenen Änderungen aus seinem GitLab Repo auf seinen Rechner holen. Mit<br>``git checkout -b <branchName> origin/<branchName>`` kann A jetzt lokal einen branch mit dem Namen ``<branchName>`` erzeugen, der dann alle Änderungen des branchs ``<branchName>`` aus origin enthält."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition hint\" style=\"background: #e9f6ec; padding: 10px\">\n",
    "<div class=\"title\"><b>Hinweis:</b></div>\n",
    "In diesem Fall bezeichnet origin das GitLab Repo von A (in der Abbildung zu Beginn ist das GitLab Repo von B als origin bezeichnet, weil es sich um eine Darstellung aus Sicht von B handelt).\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Jetzt kann A mit ``git diff <SHA1> <SHA2>`` - wobei ``<SHA1>`` der hash vom letzten commit auf master ist und ``<SHA2>`` der hash vom letzten commit auf ``<branchName>`` - die mit dem pull request eingeführten Änderungen ansehen (die hashes lassen sich mit ``git log`` in Erfahrung bringen)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__A akzeptiert die Änderungen (oder auch nicht):__\n",
    "\n",
    "In einem separaten Editor kann A jetzt auf ``<branchName>`` die vorgeschlagenen Änderungen von B entsprechend seinen Vorstellungen abändern (oder die Arbeit zurück an B reichen). Wenn die Änderungen soweit passen, stellt A mit ``git checkout master`` zunächst sicher, dass er im master branch ist, und übernimmt dann mit ``git merge --no-ff <branchName>`` die Änderungen (die Option ``--no-ff`` stellt dabei sicher, dass in jedem Fall ein merge commit erstellt wird).\n",
    "\n",
    "Sind die Änderungen lokal übernommen, kann A jetzt noch mit ``git push origin`` sein GitLab Repo entsprechend aktualisieren."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "__B aktualisiert seine Repos:__\n",
    "\n",
    "Hierfür kann B von seinem geklonten Repo  aus mit dem Befehl ``git fetch upstream`` und ``git merge upstream/master`` den neuesten Stand in sein geklontes Repo holen. Mit dem Befehl ``git push origin`` kann B anschließend auch noch sein geforktes Repo in GitLab aktualisieren ([siehe hier](https://stackoverflow.com/questions/7244321/how-do-i-update-or-sync-a-forked-repository-on-github))."
   ]
  }
 ],
 "metadata": {
  "hide_input": false,
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.10"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
