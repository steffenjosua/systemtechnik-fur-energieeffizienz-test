---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Effizient duschen

+++

`{only} html [![DOI](https://zenodo.org/badge/DOI/10.0000/zenodo.0000000.svg)](https://doi.org/10.0000/zenodo.0000000)`

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen - auch im pdf!!

+++

- [ ] TODO Version xy auf zenodo hochladen, DOI erhalten und dann hier Zitat angeben

+++

- [ ] TODO in JOSE publizieren und dann ebenfalls Zitat hier angeben

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
plt.rcParams['text.usetex'] = True
pd.set_option('display.latex.repr', True)
pd.set_option('display.latex.longtable', True)
locale.setlocale(locale.LC_ALL, '')
```

- [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

+++

### Funktionen definieren

+++

## Lernziele

+++

:::{admonition} Lernziele für diesen Abschnitt:
:class: tip
- was
- womit
- wozu
:::

+++

## Anforderungen an Duschen

+++

Temperatur (Achtung Legionellen!)

+++

Wassermenge

+++

- [ ] TODO Formel und Codebeispiel hinterlegen

+++

## Effizientes Duschen im Sommer

+++

Eiskalte Duschen nötigen dem Körper ab, sich danach wieder stark zu erwärmen - wodurch viele stärker schwitzen als vorher. Dadurch ist es nötig, noch öfter zu duschen, ein Teufelskreislauf. Etwas empfehlenswerter sind daher laukalte Duschen.

+++

- [ ] TODO Abschätzung und Code

+++

## Effizienter Duschen ganzjährig

+++

Je weniger das Wasser aufgeheizt werden muss, desto weniger Energie ist nötig. 

+++

Es ist sinnvoll, das Wasser mithilfe von Solarthermie, Erdwärme oder auch Abwärme vorzuwärmen. So verringert sich der Energiebedarf.

+++

- [ ] TODO Formel und Codebeispiel

+++

Auch die Wassermenge spielt eine Rolle. Sparduschköpfe mischen Wasser und Luft und verringern so den Wasserbedarf {cite:p}`co2online_8_2021`

+++

- [ ] TODO Formel und Codebeispiel

+++

## Suffizient duschen

+++

Vom Campen außerhalb von Campingplätzen und von Bergtouren ist bekannt: um sich hygienisch zu reinigen, genügt eine geringe Menge von Wasser und ein Waschlappen.

+++

- [ ] TODO abschätzen, was da das Einsparpotenzial ist

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
