---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Meilenstein 1

+++

HIER WERDEN DIE MEILENSTEINERGEBNISSE IN EINER TABELLE VERLINKT, SO DASS DIE PRÄSENTATION FLÜSSIG ABLAUFEN KANN DIESE FINDEN SICH IM ORDNER `/teach/ms1` (nicht synchronisiert auf gitlab).

```{code-cell} ipython3
import random
list = ['A','B','C','D','E','F','G','H','K','L','N','O']
random.shuffle(list)
print("Präsentationsreihenfolge : ",  list)
```
