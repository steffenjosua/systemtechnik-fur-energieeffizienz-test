---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Vorschriften und Normen für Energieeffizienz

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable


print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

### Funktionen definieren

+++

## Lernziele für dieses Kapitel

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
    <ul>
    <li> Energieeffizienz-Normen auf konkrete Systeme anwenden </li>
    <li> mithilfe beispielhafter Vorschriften aus Normen und der Kenntnis der Normungslandschaft </li>
    <li> um Messungen mit aktuellen Vorschriften vergleichen zu können </li>
    </ul>
</div>

+++

## Warum gibt es Energieeffizienz-Normen?

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Wie kann ein Hersteller angeben, welche Daten typisch sind für den Verbrauch eines Gerätes? Wie wird Vergleichbarkeit zwischen verschiedenen Herstellern möglich? Haben Hersteller ein Interesse daran, diese Informationen sichtbar zu machen?</div>

+++

*Antwort...*

+++

## Quellen für Normen

+++

### Wichtige Links

+++

Zwei Datenbanken lassen sich in der TH Köln oder mit [VPN](https://www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php) nutzen: 
- [perinorm](https://www.perinorm.com)
- [Normenbibliothek](https://www.normenbibliothek.de/vde-xaveropp/normenbibliothek/start.xav)

+++

Eine Übersicht über Energieeffizienz-Normen bietet die Energieeffizienz-Normungs-Roadmap des VDE {cite:p}`vde_verband_der_elektrotechnik_elektronik_informationstechnik_e_v_als_trager_der_elektronik_informationstechnik_in_din_und_vde_elektrische_2018`.

+++

Die [EU energy efficiency directive](https://ec.europa.eu/energy/topics/energy-efficiency/targets-directive-and-rules/energy-efficiency-directive_en) {cite:p}`europaische_kommission_richtlinie_2012` findet sich in allen europäischen Amtssprachen im Netz.

+++

Eine Übersicht der [produktspezifischen europäischen Energieeffizienz-Vorschriften](https://ec.europa.eu/energy/topics/energy-efficiency/energy-efficient-products/list-regulations-product-groups-energy-efficient-products_en) ist dort auch zu finden.

+++

![Energielabel einer Lampe](../img/energielabellampe.jpg)

+++

### Effizient Normungsinformationen aufbewahren

+++

Im jupyter Notebook lassen sich in einem dataframe in pandas auch Textschnipsel zusammen mit Zahlenwerten speichern - die Zahlenwerte kann man vorher ja auch ausrechnen und als Variablen weiter mitführen.

```{code-cell} ipython3
:tags: [hide-input]

volume = 33 
volume_unit = 'l'
volume_min = 33
volume_max = 33
height = 0.85
height_unit = 'm'
height_min = 0.80
height_max = 0.90
temperature = 5
temperature_unit = '°C'
temperature_min = 3
temperature_max = 7
operating_hours_daily = 24
operating_hours_daily_unit = 'h/d'
operating_hours_daily_min = 24
operating_hours_daily_max = 24
requirements = ['Volumen', 'Höhe', 'Temperatur', 'Betriebsstunden/Tag','Quelle','Kommentar']  
unit = [volume_unit,height_unit,temperature_unit,operating_hours_daily_unit]
values = [volume,height,temperature,operating_hours_daily]
values_min = [volume_min,height_min,temperature_min,operating_hours_daily_min]
values_max = [volume_max,height_max,temperature_max,operating_hours_daily_max]
source = ['Trump. Das Buch. 2121. Donalds Verlag', 'https://www.umweltbundesamt.de/umwelttipps-fuer-den-alltag/elektrogeraete/kuehlschrank', 'https://www.umweltbundesamt.de/umwelttipps-fuer-den-alltag/elektrogeraete/kuehlschrank', 'Bauchgefühl']
comment = ['Stand in der Zukunft???', 'von 2011, neuere?','bestätigt durch xy', 'TODO']
data = list(zip(requirements,unit,values,values_min,values_max,source,comment))
df = pd.DataFrame(data, columns = ['Anforderung','Einheit','Wert','min.', 'max.','Quelle','Kommentar'])
print(df)
```

Zugehörige Literatursammlungen lassen sich am besten mit einer Literaturverwaltung [citavi](https://www.th-koeln.de/hochschule/citavi-campus-lizenz_28056.php) oder [zotero](https://www.zotero.org) aufbewahren und mit Projektkollegen gemeinsam nutzen und pflegen. Beide Programme haben Browser-Plugins, mit denen sich pdf-Dateien häufig inklusive der Zitat-Daten mit einem Klick herunterladen lassen.

+++

## Kenngrößen für Energieeffizienz

+++

... dienen dazu, Energie auf andere wichtige Anforderungen zu beziehen. 
- Ein Produktionsunternehmen benötigt mehr Energie, wenn es mehr produziert und (fast) gar keine, wenn es Produktionsstopp hat.
- Ein großer Kühlschrank in der Gastronomie hat einen höheren elektrischen Energiebedarf als ein kleiner Kühlschrank für den Hausgebrauch.

+++

### Energielabel

+++

Neben dem EU Energielabel gibt es weitere Label, die einen mehr oder weniger starken Fokus auf Energieeffizienz, Ressourceneffizienz und Nachhaltigkeit legen:

+++

| EU Energielabel | Energy Star | Blauer Engel | Euroblume | TCO Certified |
|---|---|---|---|---|
| ![EU Energielabel Beispiel](https://ec.europa.eu/info/sites/default/files/energy_climate_change_environment/standards_tools_and_labels/images/fridges_web.jpg) | ![Energy Star Logo](https://www.energystar.gov/sites/default/files/assets/images/ES_logo_LearnMore_v_copyright_1.jpg) | <a title="Chris828, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Blauer-Engel-1st-Logo_JuryUmweltzeichen_MenschUmwelt.svg"><img width="256" alt="Blauer-Engel-1st-Logo JuryUmweltzeichen MenschUmwelt" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Blauer-Engel-1st-Logo_JuryUmweltzeichen_MenschUmwelt.svg/256px-Blauer-Engel-1st-Logo_JuryUmweltzeichen_MenschUmwelt.svg.png"></a> | <a title="Unknown author, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Euroblume_logo.svg"><img width="256" alt="Euroblume logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Euroblume_logo.svg/256px-Euroblume_logo.svg.png"></a> | <a title="TCO Development, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:TCO_Certified_Logo.svg"><img width="256" alt="TCO Certified Logo" src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/TCO_Certified_Logo.svg/256px-TCO_Certified_Logo.svg.png"></a> |

+++

**EU Energielabel**: Seit dem 1. März 2021 gilt für viele Haushaltsgeräte nicht mehr die alte Energieeffizienzklasseneinteilung von A+++ bis D. Stattdessen werden die Geräte in B bis G umgruppiert {cite:p}`albert-seifried_besonders_2021`. Wäschtrockner, Backöfen, Dunstabzugshauben und Fernsehgeräte werden in den kommenden Jahren ebenfalls umgelabelt werden.

+++

Hintergrund: Das EU-Energielabel existiert seit rund 25 Jahren. Über die verbesserte Kundeninformation, sollten bevorzugt Geräte mit geringem Stromverbrauch in den Markt gebracht werden. Inzwischen bildet die aktuelle Klasseneinteilung keine Differenzierung mehr, denn die meisten Geräte sind durch diese politische Vorgabe inzwischen deutlich optimiert worden. Dadurch wird die Effizienzskala nicht mehr ausgenutzt. Daher wurde das Label überarbeitet.

+++

Die Effizienzklasse G darf bei fast allen Waschmaschinen, Waschtrocknern, Geschirrspülern und Kühlgeräten nicht mehr in den Verkehr gebracht werden. Bestände beim Händler dürfen jedoch noch abverkauft werden. Drei Jahre später folgt das Verbot für Geräte der Klasse F. {cite:p}`albert-seifried_besonders_2021`

+++

Zudem sieht das neue Label Reparaturmöglichkeiten vor: {cite:p}`albert-seifried_besonders_2021`
- gängige Ersatzteile 7-10 Jahre verfügbar und teilweise auch für Privatleute erhältlich
- Ersatzteile lassen sich mit allgemein verfügbaren Werkzeugen und ohne Beschädigung der Geräte tauschen
- fachlich kompetente Reparateure erhalten Zugang zu Reparatur- und Wartungsinformationen

+++

Vorreiter beim Thema Zuverlässigkeit ist Miele (Beste in der Pannenstatistik der Stiftung Warentest 4/2018) {cite:p}`albert-seifried_besonders_2021`

+++

[Label2020](https://tool.label2020.eu/de) fasst die Regelungen für das neue Energielabel für jedes Gerät zusammen.

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Ermitteln Sie, welche <a href="https://ec.europa.eu/info/energy-climate-change-environment/standards-tools-and-labels/products-labelling-rules-and-requirements/energy-label-and-ecodesign_en">EU-Ökodesign-Verordnungen</a> für Ihr Gerät oder ähnliche Geräte gelten und notieren Sie hier die wichtigste Messvorschrift. Planen Sie, wie Sie diese selbst umsetzen können.
</div>

+++

*Antwort:...*

+++

- EU energy star im Februar 2018 ausgelaufen: [EU energy star Webseite](https://ec.europa.eu/energy/topics/energy-efficiency/energy-efficient-products/energy-star_en) {cite:p}``eu_energy_2018``
- geführt von der US amerikanischen EPA (environmental protection agency)
- implementiert ni USA, Kanada, Australien, Neuseeland, Japan, Taiwan
- amerikanisches Programm läuft weiter: https://www.energystar.gov/about {cite:p}``epa_what_2020``

+++

Beispiele für Kriterien {cite:p}``blauer_engel_kriterien_2013`` für Produkte:
- Ressourcen bei der Herstellung sparen
- aus nachhaltig produzierten Rohstoffen hergestellt werden
- weniger Ressourcen bei Nutzung und Entsorgung verbrauchen, z. B. besonders energieeffizient
- schädliche Substanzen für Umwelt oder Gesundheit vermeiden oder auf ein Mindestmaß beschränken
- besonders langlebig und reparaturfähig
- gut recyceln lassen
- geringe Emissionen in Boden, Wasser, Luft oder wenig Lärm verursachen
- und dabei trotzdem ihre Funktion (Gebrauchstauglichkeit) in hoher Qualität erfüllen

+++

Entscheidung durch die Jury Umweltzeichen (Vertreter aus Politik, Industrie, Gesellschaft)

+++

Euroblume: Fokus auf Umweltverträglichkeit, Produktgruppen finden sich auf der Webseite: https://eu-ecolabel.de/fuer-unternehmen/produktgruppen {cite:p}``eu_produktgruppen_2020``

+++

TCO-Certified: für nachhaltige IT-Produkte, eins der ältesten Labels, die es gibt<br><br>
Produktkategorien: https://tcocertified.de/produktkategorien/ {cite:p}``peter_tco_2020`` <br><br>
Kriterien in den Zertifizierungsdokumenten: https://tcocertified.com/certification-documents/

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Finden Sie heraus, ob Gerätearten für Ihr Projekt mit dem energy star, oder einem der anderen Label bewertet werden und notieren Sie relevante Energiekenndaten. 
</div>

+++

*Antwort:...*

+++

Wie auch im Automobilbereich gibt es bei Elektrogeräten ein Interesse einiger Hersteller, den Verbrauch "schön" zu rechnen: Das [EU-Projekt ANTICSS](https://www.anti-circumvention.eu/about-project/project-introduction) hat bis 2021 untersucht, inwiefern Energielabel gar nicht den tatsächlichen Verbrauch, sondern einen unrealistisch niedrigen Verbrauch angeben und daraus abgeschätzt, wie viel mehr elektrische Energie Energielabel-Geräte tatsächlich verbrauchen. 

+++

![Ergebnis ANTICSS](https://www.anti-circumvention.eu/storage/app/media/TW-FinRep_15.9.2021.png)

+++

In einem Beispiel hat eine Spülmaschine detektieren können, ob ein Test vorlag, weil dafür mind. ein Korb ausgebaut werden musste - und tatsächlich dann deutlich weniger elektrische Energie benötigt.

+++

### Selbst erdachte Kennwerte

+++

Die ISO 50001 fordert die Festlegung sogenannter *Energy Performance Indicators (EnPI)* (deutsch Energieleistungskennzahlen). So eine Performance-Kennzahl beinhaltet für einen Prozess Input- und Output-Größen. Beispielsweise könnte ein Bäcker sich ansehen, wie viel Kilogramm Backwaren er pro Energieeinsatz erhält.

+++

- wie ein Hersteller für Energieeffizienz-Analysesysteme für sich wirbt: https://www.youtube.com/watch?v=zSIuvuoFofM {cite:p}`enit_systems_kosten_2016`
- Beispiele für individuelle Energieeffizienzkenngrößen in einer Case Study des Energiemanagement-Anbieters Limón: https://www.limon-gmbh.de/fileadmin/user_upload/Berechnung_und_Analyse_energetischer_Kennzahlen_Schoengen.pdf {cite:p}`limon_gmbh_case_2020`

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Definieren Sie eigene Energieeffizienzkenngrößen für Ihre Projekte. Begründen Sie, warum Sie diese Kenngröße relevant halten und geben Sie am Ende des Projektes eine Information darüber, wie sehr sich die betrachteten Energieeffizienzmaßnahmen auf Ihre Kenngröße auswirken.
</div>

+++

*Antwort:...*

+++

![Prozess zur Analyse der Energieeffizienz](../img/IEC-ACEE-energy-efficiency-process.svg)
angelehnt an den IEC ACEE questionnaire 2017

+++

## Historische Entwicklung von Effizienzvorgaben

+++

{cite:p}``cui_inc_efficiency_2021`` zeigt eine Vielzahl von Effizienzstandards, die insbesondere für Schaltnetzteile relevant sind und wie sich diese seit 2004 entwickelt haben.

+++

{cite:p}`todd_philipps_schmelzwiderstand_2021` stellt dar, welche Effizienzkennlinien sich daraus ergeben:

```{code-cell} ipython3
:tags: [hide-input]

curves = 4
curve_colors = ['red', 'green', 'blue', 'darkblue']
labels = ['2007 USA', '2009 Europe', '2016 USA', '2014 Europe']
datafiles = []
ymin = 50 # minimaler Wirkungsgrad (Minimum y-Achse)
ymax = 90 # maximaler Wirkungsgrad (Maximum y-Achse)
imgfile = '../data/SYE_Schmelzwiderstand.jpg'

for i in range(curves):
    datafile = '../data/schmelzwiderstand_' + curve_colors[i] + '.csv'
    # ...
    # prüfen, ob Datei bereits existiert, falls ja:
    if os.path.isfile(datafile):
        datafiles = ['../data/schmelzwiderstand_red.csv',
                     '../data/schmelzwiderstand_green.csv',
                     '../data/schmelzwiderstand_blue.csv',
                     '../data/schmelzwiderstand_darkblue.csv']
        break
        
    # falls noch nicht:    
    else:
        print ("Datei existiert noch nicht")
        #url = # => noch herausfinden
        #filename = '../data/SYE_Schmelzwiderstand.jpg'
        # dann erstmal aus Internet einlesen...
        #r = requests.get(url, allow_redirects=True)
        # ... und in Datei speichern
        #open(filename, 'wb').write(r.content)
        im = Image.open("../data/SYE_Schmelzwiderstand.jpg").convert("RGB")
        # Datei in png umwandeln, dass matplotlib klarkommt
        im.save("../data/schmelzwiderstand.png","png")
        res=plt.imread('../data/schmelzwiderstand.png')
        %matplotlib qt
        plt.imshow(res)
        # jetzt die einzelnen Punkte anklicken:
        print(f'please click along the {curve_colors[i]} curve.')
        x = plt.ginput(50,timeout=120) 
        # erster Klick: links unten Ecke der Skala
        # zweiter Klick rechts unten bei 100 % 
        # dritter Klick oberster Punkt der y-Achse
        # danach erst Wirkungsgradkurve nachklicken
        # ginput beenden mit mittlerer Maustaste
        # mit rechter Maustaste letzten Punkt entfernen
        # mit links wieder hinzufügen
        print(x)
        np.savetxt(datafile,x,delimiter=", ")
        datafiles.append(datafile)
        
%matplotlib inline

# all die abgetippten Kurven in einen plot zeichnen
plt.figure(figsize=(16, 8))
for num, file in enumerate(datafiles):
    eta = pd.read_csv(file, header=None)
    eta.rename(columns={0:'xpixel', 1:'ypixel'}, inplace=True)
    ursprung = eta.iloc[0, :]
    eta['xpxgenullt'] = eta['xpixel']-ursprung[0]
    eta['ypxgenullt'] = -(eta['ypixel']-ursprung[1])    
    x100prozent = eta.iloc[1,2]
    eta['P/PN'] = eta['xpxgenullt']*100/x100prozent
    y100prozent = eta.iloc[2,3]
    eta['Wirkungsgrad'] = ymin+eta['ypxgenullt']*(ymax-ymin)/y100prozent
    eta.drop(axis = 0, index = [0, 1, 2], inplace=True)
    plt.plot(eta['P/PN'],eta['Wirkungsgrad'],'o-', color=curve_colors[num],
            label=labels[num])
    
plt.minorticks_on()
plt.grid(color='gray', linestyle='-', linewidth=1,which='both')
plt.xlabel('$P/P_N$ [$\%$]')
plt.ylabel('Wirkungsgrad [$\%$]')
plt.title('Daten: T. Phillips "Schmelzwiderstand", Design\&Elektronik 5/2021',
          color='gray')
plt.legend();
```

# Literatur

+++

```{bibliography}
:filter: docname in docnames
```
