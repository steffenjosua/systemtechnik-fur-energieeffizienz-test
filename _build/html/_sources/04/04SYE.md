---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Mit Daten kritisch umgehen

+++

Dieses Kapitel beinhaltet Techniken zur Plausibilisierung sowohl von Zeitreihen als auch von Einzeldaten. Dies ist wichtig, denn "wer misst, misst (auch) Mist" und daher sind sowohl eigene Messungen als auch fremde Daten immer möglicherweise fehlerbehaftet und manchmal auch aus Marketing-Gründen oder anderen Interessen heraus "geschönt". Es ist daher notwendig "fake data" von "real data" gut zu unterscheiden.
