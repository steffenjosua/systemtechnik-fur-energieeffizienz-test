---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Elektrische Energie messen: Messgrößen und Normen

+++

Dieses Kapitel wiederholt die Grundbegriffe elektrischer Größen, die für die Energiemessung relevant sind. Außerdem zeigt es auf, wie die Messung mit einem bestimmten haushaltsüblichen Energielogger und einem professionellen Leistungsanalysator durchgeführt wird. Besonders interessante Normen für die Messung der elektrischen Energieeffizienz runden die Informationen ab.
