# Unterlagen Systemtechnik für Energieeffizienz <img src="img/label-SYE.png" width=100 />

*Systemtechnik für Energieeffizienz* ist ein Kurs im Master Elektrotechnik und im Master Erneuerbare Energien an der Technischen Hochschule Köln.

Hier ein kurzer Überblick über die Unterlagen:

:::{panels}
:container: +full-width text-center
:column: col-lg-6 px-2 py-1
:card:

**[Termine für den laufenden Kurs](termine)** ✍
^^^
In der Übersicht findet sich der aktuelle Plan. Es kann Änderungen geben. Die aktuellste Version findet sich immer in den online-Unterlagen, d.h. im [HTML-Buch](http://jfmay.gitlab.io/systemtechnik-fuer-energieeffizienz/README.html) und auf [gitlab](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz).
---
**[Anmelden, Unterlagen abgeben, zusammenarbeiten](https://ilias.th-koeln.de)** ☁️
^^^
Die **Anmeldung** für die Prüfung ist in [PSSO](https://psso.th-koeln.de). Seminartermine und Tutorien finden bis auf weiteres in zoom statt. Den Link dafür finden Sie in [ILIAS - F07 - Dozenten - May - SYE](https://ilias.th-koeln.de). Außerdem findet in ILIAS die Anmeldung zu Projekten und Praktika und das Hochladen von Unterlagen statt. Um online zusammenzuarbeiten, gibt es ab Winter 2021 die Möglichkeit, einen [jupyterhub](https://jupyterhub.th-koeln.de) zu nutzen. Dafür müssen Sie sich im Netz der TH befinden (ggf. [VPN](https://www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php) nutzen) und wir müssen Sie dafür freischalten. Alle, die in ILIAS im Kurs angemeldet sind, werden für den jupyterhub freigeschaltet. 

:::

Die Unterlagen gibt es in unterschiedlichen Formaten: 

- **[gitlab](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz)** Hier können Sie die jeweils aktuellste Version der Unterlagen herunterladen und die Notebooks (`.ipynb`) entweder auf Ihrem eigenen Rechner oder auf dem jupyterhub ausführen. Die Unterlagen werden während des Semesters vervollständigt.
- **[html-Buch](http://jfmay.gitlab.io/systemtechnik-fuer-energieeffizienz/README.html)** Dieses Buch beinhaltet in strukturierter Version alle Unterlagen. Sie können interaktive Diagramme verwenden oder Code einblenden, jedoch nicht ausführen. Es wird während des Semesters vervollständigt.
- **[pdf-Buch](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz/-/blob/master/_build/latex/sye.pdf)** Dieses Buch beinhaltet in strukturierter Version alle nicht-interaktiv darstellbaren Unterlagen. Es wird während des Semesters vervollständigt.

**Danksagung**

Ein herzlicher Dank geht an meine Studierenden, die mit ihren Ideen zur Weiterentwicklung dieser Unterlagen beitragen.

:::{image} https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz/-/raw/master/img/OER4EE_Logos_CC_Lizenz_schmal.jpg
:class: float-left mr-2 rounded
:width: 800px
:::
