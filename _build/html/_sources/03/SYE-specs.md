---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"slideshow": {"slide_type": "fragment"}}

# Anforderungen ermitteln

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>Lernziele für diese Einheit:</b></div>
    <ul>
        <li> Begrifflichkeiten für Energieeffizienz kritisch analysieren </li>
        <li> Kundenwünsche und Stakeholder-Analyse einbeziehen </li>
        <li> Technische Anforderungen beschreiben </li>
        <li> Anforderungen verwalten </li>
    </ul>
</div>

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
---
slideshow:
  slide_type: fragment
tags: [hide-input]
---
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable


print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

+++ {"slideshow": {"slide_type": "fragment"}}

### Grafikparameter einstellen für die Diagramme festlegen:

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

### Funktionen definieren

+++ {"slideshow": {"slide_type": "slide"}}

## Begriff Energieeffizienz

+++

{cite:p}`martin_pehnt_energieeffizienz_2010`

+++ {"slideshow": {"slide_type": "slide"}}

Lateinisch *efficere* = zu Stande kommen, bewirken, durchsetzen, eine Tat ausführen, fertig bringen oder hervor bringen
- es geht um die erzielte Wirkung
- und um ein angemessenes Verhältnis zu den eingesetzten Mitteln

+++

*Ökonomisches Prinzip* in der Wirtschaftslehre: Einsatz von unnützen Mitteln minimieren

+++

**Effizienz** (Wirtschaftlichkeit) vs. **Effektivität** (Wirksamkeit): 
- Effizienz: Mitteleinsatz minimieren *"richtige Dinge richtig tun"*
- Effektivität: erreichtes vs. definiertes Ziel *"die richtigen Dinge tun"*

+++

Energieeffizienz: Energieeinsatz zur Bereitstellung der Dienstleistung (Licht, Wärme, ...) reduzieren

+++

### Energieeffizienz ist kein Selbstzweck

+++

![energieeffizienz-kein-selbstzweck](../img/energieeffizienz-kein-selbstzweck.png)

+++

## Einflussgrößen auf Energieeffizienz

+++

- **wirtschaftliche und gesellschaftliche Randbedingungen**: Bevölkerung, Wirtschaftsentwicklung
- **Aktivitäten und Bedürfnisse**: Wohnfläche, Fahrleistung, TV, ...
- **Qualitäten**: Heizung auf 20 oder 22°C, großer/kleiner TV, Pkw/Bahn ...
- **Technologien**: Standards z. B. Passivhaus, Energieeffizienzklasse ...
- **Umwandlung**: Verluste bei der Bereitstellung der Endenergie ...
- **Lebenszyklen**: alle Energieerzeugungsgeräte und alle Energieverbrauchsgeräte benötigen Herstellungsenergie, d.h. die Lebensdauer und die Wiederverwertungszyklen spielen eine Rolle

+++

## Ursache-Wirkungdiagramm (Ishikawa-Diagramm, Ishikawa = jap. Fishbone)

+++

{cite:p}`birolini_reliability_2010`

+++ {"slideshow": {"slide_type": "slide"}}

![4M im allgemeinen Ishikawa-Diagramm](../img/ishikawa.svg)

+++

### Ishikawa-Diagramm für die Energieeffizienz eines Systems

+++

![Ishikawa für Energieeffizienz](../img/ishikawa-energieeffizienz.svg)

+++

### Ishikawa-Diagramm als Werkzeug für die Systemanalyse

+++

![Ishikawa als Werkzeug für die Systemanalyse](../img/ishikawa-energieeffizienz-systemanalyse.svg)

+++

Die Differenzierung zwischen Haupt- und Nebenursachen ist im ersten Schritt noch nicht so wichtig. Sie könnten also einfach alles erst mal als Hauptursache einsortieren und dann später überlegen, ob bestimmte Haupt-Ursachen eigentlich nur auf andere Hauptursachen der gleichen Kategorie einwirken und daher als Nebenursache eingeordnet werden können.<br><br>
Wenn man es genau nimmt, sind manche Anforderungen an ein System nicht in Stein gemeißelt: Es ist z. B. denkbar, dass das Licht stellenweise dunkler sein darf als 500 lx. Diese Entscheidung obliegt in einem Unternehmen dem Management. Bei Privathaushalten sind es die Nutzer, die letztlich entscheiden, was gut genug ist. Sie können während der Zeit der Durchführung des Projektes natürlich noch an diesen Anforderungen drehen, wenn Sie feststellen, dass eine andere Beschreibung besser ist. Letztlich muss dann allerdings die Energieeffizienzmaßnahme daran gemessen werden, ob die geforderten Anforderungen damit erfüllt werden.

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Erstellen Sie für Ihre Projektaufgabe ein Ishikawa-Diagramm. Versionieren Sie die Datei, damit Sie dokumentieren können, wo Ihr Wissensstand endet, z. B. 2001-04-01-ishikawa-projektA-v01.endung
    
Beispiele für Online-Kollaborations-Werkzeuge (Nutzung auf eigenes Risiko bezüglich Datenschutz und Sicherheit!):
    <ul>
        <li> Um gemeinsam an so einem Diagramm zu arbeiten, können Sie z. B. <a href="https://draw.chat">draw.chat</a> nutzen: Dort ein Whiteboard erstellen und den individuellen Link mit den anderen aus Ihrem Projektteam teilen. Es ist allerdings schwierig, das Diagramm zu einem späteren Zeitpunkt weiterzubearbeiten. Man müsste dann den Screenshot nehmen vom letzten Diagramm, reinkopieren und daran weiterverändern. </li>
        <li> Wenn Sie weiterverändern wollen können Sie auch <a href="https://draw.io">draw.io</a> verwenden. Dieses können Sie an verschiedenen Stellen speichern und dann wieder aufrufen. </li>
        <li> Nicht von mir ausprobiert, aber auch interessant sieht <a href="https://creately.com/">creately.com</a> aus </li>
    </ul>
</div>

+++

*Antwort: ... Notizen ... Ishikawa-Diagramm ... und Link zur bearbeitbaren Version.*

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>Aufgabe:</b></div>
Sehen Sie sich das Ishikawa-Diagramm einer anderen Projektgruppe an und hinterfragen Sie:
    <ul>
        <li> fehlt etwas? </li>
        <li> haben die anderen einen Aspekt, den Sie übersehen haben? </li>
        <li> was sind offene Fragen? </li>
    </ul>
</div>

+++

*Antwort: Notizen dazu ...*

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Erstellen Sie die V02 (Version 2) Ihres Diagramms anhand der gewonnenen Erkenntnisse.
</div>

+++

*Antwort: ... Notizen ... Ishikawa-Diagramm ... und Link zur bearbeitbaren Version.*

+++

## Gefahr Groupthink = *zusammen sind wir dümmer*

+++

<div class="admonition seealso" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>Zum Weiterlesen:</b></div>
{cite:p}`urner_zusammen_2017`
</div>

+++

- **Effekt**: 
 - Meinungsführer formt Meinung
 - Gruppe tauscht keine Informationen mehr aus 
 - und fällt damit schlechtere Entscheidungen
- **Beispiele**: 
 - Swissair-Pleite 2001
 - Fehleinschätzungen US-Wahl 2016
- **Mögliche Abhilfen**: 
 - Stille Vorarbeit, d.h. lieber statt offenem Brainstorming erst mal alle Ideen aufschreiben lassen und dann erst besprechen
 - Experten und / oder erfahrene Teilnehmer einbeziehen
 - Länger diskutieren – dann werden auch neue Infos ausgetauscht
 - Kritik ermutigen, d.h. es geht gerade nicht darum, sich nur gegenseitig zu bestätigen, sondern andere Perspektiven zu sehen
 - Verständnisfragen und kritische Fragen stellen

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Welche Dinge haben Ihnen in früheren Projekten und in früheren Teams geholfen, erfolgreich zu sein?
</div>

+++

*Antwort:...*

+++

## Systembeschreibung

+++

### Grundbegriffe Energie, Arbeit und Leistung Reloaded

+++

**Energie = Fähigkeit, Arbeit zu leisten, Einheit Joule**

+++

**1. Hauptsatz der Thermodynamik: Energie bleibt in einem geschlossenen System konstant.**

+++

Erscheinungsformen von Energie: 
- Wärme
- Kinetische Energie
- Lageenergie
- Elektromagnetische Energie
- Elektromagnetische Strahlung
- Chemische Energie
- Kernenergie

+++

**Arbeit = Energiedifferenz zwischen Zuständen**

+++

z. B. um einen Körper zu beschleunigen: "Kraft mal Weg"

+++

**Leistung (Einheit Watt)**
\begin{equation}
P = \frac{dE}{dt}
\end{equation}

+++

abgeleitete Einheit für die Energie: 
\begin{equation}
1\text{ kWh} = 3600\text{ kWs} = 3,6\text{ MJ}   
\end{equation}

+++

### Systembeschreibung für Energieeffizienzanalysen

+++

![Systemgrenzen](../img/systemgrenzen.svg)

+++

Zur Beschreibung der Energieeffizienz benötigt man die Funktionen des Systems, z. B. 
- Was braucht der Nutzer?
- Was brauchen diejenigen, die das System warten?
- Was braucht derjenige, der das System plant und installiert?
- Was braucht man, wenn das System recyclet werden soll
- Was braucht der Hersteller?

+++

## Voice of the Customer im Prozess der Anforderungsentwicklung

+++

![Vorgehensweise zur Einbindung Voice of the Customer](../img/voice-of-customer.png)

+++

(nach {cite:p}`carulli_approach_2013`)

+++

### Was wollen die Nutzer? Und wer alles sind überhaupt Nutzer?

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Sammeln Sie, wer alles das System nutzt, vielleicht auch nur für kurze Zeit (Verkauf, Installation, Recycling...).
</div>

+++

*Antwort:...*

+++

Echte Nutzer fragen?
- Ihre Freunde und Familie
- Fremde Menschen (den Friseur, in der Bahn, ...)
- Kundenkommentare im Internet zu Haushaltsgeräten
- Herstellerkommentare und Fachforen des Elektrohandels
- Youtube-Werbung ansehen
- Fachgeschäfte oder Messen
- ...

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Suchen Sie nach einigen Quellen von Nutzern und sameln Sie deren Wünsche für Ihr Projekt. Hier steht bewusst nicht Anforderungen sondern Wünsche, denn ein Nutzer hat häufig Wünsche wie "Telefon soll telefonieren", eine technische Anforderung ist jedoch sehr viel spezifischer (s.u.). Dokumentieren Sie auch jeweils die Quelle.
</div>

+++

*Antwort:...*

+++

**Ziel: verstehen, was für Kunde (Geldgeber) und andere Stakeholder zählt**

+++

### Wer sind die Stakeholder?

+++

{cite:p}`balzert_anforderungen_2009`

+++

- direkte Gerätenutzer
 - Anzahl
 - Alter
 - Nutzungszeiten, ggf. unterschiedlich
 - Nutzungsarten und -häufigkeit
 - ...
- Käufer
 - Budget
 - Platzbedarf
 - ...
- Elektrohandel
 - Garantiefälle, etc.
 - Reparaturen
 - Entsorgung (zukünftig Produktverantwortung bis zur Entsorgung möglich)
- weitere:
 - Putzpersonal
 - Gäste
 - Schwiegereltern
 - Nachbarn
 - Hersteller
 - Politik
 - ...

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Sammeln Sie Stakeholder für Ihr Projekt. Nicht alle davon legen Wert auf Energieeffizienz. Das können Sie auch notieren.
</div>

+++

*Antwort: ...*

+++

### Beispiel Weinkühlschrank und Konfliktpotenziale bei Stakeholdern

+++

| **Stakeholder** | **Erwartung** | **Klima/Stimmung** | **Bedeutung/Macht (1..5)** |
|-|-|-|-|
| Inhaber | Will genug Platz für 10 Weinsorten und geringe Kosten | + | 5 |
| Bestimmte Nachbarn | Wollen Wein kritisieren? | - | 3? |
| Hersteller | Viele Weinschränke verkaufen | - | 3 |
| Weinhändler | Guten Ruf behalten | + (wie Inhaber) | 3 |

+++

## Technische Anforderungen formulieren: "Anforderungen an Anforderungen"

+++

{cite:p}`balzert_anforderungen_2009`

+++

- **korrekt**: das ist das, was der Kunde will (passt zu Kundenwunsch)
- **eindeutig**: von allen Stakeholdern gleich interpretiert
- **vollständig**: beschreibt Funktion komplett, wenn nicht: kennzeichnen mit tbd
- **konsistent**: frei von Widersprüchen
- **klassifizierbar nach Wichtigkeit**: essenziell, bedingt, optional
- **überprüfbar**: Messverfahren angeben
- **verfolgbar**: durch eine eindeutige Anforderungsnummer
- **Abhängigkeiten angebbar**: zu anderen Anforderungen mit Nummer
- **modifizierbar**: Änderungen nachverfolgbar (Versionierung, jede Anforderung nur einmal in der Liste)
- **angemessener Umfang**: nicht detaillierter als nötig, lösungsfrei
- **Quelle**: Quelle für Werte angeben (auch „Bauchgefühl Hr. Schmidt“)
- **Konfidenz**: reflektiert den Wissensstand, niedrig, mittel, hoch

+++

### Beispiel normgerechte Beleuchtung von Arbeitsplätzen

+++

| **Nr.** | **Anforderung** | **Wert** | **Einheit** | **Vollständig** | **Wichtigkeit** | **Überprüfbar** |**Abhängige** | **Quelle** | **Konfidenz**| 
|-|-|-|-|-|-|-|-|-|-|
| 1 | Beleuchtungsstärke auf Arbeitsplatte | 500 | lx | ja | essenziell | Luxmeter | ` ` | [BAuA ASRA3.4 Beleuchtung](https://www.baua.de/DE/Angebote/Rechtstexte-und-Technische-Regeln/Regelwerk/ASR/ASR-A3-4.html) | hoch |
| 2 | Farbwiedergabe: guter Kontrast bei grünen Leiterplatten (Lötarbeiten) | ? | ? | nein | hoch | ? | 1 | Bauchgefühl J. May | niedrig |
| 3 | Anzahl LEDs | 42 | Stück | ja | ? | zählen | 1 | hat der Herr Meier gesagt | niedrig |

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Ist Anforderung 3 wirklich eine Anforderung? Oder handelt es sich um eine mögliche technische Umsetzung (die genauso gut auch mit anderer Technologie realisierbar wäre)? Falls es keine Anforderung ist: streichen Sie die Zeile aus der Tabelle, ändern Sie jedoch die Nummern nicht, damit die Abhängigkeiten erhalten bleiben.
</div>

+++

*Antwort:...*

+++

### Dokumentation in Tabellenkalkulation

+++

Da - insbesondere bei umfangreichen Tabellen - jupyter hier nicht das praktischste Werkzeug ist, finden Sie im Link auch eine Vorlage für ein Tabellenkalkulationsprogramm ([libreoffice](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz/-/blob/master/specs/2019-10-03_anforderungsliste_system_XY_beispiel.ods), [excel](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz/-/blob/master/specs/2019-10-03_anforderungsliste_system_XY_beispiel.xlsx)).

+++

Vorgehensweise: 
- **Software**: Tabellenkalkulationsprogramm
- **Erster Schritt**: erst mal das Grundgerüst aufstellen und die ersten Anforderungen eintragen (aus Kundenwünschen und Ishikawa übersetzen) und alles auf _unvollständig_ setzen
- **Nächster Schritt**: essenzielle Anforderungen zuerst mit Werten und Quellen hinterlegen (siehe nächster Schritt Priorisierung)
- **Detailgrad**: nur so detailliert wie nötig! keine xls-Tapete erzeugen!

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Erstellen Sie nun die Anforderungstabelle Version 01 für die Projekte. Erstellen Sie das Grundgerüst anhand der bisher ermittelten Kundenwünsche und vervollständigen Sie die bisher bekannten Anforderungen. Prüfen Sie auch, ob die "Anforderungen an Anforderungen" erfüllt sind. Zuletzt: vereinbaren Sie einen Termin, an dem Sie die Anforderungen erneut vervollständigen, denn Sie werden mehr verstehen und dazulernen.
</div>

+++

*Antwort:...*

+++

### Lernkurve und Anforderungsmanagement

```{code-cell} ipython3
:tags: [hide-input]

zeit = np.linspace(0,10,10000)
lernstand = 1-np.exp(-zeit)
anforderungszahl = 0.8*lernstand
plt.plot(zeit,100*lernstand,label='Lernstand [%]')
plt.plot(zeit,100*anforderungszahl, label='Anforderungsqualität [%]')
plt.xticks([])
plt.xlabel('Lernzeit')
plt.legend(loc='best')
```

## Priorisieren von Kundenwünschen: Kano-Modell

+++

(angelehnt an {cite:p}`raghavan_challenges_2009`)

+++

![kano-Modell](../img/kano-model.svg)

+++

## Priorisieren von Anforderungen

+++

z. B. mithilfe der Methode paarweiser Vergleich

+++

![Beispiel paarweiser Vergleich](../img/paarweiser-vergleich-bsp.png)

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Beginnen Sie nun, Ihre Anforderungen in der Version 01 einmal zu priorisieren. Markieren Sie das Ergebnis in der Spalte Wichtigkeit. Sie werden diese Priorisierung mit sich füllender Anforderungsliste erneut aktualisieren müssen. Dennoch hilft Ihnen die Priorisierung dann schon jetzt dabei, zu entscheiden, welche Recherchen, Messungen, Berechnungen Sie als erstes anpacken. Recherchieren Sie Quellen für die wichtigsten Anforderungen, z. B. Normen und verlinken Sie diese in Ihrer Tabelle. Überlegen Sie auch, welche Dinge Sie in einer Messung überprüfen könnten.
</div>

+++

*Antwort: ...*

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
