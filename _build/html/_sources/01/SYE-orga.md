---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Kursorganisation

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

### Funktionen definieren

+++

## Lernziele für *Systemtechnik für Energieeffizienz*

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>Lernziele für das Modul:</b></div>
    <ul>
        <li> bestehende oder zukünftige Systeme aus Verbrauchern und Erzeugern analysieren, daraus Maßnahmen zur Verbesserung der systemischen Energieeffizienz ableiten und diese bewerten </li>
        <li> mit Messungen, Simulationen / Berechnungen in python und Methoden der Systemtechnik (Anforderungsanalyse, Einfluss-Stärken-Analyse, Kreativitätsmethoden) sowie nachvollziehbarer Dokumentation in einem jupyter notebook </li>
        <li> um als Ingenieur*in im Beruf mit vertretbarem Aufwand Optionen zur energetischen Optimierung von komplexen Systemen (technische Produkte über den Lebenszyklus, Fertigungsverfahren, batteriebetriebene Systeme) zu bewerten </li>
    </ul>
</div>

+++

(termine)=
## Termine und zugehörige Notebooks

+++

<div class="admonition warning" style="background: #fff9e5; padding: 10px">
<div class="title"><b>Achtung: Termine und Dateien werden während des Semesters noch angepasst!</b></div>
    <ul>
        <li> Termine können sich gegenüber der ersten Planung ändern: Bitte achten Sie auf die jeweils aktuelle Version dieser Datei! </li>
        <li> mindestens JUPY Termine: häufig Gruppenarbeit in Projektteams - da ist Mitmachen angesagt (nicht nur "Zuhören") </li>
        <li> Projektaufgabe: Die gültige Version wird auch als pdf im ilias-Kurs abgelegt. </li>
        <li> Alle Dateien: Diese werden während des Semesters eventuell angepasst. Die aktuellste Version finden Sie jeweils auf <a href="https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz">gitlab</a> </li>
        <li> <b>Speichern Sie sich eine Version der Datei unter anderem Namen ab, um Notizen nicht zu verlieren, wenn Sie aktuelle Dateiversionen herunterladen.</b> </li>
    </ul>
</div>

```{code-cell} ipython3
:tags: [hide-input]

# Kenndaten eintragen
no_seminars = 18
no_exercises = 17 # Tutorien
no_labs = 14
seminarstart1 = dt.datetime(2021,10,4,9,45) # Wochentag 1 für erste 90 min.
seminarstart2 = dt.datetime(2021,10,5,8,50) # Wochentag 2 für zweite 90 min.
exercisestart1 = dt.datetime(2021,10,6,9,45) # Tutorientermin 1 für 90 min.
exercisestart2 = dt.datetime(2021,10,8,9,45) # Tutorientermin 2 für 90 min.
lab1start = dt.datetime(2021,10,20,9,45) # Wochentag 1 für Praktikum
lab2start = dt.datetime(2021,10,22,9,45) # Wochentag 2 für Praktikum
projectweekstart = dt.date(2021,11,22) # Montag Projektwoche
noshowdates = [dt.date(2021,12,6), dt.date(2021,12,7), dt.date(2021,10,29), dt.date(2021,11,2), dt.date(2021,11,17), dt.date(2021,11,24), dt.date(2021,12,1), dt.date(2021,12,13), dt.date(2021,12,14), dt.date(2022,1,3), dt.date(2022,1,4), dt.date(2022,1,5), dt.date(2022,1,19), dt.date(2022,1,26), dt.date(2022,1,7)] # Tage, an denen ich nicht kann oder Mittwoche an denen GE1-Praktikum im Labor stattfindet
uni_closed_dates = pd.date_range(start='2021-12-20', end='2022-01-02')
nrw_holidays = holidays.Germany(prov='NW')
pw = pd.date_range(start=projectweekstart,end=projectweekstart+dt.timedelta(4))

# Orte
seminar_loc = 'zoom'
ex_loc = 'zoom'
lab_loc = 'HW2-42'

# Arbeitsform
seminar_type = ['INFO', #1
                'JUPY', #2
                'JUPY', #3
                'JUPY', #4
                'INFO', #5
                'INFO', #6
                'PRES', #7
                'JUPY', #8
                'INFO', #9
                'INFO', #10
                'PRES', #11
                'JUPY', #12
                'INFO', #13
                'INFO', #14
                'PRES', #15
                'PRES', #16
                'PRES', #17
                'INFO'] #18
ex_type  = 'JUPY'
lab_type = 'PRAK'

# Lernziele
seminar_obj = ['Kursorganisation verstehen, Grundbegriffe kennen, Methodik überblicken',
              'python für erste Datenanalyse nutzen, öffentliche Datenquellen kennen',
              'Anforderungen und Systemgrenzen definieren, Energieflüsse abschätzen, Energielogger ausleihen',
              'Daten für Anforderungen kritisch bewerten',            
              'elektrische Energie messen, Messgrößen wiederholen, Messgenauigkeit beurteilen, Normen kennen, Daten in python bereinigen',
              'Thermografie als Messmethode kennen und bewerten',
              'Meilenstein 1: erste Messung mit Energielogger zeigen und erste Analyse der entstandenen Dauerlinie',
              'Datenblätter auslesen, Effizienzen verrechnen, Zeitstempel richtig berechnen',
              'energieeffiziente IT-Systeme vergleichen, Lebenszyklus-Aspekte einbeziehen',
              'Energieeffizienzanalysen kritisch bewerten, Lebenszykluseffekte berücksichtigen, globale und lokale Optimierungen unterscheiden, politische Interessen kennen',
              'Meilenstein 2: erste Einsparmaßnahme modelliert und Effekte gezeigt anhand heatmap oder boxplot',
              'Abwärmenutzungspotenziale kennen, in Fallstudie, Maßnahmen nachvollziehbar bewerten, Wirtschaftlichkeit und ökologischen Nutzen unterscheiden, zeitliche und summarische Effekte unterscheiden',
              'erneuerbare Energien für mehr Energieeffizienz und Nachhaltigkeit einsetzen, Druckluftpotenziale verstehen',
              'Energieeffizienz und Ressourceneffizienz vergleichen, Synergieeffekte und Konflikte verstehen und bewerten',
              'Projektpräsentation',
              'Projektpräsentation',
              'Projektpräsentation',
              'Feedback erhalten, Ablauf der mündlichen Prüfung kennen, Daten für öffentliche Weiterverwendung sichern']
ex_obj= 'an Projekten arbeiten und python Tipps nutzen'
lab_obj = ['Thermografie nutzen', #1
           'Thermografie nutzen', #2
           'Thermografie nutzen', #3
           'Thermografie nutzen', #4
           'Thermografie nutzen', #5
           'Thermografie nutzen (Ersatz)', #6 Ersatz
           'Thermografie nutzen (Ersatz)', #7 Ersatz
           'elektrische Energie messen', #1
           'elektrische Energie messen', #2
           'elektrische Energie messen', #3
           'elektrische Energie messen', #4
           'elektrische Energie messen', #5
           'elektrische Energie messen (Ersatz)', #6 Ersatz
           'elektrische Energie messen (Ersatz)'] #7 Ersatz

# Starttermine generieren
seminartermine = pd.DataFrame()
extermine = pd.DataFrame()
labtermine = pd.DataFrame()

# Termine aneinanderreihen
a1 = pd.date_range(start=seminarstart1, periods=int((no_seminars+1)), freq='7D')
a2 = pd.date_range(start=seminarstart2, periods=int((no_seminars+1)), freq='7D')
a12 = sorted(a1.append(a2))
e1 = pd.date_range(start=exercisestart1, periods = int((no_exercises+1)), freq='7D')
e2 = pd.date_range(start=exercisestart2, periods = int((no_exercises+1)), freq='7D')
e12 = sorted(e1.append(e2))
l1 = pd.date_range(start=lab1start, periods = int((no_labs+1)), freq='7D')
l2 = pd.date_range(start=lab2start, periods = int((no_labs+1)), freq='7D')
l12 = sorted(l1.append(l2))
alldays = sorted(a12 + e12 + l12)

# Projektwoche, Tage ohne Dozent, Feiertage, Schließtage herausnehmen
no_days = []
for d in alldays:
    s = d.date()
    for k in pw:
        t = k.date()
        if s==k:
            no_days.append(d)
    if s in noshowdates or s in nrw_holidays:
        no_days.append(d)
    for k in uni_closed_dates:
        t = k.date()
        if s==k:
            no_days.append(d)
seminartermine['date'] = a12
extermine['date'] = e12
labtermine['date'] = l12
seminartermine['nichtda'] = seminartermine['date'].isin(no_days)
seminartermine
extermine['nichtda'] = extermine['date'].isin(no_days)
labtermine['nichtda'] = labtermine['date'].isin(no_days)
seminartermine = seminartermine[~seminartermine.nichtda]
extermine = extermine[~extermine.nichtda]
labtermine = labtermine[~labtermine.nichtda]
# Auf gewünschte Anzahl Veranstaltungen kürzen
seminartermine = seminartermine.head(no_seminars).reset_index(drop=True).drop(columns=['nichtda'])
extermine = extermine.head(no_exercises).reset_index(drop=True).drop(columns=['nichtda'])
labtermine = labtermine.head(no_labs).reset_index(drop=True).drop(columns=['nichtda'])
seminartermine['Datum'] = seminartermine['date'].dt.strftime('%a %d.%m.%Y %H:%M')
extermine['Datum'] = extermine['date'].dt.strftime('%a %d.%m.%Y %H:%M')
labtermine['Datum'] = labtermine['date'].dt.strftime('%a %d.%m.%Y %H:%M')
# Deadlines für Meilensteine und Projekt
ms1d = a12[6] + dt.timedelta(-6)
ms2d = a12[12] + dt.timedelta(-5)
prd = a12[29] + dt.timedelta(-6)

# Aufgaben zu Deadlines etc.
seminar_todo = ['Anmeldung Projekte in ilias startet, python installieren oder jupyterhub anmelden',
               'Termine python-Tutorium vereinbaren',
               'Formular Energieloggerausleihe in ilias hochladen, Projekte anmelden',
               'PSSO anmelden',
               '-',
               'Meilensteinabgabe in ilias bis '+ms1d.strftime('%a %d.%m.%Y 17:00')+' Präsentationszeit je Team 5 min.',
               'Praktikum Thermografie anmelden',
               '-',
               '-',
               '-',
               'Meilensteinabgabe in ilias bis '+ms2d.strftime('%a %d.%m.%Y 17:00')+' Präsentationszeit je Team 5 min.',
               '-',
               '-',
               'Projekte in ilias abgeben bis '+prd.strftime('%a %d.%m.%Y 17:00'),
               '-',
               '-',
               '-',
               '-']
ex_todo = 'mit Projektteam verabreden'
lab_todo = 'in ilias anmelden, vorbereitet kommen'

# Orte, Ziele, etc. eintragen
seminartermine['Ort'] = seminar_loc
extermine['Ort'] = ex_loc
labtermine['Ort'] = lab_loc
seminartermine['Typ'] = seminar_type
extermine['Typ'] = ex_type
labtermine['Typ'] = lab_type
seminartermine['Ziele'] = seminar_obj
extermine['Ziele'] = ex_obj
labtermine['Ziele'] = lab_obj
seminartermine['Organisatorisch'] = seminar_todo
extermine['Organisatorisch'] = ex_todo
labtermine['Organisatorisch'] = lab_todo

# Kap einfügen
seminartermine.insert(loc=0, column='Kap', value=range(1,no_seminars+1))
extermine.insert(loc=0, column = 'Kap', value='')
labtermine.insert(loc=0, column = 'Kap', value='')

# zusammenfügen
termine = pd.concat([seminartermine, extermine,labtermine], ignore_index=True)
# nach Datum sortieren
termine = termine.sort_values(by='date')
termine = termine.drop(columns=['date'])
# anzeigen
cell_hover = {  # for row hover use <tr> instead of <td>
    'selector': 'td:hover',
    'props': [('background-color', '#ffffb3')]
}
def relative2today(d, color):
    s = pd.to_datetime(d, format='%a %d.%m.%Y %H:%M').date()
    if s < dt.date.today():
        return f"color: {color};"
    else:
        return f"black: {color};"
    
termine.style.hide_index().set_table_styles([cell_hover]).set_properties(**{'text-align': 'left'}).applymap(relative2today, color='gray', subset=['Datum'])
```

## Arbeitsweise und Erwartung an Studierende

+++

Für jeden Termin gibt es ein oder mehrere Abschnitte im jupyter book. Diese lassen sich auch ausführbar als `.ipynb` aufrufen. Dort können Sie dann Werte und Code selbst eintragen, die Notebooks neu ausführen und Energiedatenanalysen selbst betreiben. Ebenso können Sie die Dateien als `.pdf` drucken. Zudem gibt es die Unterlagen als [pdf-Skript](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz/-/blob/master/_build/latex/sye.pdf).

+++

<div class="admonition warning" style="background: #fff9e5; padding: 10px">
<div class="title"><b>Achtung!</b></div>
Insbesondere diejenigen Abschnitte im Skript bzw. die zugehörigen Notebooks, die noch nicht dran waren, können sich während des Semesters ändern. Bitte laden Sie sich die für den jeweiligen Termin aktuellen Unterlagen neu herunter.
</div>

+++

Sie benötigen entweder eine funktionierende Installation mit python > 3.6 und jupyter. Ein Anleitungsvideo mit der anaconda-Distribution finden Sie unter HIER LINK EINFÜGEN.

+++

Alternativ zur eigenen Installation: Seit dem Wintersemester 2021-2022 existiert an der TH Köln als Pilotinstanz ein [jupyterhub](https://jupyterhub.th-koeln.de). Diesen erreichen Sie, wenn Sie sich im Netz der TH befinden, hierfür müssen Sie sich ggf. erst mit [VPN](https://www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php) verbinden. Wir schalten diejenigen frei, die im aktuellen ilias-Kurs des Semesters als Mitglieder angemeldet sind. Dort ist python und alle hier benötigten Module installiert. Sie können also auch "in der Cloud" damit arbeiten.

+++

Es gibt Seminartermine, bei denen ich anwesend bin. Außerdem vereinbaren Sie zusätzlich selbständig Termine in Ihren Projektteams zur Erstellung der Energiedatenanalyse Ihres Projektes in einem eigenen jupyter notebook.

+++

Für die Seminartermine **bereiten Sie vor**:
- Bereiten Sie die jupyter notebooks vom vergangenen Termin nach und lösen Sie das, was Sie mithilfe der zusätzlichen Erklärungen im Seminartermin nun lösen können.
- Verschaffen Sie sich einen Überblick über die jupyter notebooks des nächsten Termins. Führen Sie probeweise diese aus und notieren Sie Fragen. Gerne können Sie Ihre Fragen auch im Forum stellen!

+++

Es gibt drei Arten von Seminarterminen:
- **INFO = interaktives Seminar**: für Informationen zu einem Teilgebiet der Energieeffizienz, Interaktion mit Einschätzungsfragen für Sie und Platz für Ihre vorbereiteten Fragen
- **JUPY = Notebooks in Kleingruppen durchführen**: für interaktives Beschäftigen mit Zusammenhängen in einem Teilgebiet der Energieeffizienz und das Einüben der Arbeit mit jupyter notebooks, ich komme zu den Kleingruppen (meist Projektteams) mit Hilfestellung dazu. Es funktioniert *nur, wenn Sie aktiv mitmachen*, nicht, wenn Sie nur zuhören!
- **PRES = Präsentation**: Sie präsentieren Zwischenergebnisse (bei Meilensteinterminen) oder Endergebnisse Ihrer Projektarbeit

+++

Zudem gibt es Praktika **PRAK**, d.h. zumeist im Labor eigenständiges, angeleitetes Durchführen von Messungen. Diese **bereiten Sie bitte vor**, indem Sie mit Ihrer Projektgruppe einen Versuchsplan absprechen, die zugehörigen theoretischen jupyter notebooks vorab durcharbeiten und die in der Anleitung genannten Hilfsmittel mitbringen.

+++

<div class="admonition warning" style="background: #fff9e5; padding: 10px">
    <div class="title"><b>Achtung Corona-Semester Winter 2021-2022</b></div>
Im Praktikum werden Messgeräte zu zweit verwendet werden. Es stehen Handschuhe bereit, um die Messgeräte zu bedienen und Touchstifte, so dass Sie auch Touchdisplays nicht mit bloßen Händen anfassen müssen. Um Quarantäne-Ausfälle zu minimieren, ist nur der jeweils letzte Praktikumstermin für Getestete vorgesehen. An den anderen Terminen dürfen nur Geimpfte und Genesene teilnehmen. Alle, die eine Impfung aus organisatorischen Gründen nicht geschafft haben, finden auf <a href="https://www.th-koeln.de/corona">th-koeln.de/corona</a> Infos zu Impfmöglichkeiten. Sollten Sie Bedenken oder Fragen aufgrund Ihrer persönlichen Situation zu diesem Vorgehen haben, bitte ich Sie, sich <b>frühzeitig</b> bei mir in der <a href="https://ilias.th-koeln.de/goto.php?target=crs_998134&client_id=ILIAS_FH_Koeln">Sprechstunde</a> zu melden. Dann können wir Lösungen suchen.
</div>

+++

Ob live-Termine vor Ort stattfinden, oder per Videokonferenz steht im Abschnitt [Termine und zugehörige Notebooks](termine)

+++

## Prüfungsleistungen

+++

50% der Note erhalten Sie für das **Projekt**: Dieses Semester geht es um [*den Einfluss von Reparierbarkeit auf Energielabel-Geräte*](../proj/2021_SYE_projektaufgabe.ipynb). Während des Semesters arbeiten Sie in Ihren Projektteams am Projekt. Zwischenstände berichten Sie in den beiden **Meilenstein-Terminen**.

+++

50% der Note erhalten Sie für die **mündliche Prüfung**. Die mündliche Prüfung dauert 15 min. Sowohl Diagramme und Grafiken aus den Kursmaterialien als auch aus Ihren Projektpräsentationen können dran kommen. Es geht darum, zu erklären, was man auf den Diagrammen sieht, was das mit Energieeffizienz zu tun hat und auf welche Aspekte besonders zu achten ist. Die erste Frage dürfen Sie sich selbst aussuchen aus der Auswahl an Bildern in der Prüfung. Die zweite Frage wird mit einem Zufallsgenerator ausgewählt. Die dritte Frage wähle ich so aus, dass ggf. fehlende Themenfelder noch abgefragt werden können.

+++

Ist eine der Teilleistungen mit 5,0 bewertet, so wird das gesamte Modul mit 5,0 benotet.

+++

Sollten Sie das Projekt bestanden haben und die mündliche Prüfung wiederholen müssen, so ist dies im folgenden Sommersemester möglich. Die Projektnote bleibt erhalten. Länger zu warten, empfehle ich nicht, da sich in jedem Wintersemester neue Projektthemen ergeben und auch die anderen Inhalte aktualisiert werden, so dass Sie sich erneut tiefer mit dem Modul befassen müssen, um die mündliche Prüfung dann gut zu meistern.

+++

## Weiterführendes

+++

... finden Sie im Anhang des jupyter book

+++

z. B. Quellen für öffentlich verfügbare Energiedaten sowie Tipps zur Zusammenarbeit mit git

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
