---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Systemtechnische Vorgehensweise zur Verbesserung der Energieeffizienz

+++

- [ ] VIDEOLINK EINFÜGEN (VIMEO)

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rcParams
import pandas as pd
import datetime as dt
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import os.path
import platform
from PIL import Image
import requests
import scipy
from scipy.optimize import curve_fit
from IPython.display import Audio
from distutils.spawn import find_executable

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('platform', platform.__version__)
print('Image', Image.__version__)
print('requests', requests.__version__)
print('scipy', scipy.__version__)
```

### Grafikparameter einstellen

```{code-cell} ipython3
plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

### Funktionen definieren

+++

## Lernziele

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
    <ul>
        <li> Übersicht über die Methodik </li>
        <li> Iteratives Vorgehen nachvollziehen </li>
        <li> Effekte abschätzen mithilfe von Berechnungen und Messungen </li>
    </ul>
</div>

+++

## Überblick über die Vorgehensweise

+++

1. Anforderungen analysieren
2. Systemgrenzen festlegen
3. Einflussstärken von Effekten analysieren
4. Lösungen finden
5. Anhand der Anforderungen analysieren und bewerten

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><i>Beispiel</i></div>
<b>Tasse Tee</b>
</div>

+++

<div class="admonition seealso" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>SIEHE AUCH:</b></div>
ähnliche Vorgehensweisen finden sich in der Produktentwicklung auch, wenn man andere Ziele als die Energieeffizienz verfolgt, z. B. gibt es "Design for Six Sigma", also ein Produktdesign, bei dem nur im Bereich von wenigen ppm Ausschuss entsteht. Diese Methodik findet sich in der VDI 2221. Besonders zum Schritt 4 <i>Lösungen finden</i> bietet auch die VDI 2222 einige Anregungen.
</div>

+++

### Anforderungen analysieren

```{code-cell} ipython3
teatype = 'greentea'
Ttea = 100 # °C
Vcup = 300 # ml
V = 500 # ml
```

Meist brühe ich mir eine kleine Kanne mit etwas mehr Volumen $V>V_{cup}$ als die Tasse hat. $\to$ Beim drüber Nachdenken ändern sich Anforderungen und es ist sinnvoll die Liste entsprechend anzupassen.

+++

### Systemgrenzen festlegen

```{code-cell} ipython3
Troom = 25 # °C
```

Hier festgelegt: was kann **ich** tun, um den Energiebedarf für die Tasse Tee zu optimieren? (außen vor: Transportwege für Tee, Effizienz der elektrischen Energieversorgung für den Wasserkocher, Teesorte)

```{code-cell} ipython3
Tcoldwater = 10 # °C
```

### Einflussstärken von Effekten analysieren

+++

![Effekte brainstormen im Ishikawa Diagramm](../img/Ishikawa.jpg)

+++

**Methode des physikalischen Optimums VDI 4663: ermitteln, wie viel Energie mindestens physikalisch nötig ist, dann nachmessen und dadurch Potenziale ermitteln**

+++

- spezifische Wärmekapazität von Wasser $c_{H_2O}$
- Dichte von Wasser $\rho_{H_2O}$ hier ungefähr angenommen (Temperaturabhängigkeit vernachlässigt)

```{code-cell} ipython3
ch2o = 4.19 # kJ/kg.K
rhoh2o = 1 # kg/l (ca.)
```

```{code-cell} ipython3
Wmin = ch2o*1e3*rhoh2o*(Ttea-Tcoldwater)*V*1e-3
print(str(int(np.round(1e-3*Wmin,0)))+' kJ')
```

= Mindestens nötige Energie für das Aufheizen des Wassers.

+++

Umrechnen in Wh

```{code-cell} ipython3
Wminkwh = Wmin/3600
print(str(int(np.round(Wminkwh,0)))+' Wh')
```

```{code-cell} ipython3
mkettle = 550 # g (gemessen)
Tcoldwaterroom = 25 # °C (gemessen)
Wkettle = 56.484 # Wh
```

(alles gemessene Werte)

```{code-cell} ipython3
Thotwater = 97 # °C (gemessen)
```

Messung und Berechnung vergleichen:

+++

Temperaturdifferenz Berechnung:

```{code-cell} ipython3
deltaTcalc = Ttea - Tcoldwater
print(str(deltaTcalc)+' K')
```

Temperaturdifferenz Messung:

```{code-cell} ipython3
deltaTmeas = Thotwater - Tcoldwaterroom
print(str(deltaTmeas)+' K')
```

$\to$ beide Temperaturdifferenzen sind fast gleich groß - ausreichend für Vergleich

```{code-cell} ipython3
mkettlehot = 1011 # g
vapor = mkettle + V - mkettlehot
print(str(vapor)+' g')
```

verdampfte Wassermenge

+++

eigentlich nur aufgeheizt:

```{code-cell} ipython3
mhotwater = V - vapor
print(str(mhotwater)+' g')
```

um wirklich $V$ aufgeheizt zu erhalten, müsste man mehr aufheizen

```{code-cell} ipython3
WkettleV = Wkettle*V/mhotwater
print(str(int(np.round(WkettleV,0)))+' Wh')
```

$\to$ theoretisch mögliches Energiesparpotenzial:

```{code-cell} ipython3
Wsavings = WkettleV - Wminkwh
print(str(int(np.round(Wsavings,0)))+' Wh')
```

prozentual:

```{code-cell} ipython3
Wsavingspercent = Wsavings/Wminkwh
print(str(int(np.round(100*Wsavingspercent,0)))+'%')
```

Brainstorming:
- Verluste in der elektrischen Leitung
- Verluste durch verdampfendes Wasser
- Verluste durch Wasserkocherwände

+++

Verluste z. B. durch Berechnen abschätzen

```{code-cell} ipython3
lcable = 1 # m
Acable = 0.75 # mm² 
kappaCu = 58e6 # S/m
```

Querschnitt der Leitung aus Kabelaufdruck: 0,75 mm²

+++

für geschlossenen Stromkreis Hin- und Rückleitung nötig: Faktor 2

```{code-cell} ipython3
RL = 2*(1/kappaCu)*lcable/(Acable*1e-6)
print(str(np.round(RL,3))+' Ohm')
```

Nennleistung aus Typenschild

```{code-cell} ipython3
PN = 1000 # W
UN = 230 # V
IN = PN/UN
print(str(np.round(IN,2))+' A')
```

Strom fließt über Leitungswiderstand und erwärmen ihn.
Leitungsverluste:

```{code-cell} ipython3
PV = RL*IN**2
print(str(np.round(PV,2))+' W')
```

viel oder wenig?

+++

Kochdauer etwa 3,5 Minuten

```{code-cell} ipython3
tboil = 3.5*60 # s
```

Energieverluste in Leitung während Kochvorgang:

```{code-cell} ipython3
Wboil = tboil*PV/3600
print(str(np.round(Wboil,3))+' Wh')
```

vergleichen mit Energie zum Aufheizen des Wassers:

```{code-cell} ipython3
Wboilpercent = Wboil/Wminkwh
print(str(np.round(100*Wboilpercent,2))+'%')
```

$\to$ sehr geringe Verluste - vernachlässigbar

+++

zweites Beispiel: Wasserdampfverluste: aus Messung

```{code-cell} ipython3
mvaporpercent = (V-mhotwater)/V
print(str(100*mvaporpercent)+'%')
```

das ist schon eine andere Größenordnung an Verlusten, ggf. nicht mehr vernachlässigbar

+++

so kann man das mit allen Verlusten tun: 
- vernachlässigbare Effekte vernachlässigen
- stärkere Effekte im nächsten Schritt genauer betrachten, um Lösungen abzuleiten

+++

### Lösungen finden

+++

Beispiele:
- Wasserkocher isolieren $\to$ deutlich reduzierte Wärmeverluste
- Tee bei niedrigerer Temperatur als $T_{tea}$ brühen
- Gemeinschaftskocher $\to$ geringere Oberflächenverluste
- Wasserdampf nutzen

+++

{cite:p}`heinz-kill_physical_2019` hat gemessen, was ein Gemeinschaftskocher bringen kann:

```{code-cell} ipython3
PNWK = 2400 # Nennleistung Wasserkocher 2.4 kW
VWK = 1 # 1 l Fassungsvermögen
mH2O = [102,473,823] # Masse des erhitzten Wassers in g
etagemessen = [27,68,77] # Wirkungsgrad bezogen auf reine für Aufheizen nötige Energie
eta102 = [27,12,8] # bezogen auf 102 g Wasser, Rest umsonst
plt.plot(mH2O,etagemessen,'o--',label='Wirkungsgrad')
plt.plot(mH2O,eta102,'o--',label='Wirkungsgrad für 102 g')
plt.text(0,-35,r'Sina Kurth, Jens Reifschneider, Maximilian Heinz-Kill Physikalische Grenzen der Energieeffizienz, Masterprojekt 2018-2019',color='gray')
plt.title(r'Messung an 2,4 kW Wasserkocher mit 1 l Fassungsverm\"ogen')
plt.axis([0,1000,0,100])
plt.xlabel('Wasservolumen [ml]')
plt.ylabel('Wirkungsgrad [\%]')
```

### Anhand der Anforderungen analysieren und bewerten

+++

falls Kosten wichtig: Amortisation der Maßnahme bewerten, z. B. isolierter Wasserkocher den Einsparungen gegenüberstellen

+++

![amortisation](../img/Amortisation.jpg)

+++

mit Nutzer besprechen: kälter brühen OK?

+++

nein, denn Wasser soll immer erst kochen aus hygienischen Gründen

+++

neues $T_{tea}$ festlegen und nochmals durchgehen

+++

sind Annahmen weiterhin OK?

+++

iterativer Prozess:

+++

![iterativerProzess](../img/Iterationen_Modell_1.jpg)

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Prüfen Sie, was bei neuer T_tea passiert.
</div>

+++

_Antwort: ..._

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
