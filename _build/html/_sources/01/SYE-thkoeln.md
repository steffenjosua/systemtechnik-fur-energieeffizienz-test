---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

+++ {"slideshow": {"slide_type": "fragment"}}

# Der Kurs an der TH Köln

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>Lernziele für diese Einheit:</b></div>
    <ul>
        <li> Vorstellung Dozentin und Studierende </li>
        <li> Arbeitsweise: Projekte, Exkursion, python </li>
        <li> <i>vorläufige</i> Termine in <a href='https://ilias.th-koeln.de'>ilias</a> - laufende Änderungen beachten! </li>
        <li> Öffentliche Unterlagen in <a href='https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz'>gitlab</a> </li>
        <li> Anmeldung zu Praktika, Exkursion, Präsentations- und Prüfungsterminen in ilias: F07 &#8594; May &#8594; SYE &#8594; Passwort SYE2122 </li>
        <li> Abgabe von Dateien ebenfalls in ilias </li>
        <li> Diskussion von Fragen in den Veranstaltungen und im Forum in ilias </li>
    </ul>
</div>

+++ {"slideshow": {"slide_type": "slide"}}

## Vorstellung Dozentin

+++ {"slideshow": {"slide_type": "fragment"}}

### Studium der Elektrotechnik an der Uni Ulm

+++

```{figure} https://upload.wikimedia.org/wikipedia/commons/4/46/Ulm_Donauschwabenufer1.jpg
---
width: 400px
name: fig-Ulm
---
Ulm (Source: Candidus, Wikimedia Commons, CC0)
```

+++ {"slideshow": {"slide_type": "slide"}}

### Auslandsstudium in Sydney

+++ {"slideshow": {"slide_type": "fragment"}}

<a title="Christine Leist, Public domain, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Sydney_harbourbridge%2Boperahouse.JPG"><img width="512" alt="Sydney harbourbridge+operahouse" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Sydney_harbourbridge%2Boperahouse.JPG/512px-Sydney_harbourbridge%2Boperahouse.JPG"></a>
<p>
    Bild: Sydney Harbour Bridge und Opernhaus, Christine Leist, Public domain, via Wikimedia Commons </p>

+++ {"slideshow": {"slide_type": "slide"}, "heading_collapsed": true}

## Berufserfahrung

+++ {"slideshow": {"slide_type": "fragment"}, "hidden": true}

### Promotion in der Mikrosystemtechnik
{cite:p}`may_konzeption_2011`

+++ {"slideshow": {"slide_type": "fragment"}, "hidden": true}

![Integration von MEMS in Polymer](../img/promotion_jmay.jpg)

+++ {"slideshow": {"slide_type": "slide"}, "hidden": true}

### Projektleitung Optimale Photovoltaik-Heimspeicher
{cite:p}`enargus_verbundvorhaben_2013` {cite:p}`speicher_speicher_2014`

+++ {"slideshow": {"slide_type": "fragment"}, "hidden": true}

![Öffentlich geförderter Anteil des Projektes zu Solar-Heimspeichern bei der Robert Bosch GmbH](https://www.saving-volt.de/wp-content/uploads/2014/05/bosch-photovoltaik-energy1.jpg) Bild: {cite:p}`danielb_forschungsprojekt_2014`

+++ {"slideshow": {"slide_type": "slide"}, "hidden": true}

- 1999-2005 Studium der Elektro- und Informationstechnik an der Uni Ulm
- 2003 Auslandsstudium in Australien
- 2004 Auslandspraktikum in Madagaskar
- 2005-2008 Promotion in Mikrosystemtechnik an der Uni Freiburg, durchgeführt bei der Robert Bosch GmbH
- 2008-2016 u.a. Photovoltaik-Systemtechnik und Batteriespeicher-Forschung bei der Robert Bosch GmbH
- 2016-2017 Forschungsförderung für erneuerbare Energien beim Projektträger Jülich
- 2017-heute Professorin an der TH Köln

+++ {"slideshow": {"slide_type": "slide"}}

## Kontakt zur Dozentin

+++ {"slideshow": {"slide_type": "fragment"}}

Aktuelle Kontaktdaten auf der TH-Homepage:
- [Prof. Dr. Johanna May](https://www.th-koeln.de/personen/johanna.may/)
- fachliche und organisatorische Fragen? Gerne gemeinsam diskutieren und beantworten: 
 - in der live-Veranstaltung
 - im ilias-Forum
- persönliche Fragen? Sprechstunde: 
 - Termine + Anmeldung siehe [ilias](https://ilias.th-koeln.de)
 - Buchung: in ilias in einem meiner Kurse unterhalb vom Minikalender auf Sprechstunde für Prof. May klicken, dann einen Zeitslot auswählen, dann prüfen ob richtig gebucht: im Termin ist automatisch ein zoom-Link drin, wenn nicht, bitte erneut probieren (Termine in Ihrem privaten ilias-Kalender kann ich nicht sehen)

+++ {"slideshow": {"slide_type": "slide"}}

### Persönliche Vorstellung

+++

für 5 min. in zufälligen Breakout-Rooms mit Cryptpad (Link wie Raumnummer!)
1. [Cryptpad Raum 1](https://cryptpad.fr/pad/#/2/pad/edit/T-CaG5+5bMm4DG-yo5abHSot/)
2. [Cryptpad Raum 2](https://cryptpad.fr/pad/#/2/pad/edit/NWcoJzo-TGnz9Y2XmyltN6L-/)
3. [Cryptpad Raum 3](https://cryptpad.fr/pad/#/2/pad/edit/EVP-ok9x1gQJSqKPQe7BJVUi/)
4. [Cryptpad Raum 4](https://cryptpad.fr/pad/#/2/pad/edit/Zqg53OkZ5r-UPl8qhokXT33z/)
5. [Cryptpad Raum 5](https://cryptpad.fr/pad/#/2/pad/edit/o-wOYQt-ZULCuorMYc+WcxM-/)

+++ {"slideshow": {"slide_type": "fragment"}}

- Vorname und Nachname
- Studiengang
- Was sind Ihre Erfahrungen mit Energieeffizienz, ggf. in beruflichen Umfeldern?
- Was erwarten Sie sich von diesem Modul?

+++

*Für Dozenten: alternative Fragen für Vorstellungsrunden:*
1. Wann haben Sie das letzte Mal ein Produkt oder eine Dienstleistung gekauft, hauptsächlich deswegen, weil Sie sich davon mehr Energieeffizienz versprochen haben?
2. Wie hoch ist Ihr jährlicher Stromverbrauch?
3. Welchen Spritverbrauch / Stromverbrauch hat Ihr Auto?
4. Welche Energieeffizienzklasse hat Ihre Waschmaschine?
5. Wie verringern Sie Ihren Energieverbrauch?

+++

<div class="admonition hint" style="background: #e9f6ec; padding: 10px">
<div class="title"><bTipp:</b></div>
Weiterführendes Interview zu Energieeffizienz (podcast): <a href="https://www.enpower-podcast.de/podcast/05-energieeffizienz-dr-clemens-rohde">enPower - Der Energiewende Podcast: #05 Energieeffizienz - Dr. Clemens Rohde</a> {cite:p}`julius_wesche_05_2020`
</div>

+++ {"slideshow": {"slide_type": "slide"}, "heading_collapsed": true}

## Meine Rolle als Dozentin und meine Erwartung an Sie

+++ {"slideshow": {"slide_type": "fragment"}, "hidden": true}

**Meine Rolle:**
- inhaltliche Schwerpunkte setzen
- Ihre Projektarbeiten begleiten mit Hinweisen und Feedback
- Ihren Lernfortschritt bewerten
- Nicht: alles wissen
- offen für Feedback: kritisches Mitdenken und Einbringen von Erfahrungen und Wissen herzliche willkommen!

+++ {"slideshow": {"slide_type": "slide"}, "hidden": true}

**Meine Erwartungen:**
- selbständige Projektarbeit in Projektteams
 1. Teamtermine eigenständig organisieren
 2. bei Konflikten im Team ansprechen und klären
 3. wenn 2. nicht funktioniert, mit mir sprechen
- Praktikum: vorbereitet erscheinen und eigene Ideen / Fragen mitbringen
- Exkursion: pünktlich erscheinen, angemessenes Auftreten (Außenwirkung, ggf. zukünftige Masterarbeitsthemen für manche), Interesse mit Fragen und Aufmerksamkeit zeigen

+++ {"slideshow": {"slide_type": "slide"}, "heading_collapsed": true}

## Zugang zu Literatur an der TH Köln

+++ {"slideshow": {"slide_type": "fragment"}, "hidden": true}

ins Netz der TH Köln kommen Sie mit dem cisco VPN Client: [Installationsanleitung der TH](https://www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php)

+++ {"slideshow": {"slide_type": "fragment"}, "hidden": true}

im Netz der TH haben Sie Zugriff auch auf einiges an digitalen Literaturdokumenten: 
- [springerlink](https://www.springerlink.com) $\to$ preview-Content nicht anklicken (den hat die TH nicht lizensiert), dann Stichwortsuche, u.a. gibt es dort das Journal of Energy Efficiency mit vielen aktuellen Fachartikeln
- weitere e-books sowie auch Aufsätze (Papers) finden Sie auch über die [Hochschulbibliothek](https://thb-koeln.digibib.net/search/katalog/list?start=1&defaults=on&branch=0&q-al=energieeffizienz), dort lassen sich auch online-Medien filtern
- [IEEEXPlore](https://ieeexplore.ieee.org/Xplore/home.jsp) - wissenschaftliche Literatur zu vielen technischen Themen
- [wiso-net](https://www.wiso-net.de) - deutsche Fachzeitschriften, z. B. Artikel aus der c't oder zu Kunststoffverarbeitung und sehr vielen weiteren Fachgebieten
- [perinorm](https://www.perinorm.com) - VDI-Richtlinien und ISO-Normen
- [Normenbibliothek](https://www.normenbibliothek.de) - VDE-Normen

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
