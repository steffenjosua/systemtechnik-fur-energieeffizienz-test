\select@language {english}
\contentsline {part}{I\hspace {1em}Kursmaterial}{3}{part.1}
\contentsline {chapter}{\numberline {1}Einführung und Kursorganisation}{5}{chapter.1}
\contentsline {section}{\numberline {1.1}Der Kurs an der TH Köln}{6}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Vorstellung Dozentin}{6}{subsection.1.1.1}
\contentsline {subsubsection}{Studium der Elektrotechnik an der Uni Ulm}{6}{subsubsection*.3}
\contentsline {subsubsection}{Auslandsstudium in Sydney}{7}{subsubsection*.4}
\contentsline {subsection}{\numberline {1.1.2}Berufserfahrung}{7}{subsection.1.1.2}
\contentsline {subsubsection}{Promotion in der Mikrosystemtechnik}{7}{subsubsection*.5}
\contentsline {subsubsection}{Projektleitung Optimale Photovoltaik\sphinxhyphen {}Heimspeicher}{8}{subsubsection*.6}
\contentsline {subsection}{\numberline {1.1.3}Kontakt zur Dozentin}{9}{subsection.1.1.3}
\contentsline {subsubsection}{Persönliche Vorstellung}{9}{subsubsection*.7}
\contentsline {subsection}{\numberline {1.1.4}Meine Rolle als Dozentin und meine Erwartung an Sie}{10}{subsection.1.1.4}
\contentsline {subsection}{\numberline {1.1.5}Zugang zu Literatur an der TH Köln}{10}{subsection.1.1.5}
\contentsline {subsection}{\numberline {1.1.6}Literatur}{10}{subsection.1.1.6}
\contentsline {section}{\numberline {1.2}Systemtechnische Vorgehensweise zur Verbesserung der Energieeffizienz}{11}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}python konfigurieren}{11}{subsection.1.2.1}
\contentsline {subsubsection}{Module importieren}{11}{subsubsection*.8}
\contentsline {subsubsection}{Grafikparameter einstellen}{12}{subsubsection*.9}
\contentsline {subsubsection}{Funktionen definieren}{12}{subsubsection*.10}
\contentsline {subsection}{\numberline {1.2.2}Lernziele}{12}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Überblick über die Vorgehensweise}{12}{subsection.1.2.3}
\contentsline {subsubsection}{Anforderungen analysieren}{13}{subsubsection*.11}
\contentsline {subsubsection}{Systemgrenzen festlegen}{13}{subsubsection*.12}
\contentsline {subsubsection}{Einflussstärken von Effekten analysieren}{14}{subsubsection*.13}
\contentsline {subsubsection}{Lösungen finden}{17}{subsubsection*.14}
\contentsline {subsubsection}{Anhand der Anforderungen analysieren und bewerten}{18}{subsubsection*.15}
\contentsline {subsection}{\numberline {1.2.4}Literatur}{20}{subsection.1.2.4}
\contentsline {section}{\numberline {1.3}Physikalische Grenzen der Energieeffizienz}{20}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}python konfigurieren}{20}{subsection.1.3.1}
\contentsline {subsubsection}{Module importieren}{20}{subsubsection*.16}
\contentsline {subsubsection}{Grafikparameter einstellen}{21}{subsubsection*.17}
\contentsline {subsubsection}{Funktionen definieren}{21}{subsubsection*.18}
\contentsline {subsection}{\numberline {1.3.2}Lernziele}{21}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Richtlinie VDI 4663: Bewertung von Energie\sphinxhyphen {} und Stoffeffizienz}{21}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}Literatur}{23}{subsection.1.3.4}
\contentsline {section}{\numberline {1.4}Energiekenngrößen}{23}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}Lernziele}{23}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}Grundbegriffe nach VDI 4661}{23}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}Literatur}{24}{subsection.1.4.3}
\contentsline {section}{\numberline {1.5}Systemtechnische Methoden}{24}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}Motivation}{26}{subsection.1.5.1}
\contentsline {subsubsection}{Systemtechnik: Methoden, um systematisch mit Komplexität umzugehen}{26}{subsubsection*.20}
\contentsline {subsection}{\numberline {1.5.2}Phasenmodell: Von der Idee zur Umsetzung}{27}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}Charakter einer Vorstudie}{28}{subsection.1.5.3}
\contentsline {subsection}{\numberline {1.5.4}Sensitivitätsanalysen \sphinxhyphen {} wozu?}{29}{subsection.1.5.4}
\contentsline {subsubsection}{Sensitivität}{29}{subsubsection*.21}
\contentsline {section}{\numberline {1.6}Literatur}{31}{section.1.6}
\contentsline {section}{\numberline {1.7}Kursorganisation}{31}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}python konfigurieren}{31}{subsection.1.7.1}
\contentsline {subsubsection}{Module importieren}{31}{subsubsection*.22}
\contentsline {subsubsection}{Grafikparameter einstellen}{32}{subsubsection*.23}
\contentsline {subsubsection}{Funktionen definieren}{32}{subsubsection*.24}
\contentsline {subsection}{\numberline {1.7.2}Lernziele für \sphinxstyleemphasis {Systemtechnik für Energieeffizienz}}{32}{subsection.1.7.2}
\contentsline {subsection}{\numberline {1.7.3}Termine und zugehörige Notebooks}{32}{subsection.1.7.3}
\contentsline {subsection}{\numberline {1.7.4}Arbeitsweise und Erwartung an Studierende}{32}{subsection.1.7.4}
\contentsline {subsection}{\numberline {1.7.5}Prüfungsleistungen}{33}{subsection.1.7.5}
\contentsline {subsection}{\numberline {1.7.6}Weiterführendes}{34}{subsection.1.7.6}
\contentsline {subsection}{\numberline {1.7.7}Literatur}{34}{subsection.1.7.7}
\contentsline {section}{\numberline {1.8}Verhalten im Labor}{34}{section.1.8}
\contentsline {subsection}{\numberline {1.8.1}Lernziele für diesen Abschnitt}{34}{subsection.1.8.1}
\contentsline {subsection}{\numberline {1.8.2}SARS\sphinxhyphen {}CoV\sphinxhyphen {}2 Besonderheiten}{34}{subsection.1.8.2}
\contentsline {subsection}{\numberline {1.8.3}Sicherheitshinweise für das Praktikum}{34}{subsection.1.8.3}
\contentsline {subsubsection}{Pünktlich mit Unterschrift}{35}{subsubsection*.26}
\contentsline {subsubsection}{Bei Krankheit}{35}{subsubsection*.27}
\contentsline {subsubsection}{Verhalten im Labor}{35}{subsubsection*.28}
\contentsline {subsubsection}{Not\sphinxhyphen {}Aus}{35}{subsubsection*.29}
\contentsline {subsubsection}{Bei Unfällen oder Brand}{36}{subsubsection*.30}
\contentsline {subsubsection}{Während der Versuchsdurchführung}{36}{subsubsection*.31}
\contentsline {subsection}{\numberline {1.8.4}Literatur}{37}{subsection.1.8.4}
\contentsline {chapter}{\numberline {2}Mit python Energiedaten analysieren}{39}{chapter.2}
\contentsline {section}{\numberline {2.1}Kleine python Einführung}{39}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}python installieren}{39}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Jupyter Hacks und Ihr erstes Jupyter Notebook}{39}{subsection.2.1.2}
\contentsline {subsubsection}{Interaktiv verwenden}{40}{subsubsection*.33}
\contentsline {subsubsection}{Shift\sphinxhyphen {}Enter}{40}{subsubsection*.34}
\contentsline {subsubsection}{Was kann ein Jupyter Notebook eigentlich alles?}{41}{subsubsection*.35}
\contentsline {subsubsection}{Dashboard}{41}{subsubsection*.36}
\contentsline {subsubsection}{Neues Notebook erstellen}{42}{subsubsection*.37}
\contentsline {subsubsection}{.ipynb Datei}{42}{subsubsection*.38}
\contentsline {subsubsection}{Notebook Ansicht}{42}{subsubsection*.39}
\contentsline {subsubsection}{Zellen (Cells) und Kernel}{43}{subsubsection*.40}
\contentsline {subsubsection}{Zellen}{43}{subsubsection*.41}
\contentsline {subsubsection}{Hello World}{43}{subsubsection*.42}
\contentsline {subsubsection}{Speichern und Checkpoint}{43}{subsubsection*.43}
\contentsline {subsubsection}{Neue Zelle einfügen}{43}{subsubsection*.44}
\contentsline {subsubsection}{Aktive Zellen}{44}{subsubsection*.45}
\contentsline {subsubsection}{Markdown}{44}{subsubsection*.46}
\contentsline {subsubsection}{Kernel}{44}{subsubsection*.47}
\contentsline {subsection}{\numberline {2.1.3}Motivation für integrierte Dokumentation}{44}{subsection.2.1.3}
\contentsline {subsubsection}{Reproducible Research}{45}{subsubsection*.48}
\contentsline {subsubsection}{Reproduzierbare (=nachvollziehbare) Vorgehensweise}{45}{subsubsection*.49}
\contentsline {subsubsection}{Zur Reproduzierbarkeit gehört auch die Software\sphinxhyphen {}Version}{45}{subsubsection*.50}
\contentsline {subsubsection}{Zwischenergebnisse im Text weiterverwenden}{46}{subsubsection*.51}
\contentsline {subsubsection}{Vorsicht: Zwischenergebnisse sind unabhängig von der Reihenfolge}{46}{subsubsection*.52}
\contentsline {subsubsection}{Ordnung wiederherstellen}{46}{subsubsection*.53}
\contentsline {subsubsection}{Andere Programmiersprachen integrieren}{47}{subsubsection*.54}
\contentsline {subsubsection}{Notebooks benennen}{47}{subsubsection*.55}
\contentsline {subsubsection}{Beenden}{47}{subsubsection*.56}
\contentsline {subsubsection}{Notebook nun wieder öffnen}{47}{subsubsection*.57}
\contentsline {subsection}{\numberline {2.1.4}Zusammenarbeit organisieren mit gitlab}{47}{subsection.2.1.4}
\contentsline {subsubsection}{Beispiel: Ablageort für Kursdateien}{47}{subsubsection*.58}
\contentsline {subsubsection}{Einfaches Herunterladen ohne Versionierung}{48}{subsubsection*.59}
\contentsline {subsubsection}{Herunterladen mit Versionierung}{48}{subsubsection*.60}
\contentsline {subsubsection}{Projektdokumentation in gitlab?}{48}{subsubsection*.61}
\contentsline {subsubsection}{Projektdokumentation in sciebo?}{49}{subsubsection*.62}
\contentsline {subsubsection}{Dateigröße von Jupyter Notebooks minimieren}{49}{subsubsection*.63}
\contentsline {subsubsection}{Vor dem Teilen: Kernel neu starten und Notebook überprüfen}{49}{subsubsection*.64}
\contentsline {subsection}{\numberline {2.1.5}Berichte erstellen aus Jupyter Notebooks}{49}{subsection.2.1.5}
\contentsline {subsection}{\numberline {2.1.6}Projektpräsentation mit jupyter}{50}{subsection.2.1.6}
\contentsline {subsection}{\numberline {2.1.7}Wichtige python\sphinxhyphen {}Befehle}{50}{subsection.2.1.7}
\contentsline {subsubsection}{Taschenrechner in python}{50}{subsubsection*.65}
\contentsline {subsubsection}{Hilfe im Netz}{50}{subsubsection*.66}
\contentsline {subsubsection}{Strukturieren durch Einrückungen (\sphinxstyleemphasis {Indent})}{51}{subsubsection*.67}
\contentsline {subsubsection}{Variablen in python}{51}{subsubsection*.68}
\contentsline {subsubsection}{Variablen weiterverwenden mit}{51}{subsubsection*.69}
\contentsline {subsubsection}{Variablentypen}{52}{subsubsection*.70}
\contentsline {subsubsection}{Identitätsfunktion \sphinxstyleliteralintitle {\sphinxupquote {id()}}}{52}{subsubsection*.71}
\contentsline {subsubsection}{Gültige Variablennamen}{53}{subsubsection*.72}
\contentsline {subsubsection}{Division mit \sphinxstyleliteralintitle {\sphinxupquote {/}}}{53}{subsubsection*.73}
\contentsline {subsubsection}{Zeichen in Strings einzeln ansprechen}{53}{subsubsection*.74}
\contentsline {subsubsection}{Länge eines Strings und letztes Zeichen}{54}{subsubsection*.75}
\contentsline {subsubsection}{von hinten zählen}{54}{subsubsection*.76}
\contentsline {subsubsection}{mit Strings arbeiten}{54}{subsubsection*.77}
\contentsline {subsubsection}{Listen ansprechen: wie Strings}{55}{subsubsection*.78}
\contentsline {subsubsection}{Wenn\sphinxhyphen {}Dann}{55}{subsubsection*.79}
\contentsline {subsubsection}{Schleifen}{55}{subsubsection*.80}
\contentsline {subsubsection}{Funktionen}{56}{subsubsection*.81}
\contentsline {subsubsection}{Bibliotheken verwenden}{56}{subsubsection*.82}
\contentsline {subsection}{\numberline {2.1.8}Literatur}{57}{subsection.2.1.8}
\contentsline {section}{\numberline {2.2}Erste Energiedatenanalyse}{57}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}python konfigurieren}{57}{subsection.2.2.1}
\contentsline {subsubsection}{Module importieren}{57}{subsubsection*.83}
\contentsline {subsubsection}{Grafikparameter einstellen}{58}{subsubsection*.84}
\contentsline {subsubsection}{Funktionen definieren}{58}{subsubsection*.85}
\contentsline {subsection}{\numberline {2.2.2}Lernziele}{58}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Energiedaten in jupyter notebook einlesen: Beispiel\sphinxhyphen {}Datensatz}{58}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Setup für Datenanalyse}{58}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Daten laden}{58}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}aktuellen Pfad in python finden}{59}{subsection.2.2.6}
\contentsline {subsubsection}{relative Pfadangabe in python}{59}{subsubsection*.86}
\contentsline {subsubsection}{absolute Pfadangabe in python}{59}{subsubsection*.87}
\contentsline {subsection}{\numberline {2.2.7}erster Überblick über die Daten: Anfang}{59}{subsection.2.2.7}
\contentsline {subsection}{\numberline {2.2.8}und Ende}{60}{subsection.2.2.8}
\contentsline {subsection}{\numberline {2.2.9}Größe des Datensatzes}{60}{subsection.2.2.9}
\contentsline {subsection}{\numberline {2.2.10}Datentypen überprüfen}{60}{subsection.2.2.10}
\contentsline {subsection}{\numberline {2.2.11}python datetime Objekt}{60}{subsection.2.2.11}
\contentsline {subsubsection}{Jahr, Monat, Tag und Zeitstempel ausgeben}{60}{subsubsection*.88}
\contentsline {subsubsection}{Datumswerte konvertieren}{61}{subsubsection*.89}
\contentsline {subsubsection}{Datumsformat in \sphinxstyleliteralintitle {\sphinxupquote {datetime}} mit \sphinxstyleliteralintitle {\sphinxupquote {strptime()}} konvertieren}{61}{subsubsection*.90}
\contentsline {subsection}{\numberline {2.2.12}Datumswerte in unserem Beispiel}{61}{subsection.2.2.12}
\contentsline {subsubsection}{Datumswerte in unserem Beispiel konvertieren}{61}{subsubsection*.91}
\contentsline {subsection}{\numberline {2.2.13}Daten in einem Diagramm darstellen}{61}{subsection.2.2.13}
\contentsline {subsection}{\numberline {2.2.14}Energiedaten darstellen in jupyter notebook}{62}{subsection.2.2.14}
\contentsline {subsubsection}{Spalten für Jahr, Monat, Tag, etc. einfügen}{62}{subsubsection*.92}
\contentsline {subsection}{\numberline {2.2.15}Stromlastprofil eines Industrieunternehmens}{63}{subsection.2.2.15}
\contentsline {subsection}{\numberline {2.2.16}Dauerlinie}{65}{subsection.2.2.16}
\contentsline {subsection}{\numberline {2.2.17}Vergleich mit lokaler Solarerzeugung}{67}{subsection.2.2.17}
\contentsline {subsection}{\numberline {2.2.18}Stromlastprofil Januar 2017 genauer ansehen}{68}{subsection.2.2.18}
\contentsline {subsubsection}{Wochentage aus matplotlib}{69}{subsubsection*.93}
\contentsline {subsubsection}{Wöchentliches Gitter im Diagramm}{69}{subsubsection*.94}
\contentsline {subsection}{\numberline {2.2.19}Jahreszeitliche Schwankungen mit Boxplots}{70}{subsection.2.2.19}
\contentsline {subsection}{\numberline {2.2.20}Wochentagsabhängigkeit des Lastprofils}{71}{subsection.2.2.20}
\contentsline {subsection}{\numberline {2.2.21}Gleitende Mittelwerte bilden: Fensterung von Daten (\sphinxstyleemphasis {Windowing})}{72}{subsection.2.2.21}
\contentsline {subsection}{\numberline {2.2.22}Histogramm der Leistungswerte}{74}{subsection.2.2.22}
\contentsline {subsection}{\numberline {2.2.23}Heatmaps erzeugen}{75}{subsection.2.2.23}
\contentsline {subsection}{\numberline {2.2.24}Sankey\sphinxhyphen {}Diagramme erzeugen}{77}{subsection.2.2.24}
\contentsline {subsubsection}{Formale Anforderungen an ein Sankey\sphinxhyphen {}Diagramm}{78}{subsubsection*.95}
\contentsline {subsubsection}{Zum Schmunzeln: ein Sankey\sphinxhyphen {}Diagramm ohne Energiebezug}{79}{subsubsection*.96}
\contentsline {subsection}{\numberline {2.2.25}Literatur}{79}{subsection.2.2.25}
\contentsline {section}{\numberline {2.3}Beispiel Datenanalyse}{79}{section.2.3}
\contentsline {section}{\numberline {2.4}Datenanalyse mit pandas}{83}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Das DataFrame}{84}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Erzeugen eines DataFrames}{84}{subsection.2.4.2}
\contentsline {subsubsection}{DataFrame aus Dicionary}{84}{subsubsection*.97}
\contentsline {subsubsection}{DataFrame aus Liste}{84}{subsubsection*.98}
\contentsline {subsubsection}{DataFrame aus Series}{85}{subsubsection*.99}
\contentsline {subsubsection}{Zusammenfassung}{85}{subsubsection*.100}
\contentsline {subsection}{\numberline {2.4.3}Die wichtigsten Eigenschaften eines DataFrames}{85}{subsection.2.4.3}
\contentsline {subsubsection}{Die Dimensionen eines DataFrames}{85}{subsubsection*.101}
\contentsline {subsubsection}{Anzahl der Elemente im DataFrame}{85}{subsubsection*.102}
\contentsline {subsubsection}{Datentypen im DataFrame}{86}{subsubsection*.103}
\contentsline {subsubsection}{Überblick über das DataFrame}{86}{subsubsection*.104}
\contentsline {subsubsection}{Zusammenfassung}{86}{subsubsection*.105}
\contentsline {subsection}{\numberline {2.4.4}Weitere hilfreiche Befehle}{86}{subsection.2.4.4}
\contentsline {subsubsection}{Einen eigenen Index verwenden}{86}{subsubsection*.106}
\contentsline {subsubsection}{Spalten/Zeilen umbenennen}{87}{subsubsection*.107}
\contentsline {subsubsection}{Spalten hinzufügen/entfernen}{87}{subsubsection*.108}
\contentsline {subsubsection}{Zeilen hinzufügen/entfernen}{88}{subsubsection*.109}
\contentsline {subsubsection}{Zusammenfassung}{88}{subsubsection*.110}
\contentsline {subsection}{\numberline {2.4.5}Daten im DataFrame auswählen (Indexieren)}{88}{subsection.2.4.5}
\contentsline {section}{\numberline {2.5}Einlesen und downsamplen zeitlich hochaufgelöster Daten}{88}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Beispiel: hochaufgelöste Daten, kurzer Zeitraum}{89}{subsection.2.5.1}
\contentsline {subsection}{\numberline {2.5.2}Beispiel: hoch aufgelöste Daten, langer Zeitraum}{93}{subsection.2.5.2}
\contentsline {chapter}{\numberline {3}Anforderungen und Systemgrenzen definieren}{95}{chapter.3}
\contentsline {section}{\numberline {3.1}Anforderungen ermitteln}{95}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}python konfigurieren}{95}{subsection.3.1.1}
\contentsline {subsubsection}{Module importieren}{95}{subsubsection*.111}
\contentsline {subsubsection}{Grafikparameter einstellen für die Diagramme festlegen:}{96}{subsubsection*.112}
\contentsline {subsubsection}{Funktionen definieren}{96}{subsubsection*.113}
\contentsline {subsection}{\numberline {3.1.2}Begriff Energieeffizienz}{96}{subsection.3.1.2}
\contentsline {subsubsection}{Energieeffizienz ist kein Selbstzweck}{96}{subsubsection*.114}
\contentsline {subsection}{\numberline {3.1.3}Einflussgrößen auf Energieeffizienz}{97}{subsection.3.1.3}
\contentsline {subsection}{\numberline {3.1.4}Ursache\sphinxhyphen {}Wirkungdiagramm (Ishikawa\sphinxhyphen {}Diagramm, Ishikawa = jap. Fishbone)}{97}{subsection.3.1.4}
\contentsline {subsubsection}{Ishikawa\sphinxhyphen {}Diagramm für die Energieeffizienz eines Systems}{98}{subsubsection*.115}
\contentsline {subsubsection}{Ishikawa\sphinxhyphen {}Diagramm als Werkzeug für die Systemanalyse}{99}{subsubsection*.116}
\contentsline {subsection}{\numberline {3.1.5}Gefahr Groupthink = \sphinxstyleemphasis {zusammen sind wir dümmer}}{100}{subsection.3.1.5}
\contentsline {subsection}{\numberline {3.1.6}Systembeschreibung}{101}{subsection.3.1.6}
\contentsline {subsubsection}{Grundbegriffe Energie, Arbeit und Leistung Reloaded}{101}{subsubsection*.117}
\contentsline {subsubsection}{Systembeschreibung für Energieeffizienzanalysen}{102}{subsubsection*.118}
\contentsline {subsection}{\numberline {3.1.7}Voice of the Customer im Prozess der Anforderungsentwicklung}{103}{subsection.3.1.7}
\contentsline {subsubsection}{Was wollen die Nutzer? Und wer alles sind überhaupt Nutzer?}{103}{subsubsection*.119}
\contentsline {subsubsection}{Wer sind die Stakeholder?}{104}{subsubsection*.120}
\contentsline {subsubsection}{Beispiel Weinkühlschrank und Konfliktpotenziale bei Stakeholdern}{105}{subsubsection*.121}
\contentsline {subsection}{\numberline {3.1.8}Technische Anforderungen formulieren: “Anforderungen an Anforderungen”}{105}{subsection.3.1.8}
\contentsline {subsubsection}{Beispiel normgerechte Beleuchtung von Arbeitsplätzen}{105}{subsubsection*.122}
\contentsline {subsubsection}{Dokumentation in Tabellenkalkulation}{106}{subsubsection*.123}
\contentsline {subsubsection}{Lernkurve und Anforderungsmanagement}{106}{subsubsection*.124}
\contentsline {subsection}{\numberline {3.1.9}Priorisieren von Kundenwünschen: Kano\sphinxhyphen {}Modell}{107}{subsection.3.1.9}
\contentsline {subsection}{\numberline {3.1.10}Priorisieren von Anforderungen}{108}{subsection.3.1.10}
\contentsline {subsection}{\numberline {3.1.11}Literatur}{109}{subsection.3.1.11}
\contentsline {chapter}{\numberline {4}Mit Daten kritisch umgehen}{111}{chapter.4}
\contentsline {section}{\numberline {4.1}Plausible Daten}{111}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}python konfigurieren}{111}{subsection.4.1.1}
\contentsline {subsubsection}{Module importieren}{111}{subsubsection*.125}
\contentsline {subsubsection}{Grafikparameter einstellen}{111}{subsubsection*.126}
\contentsline {subsubsection}{Funktionen definieren}{111}{subsubsection*.127}
\contentsline {subsection}{\numberline {4.1.2}Lernziele}{111}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Plausible Daten \sphinxhyphen {} wie beurteilen?}{112}{subsection.4.1.3}
\contentsline {subsubsection}{Anforderungen benötigen plausible Werte}{112}{subsubsection*.128}
\contentsline {subsubsection}{Beispiel Energiebedarf einer Bäckerei}{112}{subsubsection*.129}
\contentsline {subsubsection}{Beispiel Messdaten einer Waschmaschine}{113}{subsubsection*.130}
\contentsline {subsection}{\numberline {4.1.4}Techniken zur Plausibilisierung von Daten}{114}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Varianz von Daten}{114}{subsection.4.1.5}
\contentsline {subsubsection}{Reproduzierbarkeit und Schwankungen bei mehrfachen Messungen}{114}{subsubsection*.131}
\contentsline {subsubsection}{Umgang mit variierenden Randbedingungen}{115}{subsubsection*.132}
\contentsline {subsection}{\numberline {4.1.6}Literatur}{115}{subsection.4.1.6}
\contentsline {section}{\numberline {4.2}Datenqualität beurteilen, Daten bereinigen und ergänzen}{115}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}python konfigurieren}{115}{subsection.4.2.1}
\contentsline {subsubsection}{Module importieren}{115}{subsubsection*.134}
\contentsline {subsubsection}{Grafikparameter einstellen}{116}{subsubsection*.135}
\contentsline {subsubsection}{Funktionen definieren}{116}{subsubsection*.136}
\contentsline {subsection}{\numberline {4.2.2}Lernziele}{116}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Kriterien für Datenqualität in Datensätzen}{116}{subsection.4.2.3}
\contentsline {subsubsection}{Datenqualität: Struktureller Überblick}{117}{subsubsection*.137}
\contentsline {subsubsection}{Messbare Kriterien für Datenqualität}{117}{subsubsection*.138}
\contentsline {subsubsection}{Externe Daten im Nachhinein verbessern}{118}{subsubsection*.139}
\contentsline {subsection}{\numberline {4.2.4}Fehlerhafte Daten bereinigen}{118}{subsection.4.2.4}
\contentsline {subsubsection}{Datenqualität: eigene oder aus externen Daten selbst zusammengefasste Daten bereinigen}{118}{subsubsection*.140}
\contentsline {subsubsection}{Beispiel für Ersetzen / Eliminieren von fehlerhaften / fehlenden Daten bei der Waschmaschinenmessung}{118}{subsubsection*.141}
\contentsline {subsubsection}{Mit Mittelwert auffüllen}{118}{subsubsection*.142}
\contentsline {subsubsection}{Linear interpolieren}{120}{subsubsection*.143}
\contentsline {subsubsection}{Zufallszahlen hinzufügen}{121}{subsubsection*.144}
\contentsline {subsection}{\numberline {4.2.5}Wissenschaftliches Fehlverhalten}{123}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Messungen und Messdaten kritisch betrachten}{123}{subsection.4.2.6}
\contentsline {subsubsection}{Datenqualität bei Messdaten: Beispiel einer Temperaturmessung}{124}{subsubsection*.145}
\contentsline {subsection}{\numberline {4.2.7}Neue Profile aus gemessenen Daten zusammenstellen}{124}{subsection.4.2.7}
\contentsline {subsection}{\numberline {4.2.8}Energieströme und zugehörige Messgrößen}{128}{subsection.4.2.8}
\contentsline {subsection}{\numberline {4.2.9}Energiedaten aus Top\sphinxhyphen {}Down und Bottom\sphinxhyphen {}Up\sphinxhyphen {}Analysen ergänzen}{129}{subsection.4.2.9}
\contentsline {subsubsection}{Energiedaten Top\sphinxhyphen {}Down analysieren: vom Gesamtverbrauch auf den Einzelverbrauch schließen}{129}{subsubsection*.146}
\contentsline {subsubsection}{Nutzen von Top\sphinxhyphen {}Down\sphinxhyphen {}Daten: Iteratives Vorgehen bei der Analyse}{130}{subsubsection*.147}
\contentsline {subsubsection}{Bottom\sphinxhyphen {}Up\sphinxhyphen {}Analyse: Typenschilder zählen}{130}{subsubsection*.148}
\contentsline {subsubsection}{ABC\sphinxhyphen {}Analyse von Verbrauchern}{131}{subsubsection*.149}
\contentsline {subsection}{\numberline {4.2.10}Literatur}{131}{subsection.4.2.10}
\contentsline {chapter}{\numberline {5}Elektrische Energie messen: Messgrößen und Normen}{133}{chapter.5}
\contentsline {section}{\numberline {5.1}Elektrische Energie}{133}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}python konfigurieren}{133}{subsection.5.1.1}
\contentsline {subsubsection}{Module importieren}{133}{subsubsection*.150}
\contentsline {subsubsection}{Grafikparameter einstellen}{133}{subsubsection*.151}
\contentsline {subsubsection}{Funktionen definieren}{133}{subsubsection*.152}
\contentsline {subsection}{\numberline {5.1.2}Lernziele}{133}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Elektrische Leistung}{134}{subsection.5.1.3}
\contentsline {subsubsection}{Gleichstromleistung}{134}{subsubsection*.153}
\contentsline {subsubsection}{Wechselstromleistung}{134}{subsubsection*.154}
\contentsline {paragraph}{Augenblicksleistung}{134}{paragraph*.155}
\contentsline {paragraph}{Wechselspannung}{134}{paragraph*.156}
\contentsline {paragraph}{Zeitveränderliche Ströme}{135}{paragraph*.157}
\contentsline {paragraph}{Wirkleistung}{136}{paragraph*.158}
\contentsline {paragraph}{Zeitdiskrete Signale und zeitliche Auflösung}{136}{paragraph*.159}
\contentsline {paragraph}{Mittlere Ströme und Leistungen}{137}{paragraph*.160}
\contentsline {paragraph}{Leistungsfaktor}{137}{paragraph*.161}
\contentsline {paragraph}{Effektivwert}{137}{paragraph*.162}
\contentsline {paragraph}{Blindleistung}{138}{paragraph*.163}
\contentsline {paragraph}{Verzerrungsblindleistung}{138}{paragraph*.164}
\contentsline {paragraph}{Scheitelfaktor}{141}{paragraph*.165}
\contentsline {subsection}{\numberline {5.1.4}Elektrische Energie}{141}{subsection.5.1.4}
\contentsline {subsubsection}{Lastprofil: zeitlich veränderliche Wirkleistung}{141}{subsubsection*.166}
\contentsline {subsection}{\numberline {5.1.5}Literatur}{143}{subsection.5.1.5}
\contentsline {section}{\numberline {5.2}Elektrische Energie messen}{143}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}python konfigurieren}{143}{subsection.5.2.1}
\contentsline {subsubsection}{Module importieren}{143}{subsubsection*.168}
\contentsline {subsubsection}{Grafikparameter einstellen}{143}{subsubsection*.169}
\contentsline {subsubsection}{Funktionen definieren}{143}{subsubsection*.170}
\contentsline {subsection}{\numberline {5.2.2}Lernziele}{143}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Messgenauigkeit}{144}{subsection.5.2.3}
\contentsline {subsubsection}{Zeitdiskrete Signale (Digitalsignale)}{144}{subsubsection*.171}
\contentsline {subsubsection}{Wertdiskrete Signale}{147}{subsubsection*.172}
\contentsline {subsubsection}{Messabweichungen bei elektrischen Energiemessungen}{150}{subsubsection*.173}
\contentsline {subsubsection}{Angabe der Genauigkeit}{151}{subsubsection*.174}
\contentsline {subsection}{\numberline {5.2.4}Eigenständige Messgeräte für unterschiedliche Einsätze}{152}{subsection.5.2.4}
\contentsline {subsubsection}{USB\sphinxhyphen {}Geräte vermessen}{152}{subsubsection*.175}
\contentsline {subsubsection}{Einphasige Elektrogeräte mit einem Leistungsanalysator vermessen}{153}{subsubsection*.176}
\contentsline {subsubsection}{Literatur}{158}{subsubsection*.177}
\contentsline {section}{\numberline {5.3}Einphasige elektrische Geräte mit dem Energielogger vermessen}{159}{section.5.3}
\contentsline {subsection}{\numberline {5.3.1}python konfigurieren}{159}{subsection.5.3.1}
\contentsline {subsubsection}{Module importieren}{159}{subsubsection*.178}
\contentsline {subsubsection}{Grafikparameter einstellen}{159}{subsubsection*.179}
\contentsline {subsubsection}{Funktionen definieren}{159}{subsubsection*.180}
\contentsline {subsection}{\numberline {5.3.2}Lernziele}{159}{subsection.5.3.2}
\contentsline {subsection}{\numberline {5.3.3}Ausleihe}{159}{subsection.5.3.3}
\contentsline {subsection}{\numberline {5.3.4}Inbetriebnahme}{160}{subsection.5.3.4}
\contentsline {subsection}{\numberline {5.3.5}Daten mit python auslesen}{161}{subsection.5.3.5}
\contentsline {subsection}{\numberline {5.3.6}Genauigkeit}{163}{subsection.5.3.6}
\contentsline {section}{\numberline {5.4}Literatur}{163}{section.5.4}
\contentsline {section}{\numberline {5.5}Praktikum elektrische Energie messen}{163}{section.5.5}
\contentsline {subsection}{\numberline {5.5.1}python konfigurieren}{163}{subsection.5.5.1}
\contentsline {subsubsection}{Module importieren}{163}{subsubsection*.181}
\contentsline {subsubsection}{Grafikparameter einstellen}{164}{subsubsection*.182}
\contentsline {subsubsection}{Funktionen definieren}{164}{subsubsection*.183}
\contentsline {subsection}{\numberline {5.5.2}Lernziele}{164}{subsection.5.5.2}
\contentsline {subsection}{\numberline {5.5.3}Vorbereiten}{164}{subsection.5.5.3}
\contentsline {subsection}{\numberline {5.5.4}Sicherheitshinweise}{165}{subsection.5.5.4}
\contentsline {subsection}{\numberline {5.5.5}Dokumentation}{165}{subsection.5.5.5}
\contentsline {subsection}{\numberline {5.5.6}Versuchsplan}{165}{subsection.5.5.6}
\contentsline {subsection}{\numberline {5.5.7}Professionelles Leistungsmessgerät für elektrische Verbraucher}{166}{subsection.5.5.7}
\contentsline {subsection}{\numberline {5.5.8}Verfügbare elektrische Verbraucher}{167}{subsection.5.5.8}
\contentsline {subsubsection}{Leuchtmittel}{167}{subsubsection*.184}
\contentsline {subsubsection}{Bildschirm (Monitor)}{168}{subsubsection*.185}
\contentsline {subsubsection}{Computer}{168}{subsubsection*.186}
\contentsline {subsubsection}{Weitere verfügbare Geräte}{169}{subsubsection*.187}
\contentsline {subsubsection}{Messgerät kennenlernen mit Beispielmessungen}{169}{subsubsection*.188}
\contentsline {section}{\numberline {5.6}Literatur}{172}{section.5.6}
\contentsline {section}{\numberline {5.7}Vorschriften und Normen für Energieeffizienz}{172}{section.5.7}
\contentsline {subsection}{\numberline {5.7.1}python konfigurieren}{172}{subsection.5.7.1}
\contentsline {subsubsection}{Module importieren}{172}{subsubsection*.189}
\contentsline {subsubsection}{Grafikparameter einstellen}{172}{subsubsection*.190}
\contentsline {subsubsection}{Funktionen definieren}{172}{subsubsection*.191}
\contentsline {subsection}{\numberline {5.7.2}Lernziele für dieses Kapitel}{172}{subsection.5.7.2}
\contentsline {subsection}{\numberline {5.7.3}Warum gibt es Energieeffizienz\sphinxhyphen {}Normen?}{172}{subsection.5.7.3}
\contentsline {subsection}{\numberline {5.7.4}Quellen für Normen}{173}{subsection.5.7.4}
\contentsline {subsubsection}{Wichtige Links}{173}{subsubsection*.192}
\contentsline {subsubsection}{Effizient Normungsinformationen aufbewahren}{174}{subsubsection*.193}
\contentsline {subsection}{\numberline {5.7.5}Kenngrößen für Energieeffizienz}{174}{subsection.5.7.5}
\contentsline {subsubsection}{Energielabel}{174}{subsubsection*.194}
\contentsline {subsubsection}{Selbst erdachte Kennwerte}{176}{subsubsection*.195}
\contentsline {subsection}{\numberline {5.7.6}Historische Entwicklung von Effizienzvorgaben}{177}{subsection.5.7.6}
\contentsline {section}{\numberline {5.8}Literatur}{178}{section.5.8}
\contentsline {part}{II\hspace {1em}Aktuelle Projektaufgabe}{179}{part.2}
\contentsline {chapter}{\numberline {6}Systemtechnik für Energieeffizienz (SYE) Projektaufgabe Winter 2021}{181}{chapter.6}
\contentsline {chapter}{\numberline {7}\sphinxstyleemphasis {Energieeffizienz und Reparierbarkeit bei Energielabel\sphinxhyphen {}Geräten}}{183}{chapter.7}
\contentsline {section}{\numberline {7.1}python konfigurieren}{183}{section.7.1}
\contentsline {subsection}{\numberline {7.1.1}Module importieren}{183}{subsection.7.1.1}
\contentsline {subsection}{\numberline {7.1.2}Grafikparameter einstellen}{184}{subsection.7.1.2}
\contentsline {subsection}{\numberline {7.1.3}Funktionen definieren}{184}{subsection.7.1.3}
\contentsline {section}{\numberline {7.2}Motivation: wozu das Ganze?}{184}{section.7.2}
\contentsline {section}{\numberline {7.3}Lernziele: was lernen Sie hier?}{185}{section.7.3}
\contentsline {section}{\numberline {7.4}Aufgabenstellung}{185}{section.7.4}
\contentsline {subsection}{\numberline {7.4.1}Projektsteckbrief}{185}{subsection.7.4.1}
\contentsline {subsection}{\numberline {7.4.2}Anforderungen dokumentieren}{185}{subsection.7.4.2}
\contentsline {subsection}{\numberline {7.4.3}Verbrauchsanalyse und Erzeugungsanalyse}{186}{subsection.7.4.3}
\contentsline {subsection}{\numberline {7.4.4}Klimawirkung und Ressourceneffizienz einschätzen}{186}{subsection.7.4.4}
\contentsline {subsection}{\numberline {7.4.5}Messungen durchführen}{187}{subsection.7.4.5}
\contentsline {subsection}{\numberline {7.4.6}Maßnahmen zur Verbesserung der Klimawirkungen und des Energieverbrauchs evaluieren}{187}{subsection.7.4.6}
\contentsline {subsection}{\numberline {7.4.7}Projektaufgabenauswahl}{188}{subsection.7.4.7}
\contentsline {section}{\numberline {7.5}Projektdokumentation}{188}{section.7.5}
\contentsline {section}{\numberline {7.6}Formales}{189}{section.7.6}
\contentsline {section}{\numberline {7.7}Literatur}{190}{section.7.7}
\contentsline {part}{III\hspace {1em}Anhang (nicht prüfungsrelevant)}{191}{part.3}
\contentsline {chapter}{\numberline {8}Öffentlich verfügbare Energiedaten}{195}{chapter.8}
\contentsline {section}{\numberline {8.1}python konfigurieren}{195}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Module importieren}{195}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Grafikparameter einstellen}{195}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Funktionen definieren}{195}{subsection.8.1.3}
\contentsline {section}{\numberline {8.2}Lernziele}{195}{section.8.2}
\contentsline {section}{\numberline {8.3}Sammlungen mit Energiedaten}{196}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Energiedaten\sphinxhyphen {}Zeitreihen, teilweise modelliert, teilweise gemessen}{198}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Gemessene Verbrauchsdaten von Elektrogeräten}{199}{subsection.8.3.2}
\contentsline {section}{\numberline {8.4}Photovoltaik\sphinxhyphen {}Erzeugungsdaten und solare Wetterdaten}{199}{section.8.4}
\contentsline {section}{\numberline {8.5}Literatur}{199}{section.8.5}
\contentsline {chapter}{\numberline {9}Energietechnik modellieren mit python}{201}{chapter.9}
\contentsline {section}{\numberline {9.1}Energiemodelle mit python}{201}{section.9.1}
\contentsline {section}{\numberline {9.2}Apps für Energiedatenanalyse}{201}{section.9.2}
\contentsline {section}{\numberline {9.3}Literatur}{202}{section.9.3}
\contentsline {chapter}{Bibliography}{203}{chapter*.196}
