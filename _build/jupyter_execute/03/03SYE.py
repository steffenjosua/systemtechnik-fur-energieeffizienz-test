#!/usr/bin/env python
# coding: utf-8

# # Anforderungen und Systemgrenzen definieren

# Dieses Kapitel beschreibt detaillierter, worauf es bei der Klärung von Anforderungen ankommt und wie man sie so dokumentiert, dass man rasch sehen kann, wie vertrauenswürdig und plausibel die zugehörigen Daten sind. Sie können dies dann direkt beginnen, auf Ihre Projekte anzuwenden.
