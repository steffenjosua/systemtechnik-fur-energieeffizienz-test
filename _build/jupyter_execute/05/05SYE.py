#!/usr/bin/env python
# coding: utf-8

# # Elektrische Energie messen: Messgrößen und Normen

# Dieses Kapitel wiederholt die Grundbegriffe elektrischer Größen, die für die Energiemessung relevant sind. Außerdem zeigt es auf, wie die Messung mit einem bestimmten haushaltsüblichen Energielogger und einem professionellen Leistungsanalysator durchgeführt wird. Besonders interessante Normen für die Messung der elektrischen Energieeffizienz runden die Informationen ab.
