#!/usr/bin/env python
# coding: utf-8

# # Einführung und Kursorganisation
# 
# Dieses Kapitel beinhaltet organisatorische Informationen (was wird benotet, welche Termine finden wo statt) und eine Einführung in das Thema Systemtechnik für Energieeffizienz (SYE).

# ![SYEstruktur](../img/SYEstruktur.png)
