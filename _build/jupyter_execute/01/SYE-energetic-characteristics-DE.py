#!/usr/bin/env python
# coding: utf-8

# # Energiekenngrößen

# ## Lernziele

# <div class="admonition note" style="background: #e5f1ff; padding: 10px">
# <div class="title"><b>LERNZIELE</b></div>
#     <ul>
#         <li> Energiekenngrößen und Grundbegriffe der Energieeffizienz kennen </li>
#         <li> mithilfe der VDI 4661 und weiterer Normen </li>
#         <li> um Literaturwerte gut einschätzen zu können </li>
#     </ul>
# </div>

# ## Grundbegriffe nach VDI 4661

# | Formelzeichen | Benennung | Einheit |
# |-|-|-|
# | $AZ$ | energetische Amortisationszeit | a |
# | $E$ | Energie | J |
# | $EF$ | Erntefaktor | - |
# | $f_{\text{ex}}$ | Exergieanteil bezogen auf den Heizwert | - |
# | $g$ | Bereitstellungsnutzungsgrad | - |
# | $H$ | Enthalpie | J | 
# | $H_s$ | Brennwert | J/kg |
# | $H_i$ | Heizwert | J/kg |
# | $I$ | Stromstärke | A |
# | $KEA$ | kumulierter Energieaufwand | J |
# | $L$ | Lebensdauer | a |
# | $\dot{m}$ | Massenstrom | kg/s |
# | $n_A$ | Arbeitsauslastung | - |
# | $p$ | Druck | Pa |
# | $P$ | (Wirk-)Leistung | W |
# | $Q$ | Wärme *oder* Blindleistung | J *oder* var |
# | $\dot{Q}$ | Wärmestrom | W |
# | $S$ | Scheinleistung *oder* Entropie | VA *oder* J/K |
# | $t$ | Zeit | s |
# | $T$ | thermodynamische Temperatur *oder* Periode | K *oder* s |
# | $T_{\text{aN}}$ | mittlere Jahresausnutzungsdauer | h/a |
# | $U$ | innere Energie *oder* Spannung | J *oder* V |
# | $V$ | Volumen | m$^3$ |
# | $W$ | Arbeit | J |
# | $w$ | spezifischer Energiebedarf oder spezifischer Energieverbrauch | J/FE |
# | $\alpha$ | Wärmeausbeute (Wirkungsgrad der Wärmeerzeugung ohne Berücksichtigung der Stromerzeugung) | - |
# | $\beta$ | Stromausbeute (Wirkungsgrad der Stromerzeugung ohne Berücksichtigung der Wärmeerzeugung) | - |
# | $\varepsilon$ | Leistungszahl | - |
# | $\zeta$ | exergetischer Wirkungsgrad | - |
# | $\xi$ | exergetischer Nutzungsgrad | - |
# | $\omega$ | Brennstoffausnutzungsgrad *oder* Kreisfrequenz | - *oder* Hz |
# | $\bar{\omega}$ | mittlerer Brennstoffausnutzungsgrad | - |
# | $\eta$ | Wirkungsgrad | - |
# | $\bar{\eta}$ | Nutzungsgrad | - |
# | $\eta_C$ | Carnotfaktor | - | 
# | $\sigma$ | Stromkennzahl | - | 
# | $\varphi$ | Phasenverschiebungswinkel | rad |
# | $\Phi$ | elektromagnetische Strahlung | J |

# <div class="admonition important" style="background: #e9f6ec; padding: 10px">
# <div class="title"><b>AUFGABE</b></div>
# Wir bilden zwei Teams (Team 1: alle mit Schuhgröße < 41, Team 2: alle mit > 41). Jedes Team sucht drei Kenngrößen heraus, die das andere Team in eigenen Worten, am besten an einem Beispiel, erklären muss. Wer die meisten Kenngrößen erklären kann, gewinnt.
# </div>

# *Notizen:...*

# ## Literatur

# ```{bibliography}
# :filter: docname in docnames
# ```
