#!/usr/bin/env python
# coding: utf-8

# # Systemtechnik für Energieeffizienz (SYE) Projektaufgabe Winter 2021<br>
# # *Energieeffizienz und Reparierbarkeit bei Energielabel-Geräten*

# **Das Projekt ist eine Teilprüfungsleistung im Modul SYE. Bewertet wird zum einen die Erfüllung der Projektaufgabe, wie sie hier beschrieben ist (90% der Punkte). Weitere 10% der Punkte sind für die Teilnahme
# an der Exkursion reserviert. Falls diese nicht stattfinden kann, wird die Projektaufgabe zu 100% für die Bewertung der Teilprüfungsleistung herangezogen.**<br>
# **Für das Projekt bearbeiten Sie in einem Team von 3-4 Studierenden eine Aufgabenstellung zur Energieeffizienz mit systemtechnischen Methoden. Damit die Projektergebnisse sich untereinander vergleichen lassen, stimmen wir gewisse Randbedingungen in der live-Veranstaltung gemeinsam ab.**<br>
# **Bewertet wird die Präsentation und die Unterlagen (Jupyter Notebook, Messdaten, Notizen, Bewertungstabellen, Präsentationsdatei, etc.), die mit der Präsentation abzugeben sind. Bitte achten Sie für die Bewertung darauf, dass Ihr individueller Beitrag sichtbar wird, indem Sie eine Tabelle beifügen, in der alle Arbeitspakete und Prozentzahlen der einzelnen Beiträge genannt sind.**

# ## python konfigurieren

# ### Module importieren

# In[1]:


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)


# ### Grafikparameter einstellen

# In[2]:


plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)


# ### Funktionen definieren

# ## Motivation: wozu das Ganze?

# In[3]:


year = np.linspace(2006,2020,15)
# all quantities: tons
# group 1
other_large_appliance = [388299.102,511662.282,453297.662,449775.927,494302.539,501422.823,490002.1938,492822.8751,502120.0460,523737.003,530566.889,517408.506,0,0,0]
cooling_households = [171075.167,168144.221,176838.850,186664.578,193458.527,218393.375,226566.0351,242198.1323,257862.1875,268774.177,274361.034,272169.312,0,0,0]
heat_exchanger_households = [0,0,0,0,0,0,0,0,0,0,0,0,285987.725,292203.226,330499.927]
# group 2
household_small_appliance = [109082.670,163000.788,157515.670,143799.397,187230.554,176494.689,164834.9191,170524.6714,190415.3943,209558.977,200754.954,201291.883,0,0,0]
household_screen = [0,0,0,0,0,0,0,0,0,0,0,0,115220.398,130752.620,144457.243]
# group 3
personal_ict = [124128.549,206918.124,137360.272,219826.911,132452.420,100764.523,83297.3564,74228.2561,76128.5924,62519.430,62563.481,55404.487,0,0,0]
personal_print = [47870.015,55072.267,53712.598,46092.476,46923.086,43981.567,45396.3850,44733.2543,46595.9754,50570.908,52112.081,52260.026,0,0,0]
personal_communication = [12555.762,9579.486,11342.635,14430.358,10667.598,12552.229,12256.0718,12080.2069,13389.9650,13942.201,11925.774,15226.691,0,0,0]
mobile_phone = [6099.659,6740.723,5707.085,5077.303,4693.706,5239.479,6208.8614,6079.2132,5672.7798,6390.532,5834.435,5860.419,0,0,0]
data_viewer = [30350.838,35973.140,33369.498,31887.395,26564.706,29429.033,27066.6229,25370.1483,26954.4590,25307.572,26593.020,28991.899,0,0,0]
cameras = [2114.030,12573.348,5239.499,2751.776,3358.815,3696.878,2782.4937,2135.8959,1669.3076,1590.995,1557.075,1439.893,0,0,0]
household_discharge_lamps = [0,0,0,0,0,0,0,0,0,0,0,0,5699.502,4995.227,3999.669]
household_lamps = [0,0,0,0,0,0,0,0,0,0,0,0,8846.860,10383.998,10784.491]
# group 4
household_tv = [107662.012,119406.695,148483.375,120193.711,131090.208,115405.045,101938.8386,79372.0994,84238.9159,68553.672,68136.174,76563.747,0,0,0]
household_multimedia = [104537.380,119406.695,148483.375,120193.711,131090.208,115405.045,101938.8386,79372.0994,84238.9159,68553.672,68136.174,76563.747,0,0,0]
household_pv_modules = [0,0,0,0,0,0,0,0,0,0,106894.642,155539.257,0,0,0]
household_large_appliance = [0,0,0,0,0,0,0,0,0,0,0,0,552973.611,819770.610,949241.307]
household_large_pv_module = [0,0,0,0,0,0,0,0,0,0,0,0,210528.632,272421.793,320433.703]
# group 5
household_lamps2 = [0,0,0,0,0,0,0,0,0,0,2908.284,7945.217,0,0,0]
household_small_appliance2 = [0,0,0,0,0,0,0,0,0,0,0,0,635533.313,525881.212,559261.930]
household_small_pv_modules = [0,0,0,0,0,0,0,0,0,0,0,0,0.010,37.527,82.264]
# group 6
powertools = [103487.921,116732.647,127312.591,101399.802,164379.024,104374.401,103478.8738,107324.8169,118115.6519,123292.538,132545.751,142262.908,0,0,0]
household_small_ict = [0,0,0,0,0,0,0,0,0,0,0,0,120515.085,93023.817,97081.988]
#group 7
household_toys = [28274.583,32084.327,79775.376,14613.024,14488.424,13232.619,13965.014,13862.6313,16200.4974,17156.031,17804.466,19848.894,0,0,0]
household_leisure = [5748.842,6638.487,13540.046,20190.789,13594.678,15146.168,15348.7071,17480.5763,20456.9281,25305.932,29159.037,36734.375,0,0,0]
# group 8
household_medical = [2101.993,1985.681,2106.906,6436.320,2860.013,3577.395,3454.6027,3397.8853,3518.6794,4061.718,4652.192,4693.262,0,0,0]
# group 9
household_surveillance = [2813.624,3248.591,3316.890,4834.642,5074.226,6160.001,6057.1813,7068.8554,9507.9712,14070.441,19459.331,21885.826,0,0,0]

# regroup
data = {'year': year,
        'large appliance': 0.001*np.add(other_large_appliance,household_large_appliance),
        'HVAC': 0.001*np.add(cooling_households,heat_exchanger_households),
        'small appliance': 0.001*np.add(np.add(household_small_appliance,household_small_appliance2),cameras),
        'TVs, screens, multimedia': 0.001*np.add(np.add(household_screen,household_tv),household_multimedia),
        'ICT': 0.001*np.add(np.add(np.add(personal_ict,personal_print),np.add(personal_communication,mobile_phone)),np.add(data_viewer, household_small_ict)),
        'lamps': 0.001*np.add(np.add(household_discharge_lamps,household_lamps),household_lamps2),
        'PV modules': 0.001*np.add(np.add(household_pv_modules, household_large_pv_module),household_small_pv_modules),
        'power tools, toys, leisure, medical, surveillance': 0.001*np.add(np.add(np.add(powertools,household_toys),np.add(household_medical,household_leisure)),household_surveillance)
        }
# https://www.stiftung-ear.de/fileadmin/Bilder/StatDaten/Input_GA-bezogen_b2c.png

fig = plt.figure(figsize=(8,5))
ax = plt.subplot(111)

ewaste = pd.DataFrame(data, columns = ['year', 'large appliance', 'HVAC', 'small appliance', 'TVs, screens, multimedia', 'ICT', 'lamps', 'PV modules', 'power tools, toys, leisure, medical, surveillance'])
#ewaste = pd.DataFrame(data, columns = ['year', 'other_large_appliance', 'cooling_households', 'heat_exchanger_households', 'household_small_appliance', 'household_screen', 'personal_ict', 'personal_print', 'personal_communication', 'mobile_phone', 'data_viewer', 'cameras', 'household_discharge_lamps', 'household_lamps', 'household_tv', 'household_multimedia', 'household_pv_modules', 'household_large_appliance', 'household_large_pv_module', 'household_lamps2', 'household_small_appliance2', 'household_small_pv_modules', 'powertools', 'household_small_ict', 'household_toys', 'household_leisure', 'household_medical', 'household_surveillance'])
ewaste.plot.bar(ax=ax, x = 'year', title = 'b2c input quantities [Mt], source: Stiftung ear, Germany', stacked=True)

box = ax.get_position()
ax.set_position([box.x0 + box.width*0.3, box.y0, box.width*0.7, box.height])
ax.legend(loc='right', bbox_to_anchor=(2.5,.5))


# {cite:p}`stiftung_ear_inputmengen_2021` zeigt, dass die Mengen an verkauften, registrierten Elektrogeräten zunehmen. Diese Geräte werden nach Ablauf ihrer Lebensdauer im Elektroschrott landen. Möglicherweise ist dies aus Energieeffizienz-Sicht sogar begrüßenswert, jedoch nicht unbedingt aus der Sicht, dass für Neugeräte erneut Rohstoffe und Herstellungsenergie aufgewendet werden müssen und dass der Elektroschrott bei der Entsorgung auch Aufwände verursacht.

# Die [Podcast-Folge](https://www.oeko.de/podcast/episode/wie-verlaengern-wir-das-leben-unserer-elektrogeraete) {cite:p}`ev_oekode_episode_2021` fasst zusammen, dass Energieeffizienz inzwischen besser geworden und dass die neue Herstellung von eventuell noch effizienteren Geräten ökologisch inzwischen manchmal problematischer ist, als die Weiternutzung eines Altgeräts.

# Seit 2021 gelten für viele energieeffiziente Geräte neue Energielabel-Regelungen und für die restlichen Geräte werden die entsprechenden Richtlinien in den kommenden Jahren erstellt. Es sind auch erste Ansätze zu Reparierbarkeit darin zu finden, um so die Lebensdauer zu verlängern.

# Es stellt sich die Frage, welche Vorteile Reparaturmöglichkeiten bringen und gleichzeitig auch, welche Nachteile dadurch zu erwarten sind. Z. B. verringert sich die Energieeffizienz von gealterten Geräten oftmals auch nutzungsabhängig während der Lebensdauer.

# ## Lernziele: was lernen Sie hier?

# <div class="alert alert-block alert-info"><b>Lernziele für das Projekt: </b>
# 
# *am Beispiel eines Energielabel-Geräts:*
# - Anforderungen an ein Energie verwendendes System definieren
# - Energiedaten aus Messungen und Literatur analysieren mit python
# - Energiedaten generieren mit python
# - Lebenszyklusaspekte berücksichtigen
# - Potenziale zur Verbesserung der Energieeffizienz ableiten
# - nachvollziehbare Analyse und Dokumentation in jupyter notebook
# </div>

# ## Aufgabenstellung

# Die Projektaufgabe besteht darin, ein Energielabel-Gerät aus der unten stehenden Tabelle im genannten Nutzungsszenario nachvollziehbar zu evaluieren. 

# ### Projektsteckbrief

# Jedes Projekt erstellt einen Steckbrief, der die wichtigsten Kenndaten des Projektes enthält: Hilfreich ist eine Abbildung mit dem betrachteten System. Außerdem enthält der Steckbrief **wichtige Kennzahlen** des Systems, die sich das Publikum merken soll, um diese gut zu verstehen. Das bedeutet, dass es nicht zu viele Kennzahlen sein dürfen und dass Sie auswählen, welche Kennzahlen besonders wichtig sind, um die darauf folgende Analyse zu verstehen.

# ### Anforderungen dokumentieren

# Das System, das Sie betrachten, wird auf eine bestimmte Weise, die sich aus der Projekt-Auswahl-Tabelle ergibt, eingesetzt. Die Nutzenden haben also Anforderungen an das System. Teilweise ist es Aufgabe des Projektteams, anhand von z. B. statistischen Daten diese Anforderungen mit plausiblen Werten zu hinterlegen. Diese Anforderungen sind zu dokumentieren. Hilfreich ist dafür eine Tabelle in [Excel](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz/-/blob/master/specs/2019-10-03_anforderungsliste_system_XY_beispiel.xlsx) oder [Libreoffice](https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz/-/blob/master/specs/2019-10-03_anforderungsliste_system_XY_beispiel.ods) (wie in Kapitel 3 gezeigt). Die Tabelle geben Sie mit ab. Die wichtigsten Anforderungen daraus zeigen Sie auch bei der Präsentation Ihrer Ergebnisse.

# Am Ende bewerten Sie Ihre Energiesparmaßnahmen nicht nur in Hinblick auf energetische, ökonomische und ökologische Potenziale, **sondern auch im Hinblick darauf, wie gut sie die gesetzten Anforderungen erfüllen**.

# Definieren Sie außerdem aus den Anforderungen eine **Energieeffizienzkenngröße** (bei einem Bäcker wären das z. B. kg Mehl pro kWh Stromverbrauch, also eine Größe, die Energieaufwand und Energienutzen miteinander verknüpft). Berechnen Sie, wie sich diese Energieeffizienzgröße mit den betrachteten Einsparmaßnahmen verändert.

# ### Verbrauchsanalyse und Erzeugungsanalyse

# Sie benötigen Verbrauchsdaten aus öffentlich verfügbaren Datenquellen. Diese lesen Sie in einem jupyter notebook ein, inklusive Nennung des Links und ordnungsgemäßer Zitierung des Datensatzes (wie eine Literaturquelle zitieren).

# Nach dem Überprüfen auf Unregelmäßigkeiten (fehlende oder unplausible Werte, falsche Datenformate konvertieren, Datumsdaten standardisieren) und Bereinigen des Datensatzes (transparent darstellen, wie Sie fehlende oder unplausible Werte durch plausible Werte auffüllen, wie Datumsformate konvertiert werden, etc.), analysieren Sie jeden Datensatz. 

# Erstellen Sie im Jupyter Notebook für die Projektdokumentation für jedes Projekt folgende Verbrauchsdarstellungen (gemessene Daten, ergänzt mit open data) und Erzeugungsdarstellungen (open data): 
# - Darstellung des Jahresverlaufs des Verbrauchs und der Erzeugung über der Zeit
# - Dauerlinien
# - Darstellung des Taganteils gegenüber dem Nachtanteil
# - Darstellung des Sommeranteils (d.h. der Zeit mit hoher Photovoltaik-Erzeugung) gegenüber dem Winteranteil
# - Übersicht über monatsspezifische Schwankungsbreiten (Boxplot)
# - Heatmap bzw. Teppichdiagramm

# Ergänzen Sie jeweils im Markdown-Format eine Zelle, die Ihre Analyseergebnisse zur jeweiligen Darstellung beschreibt. Sollten Sie nochmals unplausible oder fehlende Daten finden, müssen Sie nochmals überprüfen und bereinigen (s.o.) und die Analyse mit dem bereinigten Datensatz erneut durchführen.

# **Quellen für Daten:**

# Eine immer wieder ergänzte Sammlung mit öffentlich verfügbaren Energiedaten finden Sie im Anhang in dem Notebook [SYE-energydatasources.ipynb](../nb/SYE-energydatasources.ipynb). Gerne können Sie weitere Quellen aus dem Internet nutzen. Das Thema open data nimmt gerade an Fahrt auf und es finden sich monatlich neue interessante Repositorien mit Daten.

# Gerätemessungen sollen Sie zudem selbst erzeugen (siehe unten). Der Vorteil bei eigenen Messungen ist, dass Sie selbst das Nutzungsprofil kontrollieren können und somit genau wissen, was Sie wie gemessen haben.

# ### Klimawirkung und Ressourceneffizienz einschätzen

# Für die **Herstellungs- und die Recyclingphase** ist es notwendig, zu recherchieren: wie ist der CO$_2$-Fußabdruck für Herstellung und Recycling? Und welche Materialien werden besonders für dieses Gerät benötigt und in welcher Menge? Sie werden ziemlich sicher nicht für genau das Modell, das Sie selbst vermessen, plausible Daten finden. Es ist eher wahrscheinlich, dass Sie für die Geräteklasse, die Sie untersuchen eine oder mehrere Datenquellen finden. Das ist für die hier geforderte *Einschätzung* in Ordnung. Es geht darum, die Größenordnung zu verstehen.

# Für die **Nutzungsphase** ist es wichtig, dass Sie Annahmen treffen: 
# - beschreiben Sie das Verhalten der betrachteten Nutzenden
# - leiten Sie daraus ab, wie das Leistungsprofil $P(t)$ sein muss
# - betrachten Sie außerdem den Effekt unterschiedlicher Nutzungsdauern von Komponenten, d.h. z. B. Batterie zweimal pro Tag laden statt ersetzen oder Bildschirm weiternutzen trotz schwächerer Beleuchtung (und dementsprechend erhöhtem Stromverbrauch für die Anpassung der Beleuchtung), addieren Sie ggf. zusätzlich nötige Mengen in Ihren Berechnungen

# Der viertelstündlich aufgelöste Strommix findet sich bei [SMARD](https://www.smard.de/home/Ueber_SMARD/668) {cite:p}`bundesnetzagentur_smard_2020` und Sie können die einzelnen Anteile mit den Daten des Umweltbundesamtes zu spezifischen Kohlendioxidemissionen einzelner Energieträger multiplizieren um den spezifischen, zeitabhängigen CO2-Fussabdruck Ihres Nutzungsprofils zu erhalten. Für Herstellung und Recycling von Komponenten und Geräten genügt der summarische Fußabdruck. Für die Nutzung ist jedoch der Vergleich mit erneuerbarer Energie-Nutzung interessant.

# ### Messungen durchführen

# Vermessen Sie in Ihrem Projektteam das für Ihr Projekt relevante Gerät mit dem Energielogger und zusätzlich, wenn möglich, auch mit dem Leistungsanalysator und der Thermografiekamera im Labor. Notieren Sie die unterschiedlichen Betriebsmodi dazu, so dass Sie die Variation des Nutzerverhaltens analysieren können.

# Erstellen Sie aus den Messungen und den Verbrauchsdaten aus der Literatur Jahreslastprofile für Ihren Anwendungsfall. 

# Dokumentieren Sie jeweils Art, Typ und Name des Geräts, den Inhalt des Typenschilds und des Datenblatts im jupyter Notebook. Verlinken Sie die Messdaten als csv mit einem lokalen Link, so dass sich das jupyter notebook auch auf einem anderen Rechner mit diesen Daten im gleichen Ordner ausführen lässt. Dokumentieren Sie im Jupyter Notebook ebenfalls die Annahmen, die Sie treffen über die Häufigkeit und Art der Nutzung und des Einsatzes. Wichtig ist, dass diese Annahmen begründet sind und typische Nutzungsarten wiederspiegeln.

# ### Maßnahmen zur Verbesserung der Klimawirkungen und des Energieverbrauchs evaluieren

# Aus den gemessenen und den aus dem Internet erhaltenen Daten lassen sich einige Rückschlüsse ziehen.
# Sie können damit bereits Vorschläge für einerseits eine verbesserte Klimawirkung (d.h. einen erhöhten
# Anteil erneuerbarer Erzeugung an der Last) und andererseits für Energiesparmaßnahmen begründen
# und evaluieren.

# Es gibt allgemein 6 verschiedene Arten von Energiesparmaßnahmen (nach [Hesselbach 2012](https://www.springer.com/de/book/9783834804488) {cite:p}`hesselbach_energie-_2012`):
# 1. Energienutzung vermeiden
# 2. Energiebedarf verringern
# 3. Wandlerverluste reduzieren
# 4. Temperaturniveaus anpassen
# 5. Wirkungsgrade steigern
# 6. Integration von Energieströmen

# Überlegen Sie sich je Projektteammitglied je eine Energieeinsparmaßnahme und analysieren Sie deren möglichen Effekt mit entsprechend angepasst erstellten Lastprofilen in Ihrem Jupyter Notebook.

# Bestimmen Sie auch den ökonomischen und den ökologischen Effekt von einer besonders vielversprechenden Reparaturmaßnahme während der Lebensdauer:
# - Schätzen Sie den Aufwand für Reparatur und Reparaturkomponenten ab
# - Schätzen Sie - auch anhand von Literaturwerten - die Alterung des betrachteten Gerätes und setzen Sie mit linearer Regression eine Verringerung der Energieeffizienz an $\eta(t) = \eta_{\text{neu}}-\Delta \eta_{\text{Jahr}}\cdot\text{Anzahl Jahre}$
# - Lohnt sich die Reparatur ökonomisch?
# - Lohnt sich die Reparatur für den CO$_2$-Fußabdruck? Und lohnt sie sich auch für die Materialeffizienz?

# Evaluieren Sie die Wirtschaftlichkeit jeder Maßnahme mithilfer der Annuitätenrechnung. Für die ökologische Amortisation reicht die einfache Addition der entsprechenden Aufwände.

# ### Projektaufgabenauswahl

# | **Projekt** | **System** | **Nutzungsart** |
# |-|-|-|
# | A | Kühlschrank | lokale Balkon-Photovoltaik-Anlage, Kaffeeküche in einem coworking-Space |
# | B | Kühlschrank | Photovoltaik-Anlage auf dem Garagendach, Einfamilienhaus mit Kleinfamilie |
# | C | Kühlschrank | Insel-Photovoltaik-Anlage, Medizin-Lagerung bei Hilfseinsatz |
# | D | Kühlschrank | Photovoltaik-Mieterstrom, Berufstätigen-WG |
# | E | Waschmaschine | lokale Balkon-Photovoltaik-Anlage, gemeinschaftlich genutzte Maschinen im Waschkeller im Mehrfamilienhaus |
# | F | Waschmaschine | Photovoltaik-Anlage auf dem Garagendach, Einfamilienhaus mit Kleinfamilie |
# | G | Waschmaschine | Insel-Photovoltaik-Anlage, Berghütte |
# | H | Waschmaschine | Photovoltaik-Mieterstrom, Berufstätigen-WG |
# | I | Computer | lokale Balkon-Photovoltaik-Anlage, coworking-Space |
# | K | Computer | Photovoltaik-Anlage auf dem Garagendach, Einfamilienhaus mit Kleinfamilie |
# | L | Computer | Insel-Photovoltaik-Anlage, Berghütte |
# | M | Computer | Photovoltaik-Mieterstrom, Berufstätigen-WG |
# | N | Computer | Insel-Photovoltaikanlage Low Tech Mag Server, siehe nächste Zeile |

# *Hinweis mit Link für Projekt N:* [Solar Low Tech Mag Server](https://solar.lowtechmagazine.com/about.html)

# Alles, was nun noch nicht definiert ist und sich auf Ihr Nutzerverhalten auswirkt, müssen Sie selbst definieren und dafür begründete Annahmen treffen.

# <div class="alert alert-block alert-warning"><b>Wichtig: Gerät verfügbar bei Ihnen zuhause! </b>Es gab in den letzten Jahren immer wieder Studierende, die die Messung an Rechnern der TH durchführen wollten. Dies ist schwierig zu organisieren und in Corona-Zeiten noch schwieriger. Daher ist es <b>wichtig, dass Sie das ausgewählte Gerät bei sich zuhause haben und messen können </b>. Wenn Sie bei der Anmeldung für das Projektteam feststellen, dass dann keins der Projektteams für Sie machbar ist, bitte ich Sie, sich bei mir zu melden. Dann kann ich noch ein alternatives Gerät mit Ihnen auswählen.</div>

# ## Projektdokumentation

# Berücksichtigen Sie folgende Situationen bei der Dokumentation: 

# - *für sich selbst und Ihre Kollegen nachvollziehbar arbeiten:* so, dass ein Kollege (oder Sie selber nach einer Pause) rasch nachvollziehen können, wie Sie vorgegangen sind, welche Annahmen Sie getroffen haben und welche Messungen bzw. Berechnungen Sie durchgeführt haben
# - *für Ihren Chef oder Ihren Kunden:* der keine Zeit hat, die ganze Studie selbst zu machen, of auch nicht einmal alles zu lesen, rasch einen Überblick und nachvollziehbare Handlungsempfehlungen zu geben
# - *Zwischenstände:* wenn Sie offene Fragen oder Unstimmiges deutlich aufschreiben, wissen Sie gleich, wo Sie weiterarbeiten können, wenn Sie zwischendurch anderes bearbeitet haben

# Versetzen Sie sich in die Rolle eines internen oder externen *Energieberaters*: Ihr Chef oder Ihre (ggf. potenziellen, noch zu akquirierenden) Kunden sind die Adressaten Ihrer Präsentation. Aus Kundensicht schaffen Sie dann einen Mehrwert, wenn Sie nachvollziehbare, konkrete, fachlich fundierte, Empfehlungen aus vielen Einzelinformationen (Messdaten, Recherchen, Berechnungen, ...) zusammenfassen und ableiten. Der Kunde will wissen, ob er Ihren Daten vertrauen kann und wird Nachfragen stellen bzw. in den Daten nach Widersprüchen suchen. Sie generieren dann mit Ihrer Arbeit einen Mehrwert, wenn Ihr Kunde durch eine kurze Präsentation oder einen knappen, präzisen Bericht, die Ergebnisse Ihrer mehrmonatigen Studie lernen kann.

# Wichtig sind mir daher folgende Kriterien bei der Dokumentation:
# - vollständig und nachvollziehbar dokumentierte Annahmen, Rechnungen, Tabellen, Literaturangaben, python-Code, Messdaten, etc.
# - übersichtliche und eigenständig verständliche Foliendarstellung
# - Quellen nachvollziehbar zitieren, siehe auch IEEE-Richtlinien
# - Bilder nur mit Literaturangabe und
#  - falls Personen darauf zu sehen sind, nur mit deren Genehmigung
#  - nur, falls lizensiert (mit korrekter Lizenzangabe) oder Genehmigung des Bildinhabers 
# - alle Annahmen und Schätzungen begründet und nachvollziehbar
# - fremde und eigene Daten als solche kennzeichnen

# Falls Sie fremde Bilder verwenden: Bitte fügen Sie eine unterschriebene Erklärung bei mit dem Inhalt *Ich habe die Genehmigung für ... von ... erhalten und bestätige dies hiermit* oder geben Sie alternativ die geltende Lizenz an, unter der Sie die Bilder verwenden dürfen.

# Bitte achten Sie darauf, dass Sie zusammen mit den Daten auch die Metadaten dokumentieren: 
# - alle Randbedingungen der Messung, Einstellungen der Messgeräte - danmit Sie nicht nochmal messen müssen, wenn Sie später weitere Aspekte auswerten 
# - Dateinamen mit eindeutiger Zuordnung zu den Messungen

# <div class="alert alert-block alert-warning"><b>Mit Kopie-Daten arbeiten: </b>Speichern Sie Messdaten und Berechnungsdateien an einem sicheren Ort und legen Sie Versionen
# an bzw. arbeiten Sie an Kopien von Datendateien. So haben Sie Ihre Original-Daten noch, falls die
# Kopie-Daten bei der Bearbeitung Schaden nehmen.</div>

# <div class="alert alert-block alert-warning"><b>Daten sichern: </b>Auch Bilder, Literaturquellen, Jupyter Notebooks (ohne den Output benötigen Sie wesentlich
# weniger Speicherplatz), Excel-Tabellen zur Auswertung und für Anforderungen, handschriftliche
# und digitale Notizen sind Daten und gehören an einen sicheren Ort.</div>

# <div class="alert alert-block alert-success"><b>Die Präsentation fasst zusammen, was man aus der Untersuchung lernen kann. D.h. es ist wichtig, dass Sie relevante Randbedingungen wiederholen, damit man Ihre Vorgehensweise versteht. Ebenso ist es wichtig, die Ergebnisse zu diskutieren und ein Fazit zu bilden, mit den wichtigsten Erkenntnissen und weiteren Fragestellungen für später (Ausblick). Wenn Sie vorher nachvollziehbar gearbeitet haben und sauber dokumentiert haben, ist es ziemlich wahrscheinlich, dass Sie glaubwürdige (stimmige) Ergebnisse präsentieren werden, die auch einen potenziellen Chef oder Kunden in Ihre Arbeit vertrauen lassen.</b></div>

# ## Formales

#  Das, was Sie in den Meilensteinen zeigen, wird dann bewertet, wenn sich in der Endabgabe offene Punkte finden. Sie dürfen also auch für die Meilensteine noch Zwischenstände zeigen, die noch nicht fertig sind und diese Ergebnisse später noch korrigieren. 

# - *Meilenstein 1*: 5 min. pro Projekt folgendes präsentieren: erste Messung mit Energielogger in jupyter notebook ausgewertet und Anforderungen an System in erster Version definiert, zum [Abgabetermin](termine) in ilias alle zugehörigen Unterlagen hochladen und am [Meilensteintermin 1](termine) präsentieren

# - *Meilenstein 2*: 5 min. pro Projekt folgendes präsentieren: erste Energiesparmaßnahme mithilfe von jupyter Notebook Analyse von Lastprofilen ausgewertet, zum [Abgabetermin](termine) in ilias alle zugehörigen Unterlagen hochladen und am [Meilensteintermin 2](termine) präsentieren

# - *Abgabe der Unterlagen*: zum [Projektabgabetermin](termine) in ilias

# - *Abzugebendes Verzeichnis*: bei Abgabe in ilias komplettes Verzeichnis zippen, in dem sich jupyter notebook mit Präsentationseinstellungen oder Präsentationsfolien befinden sowie alle Messdateien, Thermografiebilder, Bewertungstabellen, etc., die zur Bearbeitung des Projektes gedient haben - testen Sie, ob sich das jupyter notebook dann von jemand anderem ausführen lässt und fügen Sie ggf. dann noch fehlende Dateien zum Verzeichnis hinzu

# <div class="alert alert-block alert-danger"><b>Wichtige Formatierungshinweise: </b>
# 
# - Bitte geben Sie für die Meilensteine und die Enddokumentation jeweils **nur eine Abgabe pro Projektteam** ab. 
# - <b>jpg</b>-Dateien immer <b>kleingeschrieben</b> mit `.jpg` am Ende benennen, sonst Probleme in jupyterhub!
# - alle anderen Dateien auch <b>mit kleingeschriebenen Endungen</b> versehen
# - Dateien sinnvoll benennen, z. B. `2021-04-01_aprilscherz.ipynb` und nicht `alte_version_andere_messung_neu_2b.ipynb`
# - <b>Leerzeichen in Dateinamen durch `_` </b> ersetzen (sonst Probleme mit Links in Markdown)
# - <b>ß, ä, ö, ü in Dateinamen durch `ss`, `ae`, `oe`, `ue`</b> ersetzen (sonst Probleme beim Einlesen im Code)
# - <b>Bilder nicht per drag and drop</b> sondern <b>in mit abzugebendem Ordner `img`</b> einbinden, denn sonst werden die nur auf den Rechnern dargestellt, die Zugriff auf dieses Verzeichnis haben und bei mir nicht
# - wenn <b>Dateinamen geändert werden</b> unbedingt <b> Notebooks nochmal laufen lassen (`restart and run all`) und ggf. Links an neue Dateinamen anpassen</b>
# - <b>alle verlinkten Dateien mit abgeben</b> mit Ausnahme von im Internet frei verfügbaren
# - für Präsentation mit RISE wichtige Aspekte auswählen und vorher üben **im jupyterhub** (5 min. sind kurz)
# </div>

# - *Dateinamenskonvention*:
#  - 1990-05-18_SYE_ProjektA_Thermografie_Bildschirm_LG_IPS231.png
#  - 1995-08-20_SYE_ProjektB_Anforderungen_Smartphone.xlsx

# - *Sichtbarmachen individueller Leistungsanteile:* Tabelle mit abgeben, in der für jedes Arbeitspaket prozentual der Anteil jedes Teammitglieds (mit Name und Matrikelnummer steht und Hauptanteile jeweils selbst präsentieren

# - *Präsentationsablauf*: pro Projekt 15 Minuten für die Präsentation und 5 Minuten für die Diskussion

# - *Bewertung:* hauptsächlich das präsentierte Ergebnis und das jupyter notebook, stichprobenartig auch eingereichte Unterlagen
# - *Bewertungskriterien:* nachvollziehbare Analyse und Evaluation, d.h. begründete und dokumentierte Annahmen, Berechnungen und Messungen, daraus begründete Ableitung von Empfehlungen für die nächsten Schritte
# - *Ergebnisbewertung, wenn der Ist-Zustand schon gut ist:* auch die begründete Erkenntnis, dass eine Maßnahme nicht den erhofften Verbesserungserfolg bringt, ist eine wertvolle Erkenntnis, denn dann kann man sich unnötige Aufwände für weitere Umsetzungen sparen und sich auf die Maßnahmen konzentrieren, die etwas bringen
# - *Unentschuldigtes Fehlen oder verspätetes Einreichen der Unterlagen:* Attest notwendig, sonst Bewertung 5,0 der Teilprüfungsleistung Projekt und damit Nicht-Bestehen des Moduls
# - *Plagiate:* Bewertung mit 5,0
# - *Literatur:* Zitationsregeln beachten, siehe z. B. die Unterlagen hier

# **Ich wünsche viel Erfolg und viel Freude bei der Arbeit an den Projekten und bin schon gespannt auf die Ergebnisse!**

# ## Literatur

# ```{bibliography}
# :filter: docname in docnames
# ```
