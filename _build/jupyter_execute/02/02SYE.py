#!/usr/bin/env python
# coding: utf-8

# # Mit python Energiedaten analysieren

# In diesem Kapitel geht es darum, mit python arbeitsfähig zu werden. Es gibt Installationshinweise und eine erste geführte Energiedatenanalyse. Diejenigen, die schon Vorkenntnisse haben, werden schneller zurecht kommen, die anderen werden etwas mehr Zeit einplanen müssen. Aber das lohnt sich: Wenn Sie jetzt an der Nutzung von python dranbleiben, kommen Sie während des Semesters gut ins Arbeiten und es wird Ihnen dann relativ leicht fallen, ein gutes Projekt-Notebook zu erstellen. Es empfiehlt sich, jetzt schon mit dem jupyterhub zu üben, um die richtige Version aller Module nutzen zu können.
