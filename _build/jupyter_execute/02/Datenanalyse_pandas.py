#!/usr/bin/env python
# coding: utf-8

# # Datenanalyse mit pandas

# *Dieses Notebook wird im Video LINK EINFÜGEN verwendet.*

# In[1]:


import pandas as pd


# pandas ist ein Python-Modul, das viele Funktionalitäten und Datentypen enthält, welch das Arbeiten mit Daten sowie deren Analyse erleichtern.
# 
# Zur Visualisierung der Daten können anschließend Module wie matplotlib, plotly oder seaborn genutzt werden. Nähere Informationen zur Visualisierung der Daten gibt es in einem [separaten Notebook](bla).
# 
# Bei diesem Notebook liegt der Fokus auf der Beschreibung des wesentlichen Datentyps von pandas: dem DataFrame.

# <div class="alert alert-block alert-info"><b>Inhalt:</b>
#     
# - Das DataFrame
#     
#     
# - Erzeugen eines DataFrames
#     
#     
# - Die wichtigsten Eigenschaften eines DataFrames
#     
#     
# - Weitere hilfreiche Befehle
#     
#     
# - Daten in DataFrame auswählen (indexieren)

# ## Das DataFrame

# Das DataFrame ist die grundlegende Datenstruktur von pandas. Die offizielle Beschreibung von [pandas](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html?highlight=dataframe#pandas.DataFrame) lautet:<br><br>
# 
# <div align="center"><i>Two-dimensional, size-mutable, potentially heterogeneous tabular data.<i></div>
# 
# Die wesentliche Aussage: ein DataFrame ist eine zweidimensionale Tabelle!
#     
# Somit besteht ein DataFrame aus Zeilen und Spalten.

# <div class="alert alert-block alert-success", align="center"><b>Ein DataFrame ist eine Tabelle.</b></div>

# pandas nutzt außerdem Konzepte von ndarrays aus numpy und Listen aus dem Python-Standard. Somit kann es nicht schaden, hier mal einen Blick zu riskieren... 
# - ndarray von [numpy](https://numpy.org/doc/stable/reference/arrays.ndarray.html#arrays-ndarray)
# - Liste von [Python-Dokumentation](https://docs.python.org/3.8/library/stdtypes.html#sequence-types-list-tuple-range)

# ## Erzeugen eines DataFrames

# Ein DataFrame kann mit dem Befehl `pd.DataFrame` erzeugt werden. Zunächst muss festgelegt werden, welche Daten das DataFrame enthalten soll. Grundsätzlich gibt es hier zwei Möglichkeiten:
# - Daten selbst erzeugen 
# - bestehende Daten aus einer Datei einlesen
# 
# Hier wird zunächst nur das Erzeugen eines DataFrames mit eigenen Daten vorgestellt (weitere Beispiele dazu gibt es z.B. [hier](https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.html?highlight=dataframe#pandas.DataFrame)).
# 
# Im Folgenden wird als Beispiel eine simple Version von "Stadt-Land-Fluss" als Daten genutzt.

# ### DataFrame aus Dicionary

# Ein Dictionary ist ein Standard-Datentyp von Python (siehe [hier](https://docs.python.org/3.8/library/stdtypes.html#mapping-types-dict)). An dieser Stelle wird nicht genauer auf Dictionaries eingegangen - es wird lediglich die Möglichkeit, aus einem Ditionary ein DataFrame zu erzeugen, vorgestellt.

# In[2]:


data_dict = {'Buchstabe':list('abcde'),
             'Stadt':['Augsburg','Berlin','Chemnitz','Düsseldorf','Erlangen'],
             'Land':['Argentinien','Brasilien','Chile','Deutschland','England'],
             'Name':['Anton','Berta','Carl','Domenik','Elena']}


# In[3]:


df_dict = pd.DataFrame(data=data_dict)


# In[4]:


#df_dict


# ### DataFrame aus Liste

# Eine Liste ist ein weiterer Standard-Datentyp von Python (siehe [hier](https://docs.python.org/3.8/library/stdtypes.html#list)). Auch hier wird nicht genauer auf die Liste eingegangen. Sie dient lediglich als Beispiel.

# In[5]:


data_list = [['a','Augsburg','Argentinien','Anton'],['b','Berlin','Brasilien','Berta'],
             ['c','Chemnitz','Chile','Carl'],['d','Düsseldorf','Deutschland','Domenik'],
             ['e','Erlangen','England','Elena']]


# In[6]:


df_list = pd.DataFrame(data=data_list, columns=['Buchstabe','Stadt','Land','Name'])


# In[7]:


#df_list


# ### DataFrame aus Series

# Eine Series ist ein weiterer Datentyp von pandas (siehe [hier](https://pandas.pydata.org/docs/reference/api/pandas.Series.html?highlight=series#pandas.Series)). Auch die Series dient hier lediglich wieder als Beispiel.

# In[8]:


series_a = pd.Series(data=['a','Augsburg','Argentinien','Anton'])
series_b = pd.Series(data=['b','Berlin','Brasilien','Berta'])
series_c = pd.Series(data=['c','Chemnitz','Chile','Carl'])
series_d = pd.Series(data=['d','Düsseldorf','Deutschland','Domenik'])
series_e = pd.Series(data=['e','Erlangen','England','Elena'])


# In[9]:


df_series =pd.DataFrame(data=[series_a,series_b,series_c,series_d,series_e])
df_series.rename({0:'Buchstabe',1:'Stadt',2:'Land',3:'Name'}, axis='columns', inplace=True)


# In[10]:


#df_series


# ### Zusammenfassung

# Es sind drei identische DataFrames aus selbst erzeugten Daten erstellt worden, allerdings auf unterschiedliche Arten. Es gibt also immer mehrer Möglichkeiten, um eine bestmmte Aufgabenstellung zu lösen.
# 
# ... dies gilt auch in anderen Bereichen.

# <div class="alert alert-block alert-success", align="center"><b>Es gibt mehrere Möglichkeiten,
# ein DataFrame aus selbst erzeugten Daten zu erstellen.</b></div>

# ## Die wichtigsten Eigenschaften eines DataFrames

# Ein DataFrame hat viele Eigenschaften (Attribute) und Fähigkeiten (Methoden). Im Detail sind diese alle in der [pandas Dokumentation](https://pandas.pydata.org/docs/reference/frame.html) gelistet. Hier werden lediglich die grundlegendsten Befehle aufgeführt, mit denen man sich einen groben Überblick über das DataFrame verschaffen kann.

# ### Die Dimensionen eines DataFrames

# Die Dimension eines DataFrames (also die Anzahl an Zeilen und Spalten) lässt sich mit dem Befehlt `.shape` abfragen.

# In[11]:


df_list.shape


# ### Anzahl der Elemente im DataFrame

# Die Anzahl der Elemente in einem DataFrame, lässt sich mit dem Befehl `.size` abfragen.

# In[12]:


df_dict.size


# ### Datentypen im DataFrame

# Die Datentypen in einem DataFrame lassen sich mit dem Befehl `.dtypes` abfragen.

# In[13]:


#df_list.dtypes


# ### Überblick über das DataFrame

# Ein schneller Überblick über das DataFrame lässt sich mit dem Befehl `.info()` gewinnen.

# In[14]:


df_series.info()


# ### Zusammenfassung

# Mit den zuvor aufgeführten Befehlen lassen sich bereits die wichtigsten Informationen über das DataFrame abfragen.

# <div class="alert alert-block alert-success", align="center"><b>Bereits mit wenigen Befehlen lässt sich ein guter Überblick über das DataFrame gewinnen.</b></div>

# ## Weitere hilfreiche Befehle

# Nachdem man sich einen ersten Überblick über das DataFrame verschafft hat, kann das DataFrame bei Bedarf weiter bearbeitet werden. Im Folgenden werden die wichtigsten Befehle gelistet.

# ### Einen eigenen Index verwenden

# Standardmäßig werden die Zeilen eines DataFrames von 0 ausgehend ganzzahlig nummeriert. Daraus ergibt sich automatisch der Standard-Index (das ist die Spalte ganz links bei der graphischen Ausgabe). Meistens ist es sinnvoller, einen selbst definierten Index zu verwenden. In dem hier betrachteten Beispiel wäre es sinnvoll, die Spalte mit den Buchstaben als Index zu verwenden.
# 
# Der Index des DataFrame lässt sich mit dem Befehl `.set_index()` festlegen.

# In[15]:


df_list.set_index('Buchstabe', inplace=True)


# In[16]:


#df_list


# ### Spalten/Zeilen umbenennen

# Die bestehenden Spalten eines DataFrame lassen sich mit dem Befehl `.rename()` umbennen.

# In[17]:


df_list.rename({'Name':'Vorname'}, axis='columns', inplace=True)


# In[18]:


#df_list


# Genauso lassen sich auch die Zeilen umbennen (was in dem hier betrachteten Beispiel nicht unbedingt sinnvoll ist - vollständigkeitshalber sei es aber dennoch erwähnt).

# In[19]:


df_list.rename({'a':'A'}, axis='index', inplace=True)


# In[20]:


#df_list


# ### Spalten hinzufügen/entfernen

# Zusätzliche Spalten lassen sich mit der folgenden Schreibweise hinzufügen.

# In[21]:


df_list['Tier'] = ['Affe','Bison','Chinchilla','Dromedar','Elefant']


# In[22]:


#df_list


# ... hier gibt es aber auch noch andere Möglichkeiten (z.B. [hier](https://www.delftstack.com/de/howto/python-pandas/how-to-add-new-column-to-existing-dataframe-in-python-pandas/)).

# Spalten können mit dem Befehl `.pop()` oder `.drop()` entfernt werden.

# In[23]:


df_list.pop('Vorname')


# In[24]:


#df_list


# In[25]:


df_list.drop('Stadt', axis='columns', inplace=True)


# In[26]:


#df_list


# ### Zeilen hinzufügen/entfernen

# Zusätzliche Zeilen lassen sich mit dem Befehl `.loc[]` hinzufügen.

# In[27]:


df_list.loc['f'] = ['Frankreich','Fasan']


# In[28]:


#df_list


# ... und auch hier existieren noch andere Möglichkeiten (z.B. [hier](https://www.delftstack.com/de/howto/python-pandas/how-to-add-one-row-to-pandas-dataframe/)).

# Zeilen können mit dem Befehl `.drop()` entfernt werden.

# In[29]:


#df_list.drop('c', axis='index')


# ### Zusammenfassung

# Mit den zuvor aufgeführten Befehlen lässt sich das DataFrame noch nachträglich ereitern/verändern.

# <div class="alert alert-block alert-success", align="center"><b>Nach dem Erstellen lässt sich das DataFrame noch weiter
# anpassen.</b></div>

# ## Daten im DataFrame auswählen (Indexieren)

# Durch Indexieren lassen sich geziehlt einzelne Elemente oder auch bestimmte Teilmengen eines DataFrames ansprechen. Im Folgenden werden unterschiedliche Arten der Indexierung vorgestellt.
