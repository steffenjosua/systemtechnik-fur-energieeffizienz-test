#!/usr/bin/env python
# coding: utf-8

# # Kleine python Einführung

# <div class="admonition note" style="background: #e5f1ff; padding: 10px">
# <div class="title"><b>Lernziele für diese Einheit:</b></div>
#     <ul>
#         <li> arbeitsfähig werden: Jupyter Hacks </li>
#         <li> Zusammenarbeit: organisieren mit gitlab und jupyterhub</li>
#         <li> Wichtige python Befehle für Energiedatenanalyse </li>
#     </ul>
# </div>

# ## python installieren

# Zum Vorgehen bei der Installation siehe auch das [Video](https://vimeo.com/624422901/e58459592d).

# Im Video finden Sie eine Anleitung zur Installation von python auf Ihrem Rechner (mit der [anaconda](https://www.anaconda.com) Distribution). Sie können alternativ auch den [jupyterhub](https://jupyterhub.th-koeln.de) der TH Köln nutzen, wenn Sie sich im Netz der TH befinden (ggf. [VPN](https://www.th-koeln.de/hochschule/vpn---virtual-private-network_26952.php) nutzen). Wir schalten dafür alle Nutzer im ILIAS-Kurs frei.

# Nun öffnen Sie auf Ihrem Rechner jupyter oder den jupyterhub im Browser.

# Außerdem können Sie die Dateien auch in binder ausführen (allerdings können dort keine Änderungen gespeichert werden):

# [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/jfmay%2Fsystemtechnik-fuer-energieeffizienz/HEAD)

# ## Jupyter Hacks und Ihr erstes Jupyter Notebook

# - Einführung in Python Teil 1: zum [Video](https://vimeo.com/624420798/f4cee39f97)
# - Einführung in Python Teil 2: zum [Video](https://vimeo.com/624420855/2577e7dffe)
# - Einführung in Python Teil 3: zum [Video](https://vimeo.com/624420922/473a1dfd23)

# Im Video finden Sie Beispiele für erste Schritte mit python und jupyter. 

# Weitere Anleitungen finden sich in sehr vielen (oft kostenlosen) Online-Tutorials wie diesem [Dataquest Jupyter Notebook Tutorial](https://www.dataquest.io/blog/jupyter-notebook-tutorial/) {cite:p}``pryke_how_2020``.

# ### Interaktiv verwenden

# In[1]:


2+2


# Klicken Sie in das Fenster mit der Rechnung, ändern Sie die Zahlen und drücken Sie dann Shift+Enter oder oben im Menü Run. Probieren Sie auch andere Rechnungen aus.

# In[2]:


a = 2
b = 2
print(a+b)


# Wir nehmen mal an, wir haben eine Reihe von Daten gemessen: Die Viertelstundenmittelwerte der Leistung eines bestimmten Haushalts betragen an 10 aufeinanderfolgenden Messzeitpunkten 3 kW, 8 kW, 9 kW, 10 kW, 2 kW, 1 kW, 4 kW, 9 kW, 1 kW, 1 kW. Die Daten wurden in einer Exceltabelle notiert und für den Bericht nun abgetippt in Word. 

# Nach etlichen Wochen der Beschäftigung mit dem Thema stellen wir fest, dass der Messpunkt 2 falsch ist: Es sind nicht 8 kW, sondern nur 3 kW. Nun ändern wir das natürlich in der Excel-Tabelle. Der Word-Bericht wird aber vergessen und es gibt keine automatische Verlinkung. Im Word-Bericht steht nun also die falsche Zahl.

# In Jupyter Notebook könnten wir die Daten einlesen (dazu später, wie das geht). Hier geben wir die Daten in einer Variable ein.

# In[3]:


p = [3,3,9,10,2,1,4,9,1,1] # Leistung in kW


# Nun können wir die Variable p im Jupyter Notebook unterhalb der Definition immer wieder verwenden. Man muss also **nur an dieser einen Stelle** den Wert ändern und kann dann folgerichtig (ohne Folgefehler) damit weiterarbeiten.

# In[4]:


print('Leistung zum Zeitpunkt 3: ',str(p[2]),'kW')


# Ändern Sie in der Definitionszeile einzelne Werte in p. Führen Sie dann zur Übertragung in der Variable erneut die Zelle aus (mit Shift-Enter). Probieren Sie dann, was der print-Befehl ausgibt. 

# Es wird also wahrscheinlicher, dass keine Copy-Paste-Fehler bei der Übertragung von Daten passieren und dass Fehler in den Daten dadurch besser auffallen.

# **AUFGABE** Was können Sie alles tun, um zu *vermeiden*, dass Ihre Dokumentation im Projekt dieses Semester nachvollziehbar wird?

# *Antwort:*...

# ### Shift-Enter

# Zum interaktiven Durchlesen des Notebooks einfach nach jeder Zelle Shift-Enter drücken, so kommen Sie Schritt für Schritt durch das Kapitel. 

# Weitere Keyboard Shortcuts finden Sie im Menü oben, wenn Sie auf das Tastatursymbol klicken.

# ### Was kann ein Jupyter Notebook eigentlich alles?

# Ein Notebook oder auch elektronisches Laborbuch integriert
# - Code
# - Output von Code (Diagramme, Tabellen, berechnete Werte, Formeln, etc.)
# - erklärenden Text im Markdown-Format
# - mathematische Gleichungen
# - andere Medien

# Idee: während man arbeitet (rechnet, simuliert) in lesbarer Form (Zwischen-)Ergebnisse dokumentieren.

# ### Dashboard

# Wenn Sie jupyter notebook gestartet haben, sehen Sie etwas ähnliches wie hier: 

# ![Dashboard Beispiel](https://www.dataquest.io/wp-content/uploads/2019/01/jupyter-dashboard.jpg)

# Das ist noch kein Notebook. 

# Das ist das **Dashboard**. Dort kann man alle Notebooks in einem Ordner sehen und verwalten. - So eine Art Datei-Explorer also für Jupyter Notebooks. 

# **Auf Ihrem Rechner**: Die URL vom Dashboard ist z. B. http://localhost:8888/tree#notebooks Localhost ist keine Webseite, sondern zeigt an, dass der Inhalt auf einem Server auf Ihrem Rechner bereitgestellt wird. Jupyter Notebooks sind Web Apps und Jupyter startet diesen lokalen python Server, um diese Apps
# in Ihrem Browser zuöffnen. So ist Jupyter praktisch unabhängig von der Plattform und es ist einfach, solche Dokumente auch im Netz zu teilen.

# **Im jupyterhub**: Nun ist der Server der jupyterhub der TH Köln und nicht auf Ihrem Rechner. Der Vorteil ist, dass alle, die damit arbeiten, die gleichen Versionen der python-Bibliotheken nutzen und dass es nicht auf die Rechenfähigkeiten Ihres Rechners ankommt, sondern auf diejenigen des TH-Servers.

# ### Neues Notebook erstellen

# ![hier klicken für neues Notebook](https://www.dataquest.io/wp-content/uploads/2019/01/new-notebook-menu.jpg)

# Klicken Sie im Dashboard oben rechts auf Neu und wählen Sie python 3 aus. 

# Jedes Notebook wird in einem neuen Browser-Tab geöffnet.

# Zurück im Dashboard sieht man nun die neue Datei *Untitled.ipynb* in grünem Text, der anzeigt, dass dieses Notebook gerade läuft.

# ### .ipynb Datei

# Text-Datei im Format JSON

# jede Zelle und ihre Inhalte, inklusive Bild-Anhänge werden dort gelistet zusammen mit Metadaten

# *wenn* man weiß, was man tut, kann man direkt diese Daten editieren mit edit $\to$ edit notebook metadata (aber nicht empfehlenswert, wenn man nicht weiß, was man tut!)

# ### Notebook Ansicht

# ![so könnte Ihr erstes Notebook jetzt aussehen](https://www.dataquest.io/wp-content/uploads/2019/01/new-notebook.jpg)

# sehen Sie sich mal in Ihrem neuen Notebook die Menüs an, scrollen Sie dort herum und schauen Sie, was es alles gibt

# ### Zellen (Cells) und Kernel

# *Kernel*: Rechenmaschine, die den Code ausführt

# *Zelle*: ein Container für Text, der im Notebook dargestellt oder Code, der im Notebook-Kernel ausgeführt werden soll

# ### Zellen

# Klicken Sie auf das + Symbol in der Symbol-Leiste: damit erhalten Sie unterhalb Ihrer Maus eine neue, leere, Zelle.

# Zelltypen: 
# - *Code-Zellen*: enthalten Code, der ausgeführt wird im Kernel, Output wird unterhalb dargestellt
# - *Markdown-Zellen*: enthalten Text, der mit Markdown formatiert wird und direkta auch so dargestellt wird

# ### Hello World

# Tippen Sie in die erste Zelle Ihres neuen Notebooks `print('Hello World!')`

# und klicken Sie auf Run oder drücken Sie Strg+Enter

# ### Speichern und Checkpoint

# mit Strg+S speichern Sie mit dem Kommando Save and Checkpoint (oder mit dem Diskettensymbol oben links)

# checkpoint Datei: `.ipynb_checkpoints` enthält alle 120s ein automatisch gespeichertes Backup

# mit save and checkpoint werden das Notebook und das Backup aktualisiert

# zurück zum letzten Checkpoint kommt man mit File $\to$ revert to checkpoint

# Nach dem Ausführen wird die Zelle eine Nummer erhalten haben mit `In[1]`. Die Ausgabe des Codes gehört auch zum Dokument.

# Führt man die Zelle ein zweites Mal aus, so ändert sich ihre Nummer auf `[2]`, weil sie als zweites auf dem Kernel lief.

# ### Neue Zelle einfügen

# Klicken Sie im Menü auf *Insert* und wählen Sie *Insert Cell Below* für eine neue Code-Zelle unter der Ersten. Probieren Sie den folgenden Code, um zu sehen, was passiert.

# In[5]:


import time
time.sleep(3)


# Diese Zelle erzeugt keine Ausgabe, sondern benötigt einfach 3 Sekunden bei der Ausführung.

# Beachten Sie, dass jupyter anzeigt, dass die Zelle gerade läuft mit dem Symbol `[*]`.

# ### Aktive Zellen

# Während Sie in einer Zelle etwas schreiben, wird ihr Rand grün.

# Während eine Zelle ausgeführt wird, wird ihr Rand blau.

# ### Markdown

# ein bisschen ähnlich wie *LaTeX* oder HTML (aber **viel** einfacher)

# eine einfache Sprache, um Text zu formatieren

# Um Bilder einzufügen, gibt es drei Optionen:
# - Verwenden Sie die URL zu einem Bild im Internet.
# - Verwenden Sie eine lokale URL zu einem Bild, dass Sie zusammen mit Ihrem Notebook aufbewahren, z. B. im selben Verzeichnis.
# - Einen Anhang können Sie mit Edit → Insert Image einfügen. Dies wird das Bild in einen Text-String konvertieren und in der .ipynb Datei speichern (die dadurch viel größer wird!).

# Weiteres zu Markdown auch im [offiziellen Markdown-Guide](https://daringfireball.net/projects/markdown/syntax) und in {cite:p}`hannan_satopay_ultimate_2019`.

# <div class="admonition hint" style="background: #e9f6ec; padding: 10px">
# <div class="title"><b>Für Experimentierfreudige:</b></div>
# Wer noch mehr Funktionen nutzen möchte als jupyter kann und nicht vor einem ungewöhnlichen Editor zurückschreckt, kann den emacs Editor mit org-mode verwenden. Dort gibt es einen Modus, der sich org-babel nennt. Mit dem emacs Editor erstelle ich Skripte in LaTeX, die auch python-Grafiken enthalten. Man kann auswählen, welche Blöcke für das Skript oder eine Veröffentlichung exportiert werden und umfangreiche Layout-Einstellungen vornehmen (wie in LaTeX). Ein ziemlich gutes und umfangreiches Tutorial findet sich (auf französisch und auf englisch) bei <a href="https://learninglab.inria.fr/en/mooc-recherche-reproductible-principes-methodologiques-pour-une-science-transparente">INRIA</a>.</div>

# ### Kernel

# Hinter *jedem* Notebook läuft ein Kernel.

# Wenn Sie eine Code-Zelle ausführen, wird der Code in diesem Kernel ausgeführt.

# Der Zustand des Kernels bleibt erhalten und gehört zum Dokument dazu.

# z. B. reicht es, Module in einer Zelle zu importieren, um sie in der nächsten weiterzuverwenden

# ## Motivation für integrierte Dokumentation

# Ingenieure und Programmierer schreiben ungern Dokumentation.
# <br><br>
# Sie probieren lieber neue Features und Ideen aus.
# <br><br>
# Aber: nach einiger Zeit haben sie vergessen, was herauskam bei der Ausprobiererei und müssen das Experiment / die Messung / die Simulation aufwendig wiederholen (oder treffen falsche Entscheidungen).
# <br><br>
# Gilt für wissenschaftliche Arbeiten und auch für technische Dokumentation von Produkten, Laborversuchen, Messungen, etc.

# ### Reproducible Research

# Verwendet man geschlossene Software-Werkzeuge, deren Code nicht einsehbar ist, kann man bei unplausiblen Ergebnissen diese nicht erklären.

# Beispiel: toter Lachs (aus dem Supermarkt) im MRT $\to$ Hirnströme gemessen {cite:p}``gewin_turning_2012``

# $\to$ viele ernsthafte neurowissenschaftliche Ergebnisse hinterfragt!

# ### Reproduzierbare (=nachvollziehbare) Vorgehensweise

# 1. Rohdaten separat aufbewahren (und sichern)
# 2. Auswerten mithilfe von archivierbarer Software (sonst ggf. anderes Ergebnis mit neuer Version)
# 3. am besten: open source Software $\to$ im Code nachvollziehbar, was da passiert
# 4. Dokumentation der Arbeitsschritte
#  1. nicht: unsinnige Daten löschen, sondern: dokumentieren, welche Daten nicht weiter analysiert werden
#  2. sonst sind diese Daten weg, auch dann, wenn man später verstanden hat, dass sie doch Sinn machen
# 5. Beschreiben der Ergebnisse
# 6. Wissenschaft = Arbeit an einer Forschungsfragestellung (nicht nur Ergebnis)
# 7. abschließende Beantwortung von Forschungsfragen sehr viel seltener als Zwischenstände

# Mehr zum Thema reproducible research finden Sie unter diesem Stichwort im Netz oder z. B. unter dem Hashtag #reproducibleresearch auf Twitter. Plattformen, auf denen Messdaten archiviert und geteilt werden sind z.B. zenodo.org. Dort haben z. B. die scientists4future einige Forschungsergebnisse zum Thema CO2-Bepreisung eingestellt: https://zenodo.org/record/2789393. So kann jeder, der möchte, sich die zugrundlegenden Daten ansehen, daran herumspielen und z. B. die Annahmen verändern, um dann zu entscheiden, ob er auf dieselben Schlussfolgerungen kommt. 

# ### Zur Reproduzierbarkeit gehört auch die Software-Version

# Version von python auf dem System ausgeben

# In[6]:


import sys
print(sys.version)


# Version von verwendeten Bibliotheken ausgeben

# meist das Attribut `__version__`

# In[7]:


import matplotlib
print("matplotlib version: ", matplotlib.__version__)
import pandas
print("pandas version: ", pandas.__version__)
import numpy
print("numpy version: ", numpy.__version__)
import seaborn
print("seaborn version: ", seaborn.__version__)


# alle Module und Versionen lassen sich auch einfach ausgeben mit dem Befehl `pip freeze`

# ### Zwischenergebnisse im Text weiterverwenden

# In[8]:


import numpy as np
def square(x):
    return x*x

x = np.random.randint(1,10)
y=square(x)
print('%d zum Quadrat ist %d' % (x,y))


# ### Vorsicht: Zwischenergebnisse sind unabhängig von der Reihenfolge

# In[9]:


print('Ist %d zum Quadrat %d?' % (x,y))


# In[10]:


y=10


# Und jetzt führen Sie die Zelle erneut aus, in der `print` steht.

# ### Ordnung wiederherstellen

# Wenn man zwischendurch was ändert, dann ändert sich nur ein Teil der Zwischenergebnisse im Kernel. 

# Abhilfe:
# - *Restart*: Kernel neu starten, d.h. alle Variablen löschen
# - *Restart & Clear Output*: wie restart plus Ausgabe entfernen
# - *Restart & Run All*: wie oben pluss alle Zellen in der Reihenfolge ausführen
# - *Interrupt*: wenn der Kernel sich aufhängt, kann man ihn so stoppen

# ### Andere Programmiersprachen integrieren

# (machen wir hier nicht im Kurs, aber es sei erwähnt)

# auch Java, C, Fortran, R, MATLAB lassen sich in jupyter ausführen, auch kombiniert

# in SYE: nur python Werkzeuge für Analyse von Datenreihen und Darstellungen derselben

# ### Notebooks benennen

# sowas wie `neueDatei1_final_nochmal_geaendert_entwurf_b2_mueller.ipynb`?

# die .ipynb-Datei entweder umbenennen
# - im Dashboard
# - oder im Dateimanager

# *geht nicht*, solange das Notebook ausgeführt wird, d.h. erst *File* $\to$ *Close and Halt* im Notebook Menü klicken oder im Dashboard *Shutdown*

# dann im Dashboard Datei auswählen (Häkchen setzen) und *Rename*

# <div class="admonition important" style="background: #e9f6ec; padding: 10px">
# <div class="title"><b>AUFGABE</b></div>
# Einigen Sie sich auf eine Dateinamenskonvention für Ihr Projekt (obiges Beispiel finde ich inakzeptabel!)
# </div>

# *Antwort:...*

# ### Beenden

# es reicht *nicht*, den Browser-Tab zu schließen!

# Sie müssen den Kernel Shutdown Befehl ausführen, indem Sie z. B. im Menü *Kernel* $\to$ *Shutdown* auswählen oder indem Sie wie im vorigen Abschnitt gezeigt *Close and Halt* durchführen.

# ### Notebook nun wieder öffnen

# nach der Umbenennung

# ## Zusammenarbeit organisieren mit gitlab

# ### Beispiel: Ablageort für Kursdateien

# Die Dateien für diesen Kurs liegen auf gitlab. Der Link ist: https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz

# Wenn Sie auf ein jupyter notebook (eine .ipynb-Datei) dort klicken, können Sie sich diese anzeigen lassen. Das Notebook lässt sich allerdings auf gitlab nicht ausführen. Neben dem Dateinamen steht unter last commit, welche wichtigen Änderungen bei der letzten Änderung erfolgt sind. Unter last update steht, wann die Datei zuletzt verändert wurde. 

# ### Einfaches Herunterladen ohne Versionierung

# Sie können alle Dateien herunterladen, indem Sie auf das Download-Symbol klicken. Dann speichern Sie diese Dateien in einem Ordner auf Ihrem Rechner und können sie dort weiternutzen. 

# ### Herunterladen mit Versionierung

# gitlab gibt folgende Anleitung für Windows, Mac, Linux an, um die Versionierung mit git zu nutzen: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html Man braucht das nur einmalig durchführen.

# Die weiteren Schritte habe ich nur unter Linux ausprobiert, bitte lesen Sie ggf. oben unter dem Link nach, wie es unter Windows geht.

# **Nach** den Schritten aus der Anleitung kann ich mein Verzeichnis beim ersten Mal auf meinem Rechner mit den Dateien aus gitlab befüllen: 
# <p style="background:black">
# <code style="background:black;color:white">git clone https://gitlab.com/jfmay/systemtechnik-fuer-energieeffizienz.git
# </code>
# </p> 

# **Zum Aktualisieren** kann ich folgenden Befehl verwenden: 
# <p style="background:black">
# <code style="background:black;color:white">git pull origin master
# </code>
# </p> 

# **Den Status abfragen** kann ich mit folgendem Befehl: 
# <p style="background:black">
# <code style="background:black;color:white">git status
# </code>
# </p> 

# **Dann kann ich die Dateien, die geändert sind zur Synchronisierung vormerken**: 
# <p style="background:black">
# <code style="background:black;color:white">git add dateiname.ipynb
# </code>
# </p> 

# **Wenn ich alle Dateien vormerken will, dann geht das mit**: 
# <p style="background:black">
# <code style="background:black;color:white">git add .
# </code>
# </p> 

# **Nun schreibe ich noch eine Nachricht für die Änderung dazu**: 
# <p style="background:black">
# <code style="background:black;color:white">git commit -m "Codezeile für 2+2 ergänzt"
# </code>
# </p> 

# **Zum Ändern in __meinem Repository__ (fremde kann man so nicht synchronisieren, dafür braucht man eine Erlaubnis des Inhabers) synchronisiere ich dann mit**: 
# <p style="background:black">
# <code style="background:black;color:white">git push
# </code>
# </p> 

# Es gibt noch viele weitere Befehle. Unter anderem kann man praktisch jeden beliebigen früheren Status wieder herstellen (wenn man immer git verwendet hat bei allen Änderungen, sonst nicht).

# ### Projektdokumentation in gitlab?

# Sie können - wenn Sie wollen - selbst einen Account bei gitlab anlegen und diesen für die Erstellung Ihres jupyter notebooks nutzen. 

# Wenn Ihre Kollegen im Projekt nun eine Änderung machen und hochladen, dann erscheint automatisch eine Ansicht in gitlab, in der man die Änderungen deutlich markiert sehen kann. Damit kann man dann entscheiden, ob man diese Änderungen, ggf. teilweise, übernimmt. Und auch hier ist es, wie oben, möglich, zu jedem beliebigen Stand zurückzugehen. Sie können also testen, ob der Vorschlag Ihres Kollegen funktioniert und - wenn nicht - entscheiden, ob Sie einfach zur vorigen (funktionalen) Version zurückgehen.

# Weitere Infos zur Kollaboration mit gitlab gibt es in Tutorials und Anleitungen, die Sie im Internet finden.

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Datenschutz:</b></div>
# Gitlab (und die meisten alternativen Plattformen wie github) sind externe Services, die nicht von der Hochschule bereitgestellt werden. Bitte lesen Sie daher die Nutzungsbedingungen und Datenschutzinformationen sorgfältig durch. Wenn Sie damit nicht einverstanden sind, nutzen Sie die entsprechende Plattform nicht. Ich empfehle zudem, möglichst wenig personenbezogene Daten dort zu hinterlassen (keine Matrikelnummer angeben!). Die Projektabgabe des fertigen Jupyter Notebooks erfolgt dann in ilias im geschützten Bereich.
# </div>

# ### Projektdokumentation in sciebo?

# Sciebo hat den Vorteil, dass die Daten in Deutschland liegen. Allerdings kann man auf sciebo nicht eine Versionierung durchführen, die mehrere Leute nutzen können wie in git-Systemen. Eine Versionierung müssen Sie manuell durchführen, z. B. indem Sie die Dateinamen mit Versionen benennen:
# 
# projektA-v17.ipynb
# 
# So könnte die 17. Version des Jupyter Notebooks von Projekt A heißen. Damit man zu den früheren Versionen zurückspringen kann, ist es sinnvoll, diese ebenfalls aufzubewahren, z. B. in einem Ordner mit dem Namen "Archiv". 

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Vorsicht beim automatischen Synchronisieren mit sciebo:</b></div>
# Wenn Sie die sciebo Desktop App nutzen, werden Dateien automatisch synchronisiert. Das bedeutet, dass wenn Sie eine Datei auf Ihrem Rechner löschen, diese auch auf sciebo gelöscht wird. Wenn das ein Versehen war, gibt es keine Möglichkeit, die Datei wiederherzustellen. Die Datei ist dann für immer verschwunden und muss bei Bedarf neu erstellt werden. Erstellen Sie daher am besten häufig neue Versionen, so dass es nicht so schlimm ist, wenn Sie zur älteren Version zurück müssen. Außerdem empfiehlt sich ein Backup des Ordners an einer anderen Stelle.
# </div>

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Vorsicht beim automatischen Synchronisieren auch bei dropbox und Co.</b></div>
# Wie auch bei sciebo können beim automatischen Synchronisieren Daten verloren gehen. Außerdem müssen Sie beachten, dass die Daten auf einem Server in einem fremden Land liegen, lesen Sie also ggf. die Nutzungsbedingungen und Datenschutzbedingungen genau durch. Wenn Sie mit diesen nicht einverstanden sind, nutzen Sie die Plattform nicht.
# </div>

# ### Dateigröße von Jupyter Notebooks minimieren

# Ausgaben entfernen mit Cell $\to$ All Output $\to$ Clear

# ### Vor dem Teilen: Kernel neu starten und Notebook überprüfen

# Nach einer Weile haben Sie mal in der einen Zelle, mal in der anderen was geändert ... 

# Und hinten und vorne sind Widersprüche entstanden ...

# Kernel $\to$ Restart bzw. Kernel $\to$ Restart & Run All

# ## Berichte erstellen aus Jupyter Notebooks

# Hier werden wir nur die einfachsten Wege kennenlernen. Man kann auch Add-Ons (Extensions) verwenden, die noch viele weitere Features beinhalten, inklusive automatischem Abbildungsverzeichnis, Grafiken etc.pp. Aber das würde für dieses Modul hier zu weit führen.

# Sie exportieren direkt ein pdf im Menü File $\to$ Download as PDF via LaTeX. Allerdings fehlen dann die Grafiken (und es funktioniert auch nur, wenn auf Ihrem Rechner LaTeX installiert ist). Daher empfehle ich Ihnen File $\to$ Print Preview in eine Datei zu drucken.

# Um daraus ein Word-Dokument zu machen (falls Sie das brauchen, weil der Bericht nicht als pdf, sondern als Word abgegeben werden soll), empfehle ich [pandoc](https://pandoc.org/). Allerdings sind dann natürlich alle interaktiven Fähigkeiten von jupyter futsch. Würde ich also erst machen, wenn man mit dem Code und dessen Ergebnissen zufrieden ist.

# ## Projektpräsentation mit jupyter

# Sie können, müssen aber nicht, Ihr Projekt in jupyter präsentieren. Dazu ist auf dem jupyterhub die extension RISE installiert, die Sie mit dem Diagramm-Symbol oben in der Menüleiste erreichen.

# ## Wichtige python-Befehle

# hilfreiche Tutorials gibt es viele, z. B. https://www.python-kurs.eu/index.php {cite:p}``bernd_klein_python-kurs_2009``- gibt's auch als Buch, und vom selben Autor nutze ich selbst sehr gerne {cite:p}``klein_numerisches_2019``
# <br><br>
# wichtig ist, dass Sie Python 3 aufwärts verwenden
# <br><br>
# es lohnt sich, ein Anfänger-Tutorial durchzuarbeiten, wenn man noch nie mit python gearbeitet hat
# <br><br>
# wenn Sie schon Vorkenntnisse in anderen Programmiersprachen oder in MATLAB / scilab haben, wir es schneller gehen, ansonsten dauert es etwas länger

# ### Taschenrechner in python

# Berechnen Sie $4,567\cdot8,2389$

# In[11]:


x = 4.567*8.2389
print(x)


# Berechnen Sie $(1+43)\cdot3$

# In[12]:


x=(1+43)*3
print(x)


# ### Hilfe im Netz

# Aufgrund der Vielzahl an python-Seiten sei hier nur ein paar wichtige Seiten verwiesen, die bei den meisten Problemen weiterhelfen:
# - Die Dokumentation von python, in der Sie auch für vieles Beispiele finden: [offizielle python Doku](https://docs.python.org)
# - Ein Forum für Programmierer aller möglichen Tools, in der sehr viele Beispiele zu finden sind: [Stackoverflow](https://www.stackoverflow.com)

# Wichtig ist, vorsichtig zu sein, mit Codeschnipseln, die Sie aus dem Internet kopieren: Nicht alle Beispiele funktionieren, manche sind veraltet oder funktionieren nur mit einer anderen Systemkonfiguration. Im Extremfall wird Ihr Rechner instabil laufen und Sie müssen die Ausführung abbrechen. Dafür ist es notwendig, dass Sie vorher erstellte Stände Ihrer Software gespeichert haben. (Also: wirklich häufig speichern hilft schon sehr viel!) So können Sie, wenn das mit dem neuen Codeschnipsel nicht funktioniert,
# einen alten Stand wiederherstellen und damit weiterarbeiten.

# ### Strukturieren durch Einrückungen (*Indent*)

# Benötigt man in einem Block von Code einen weiteren Block von Code, so strukturiert man durch Einrückungen

# `  Anweisungskopf:
#       Anweisung
#       .
#       Anweisung` 

# - Doppelpunkt am Ende des Anweisungskopfes (*header*)
# - gleichmäßige Einrückung der zugehörigen Anweisungen)

# Der Editor im jupyter Notebook hilft dabei.

# ### Variablen in python

# benötigen erst mal keinen Typ

# In[13]:


x = 42


# Definition von `x`

# Gleichheitszeichen: der Variablen `x` wird der Wert 42 zugewiesen

# ausgeben mit

# In[14]:


print("Wert von x: ", x)


# ### Variablen weiterverwenden mit

# In[15]:


y = x**2
print(y)


# oder auch auf beiden Seiten der Zuweisung verwenden, um z. B. um 1 zu erhöhen

# In[16]:


x=x+1
print(x)


# alternative Schreibweise

# In[17]:


x+=1
print(x)


# letzteres bei Listen besser nicht verwenden (Laufzeitprobleme!)

# ### Variablentypen

# Typ implizit auf `int` (Integer = Ganzzahl) ändern:

# In[18]:


i = 14
print(i,type(i))


# implizit auf `float` (Gleitkommazahl) ändern:

# In[19]:


i = 19+0.15
print(i,type(i))


# implizit auf `string` (Zeichenkette) setzen)

# In[20]:


i = "vierunddreissig"
print(i,type(i))


# ### Identitätsfunktion `id()`

# innerhalb eines Programmes eindeutig

# Identität einer Instanz überprüfen

# dient dazu, zu sehen, ob sie sich von allen anderen Instanzen unterscheidet

# In[21]:


a = 23
id(a)


# In[22]:


b = a
id(a), id(b)


# In[23]:


b = 34
id(a), id(b)


# im zweiten Beispiel: Identität von `b` geändert, nicht aber von `a`

# ### Gültige Variablennamen

# - Großbuchstaben A bis Z
# - Kleinbuchstaben a bis z
# - Unterstrich `_`
# - Zahlen 0 bis 9 (nicht an erster Stelle)
# - keine Schlüsselwörter als Variablen, d.h. *nicht* `and`, `as`, `assert`, `break`, `class`, `continue`, `def`, `del`, `elif`, `else`, `except`, `False`, `finally`, `for`, `from`, `global`, `if`, `import`, `in`, `is`, `lambda`, `None`, `nonlocal`, `not`, `or`, `pass`, `raise`, `return`, `True`, `try`, `while`, `with`, `yield`

# In[24]:


temperature = 17
max_temperature = 44
MinTemperature = -20


# ### Division mit `/`

# In[25]:


d = 10
e = 3
f = d/e
print(f)


# In[26]:


type(f)


# Division mit Abschneiden mit `//`

# In[27]:


g = d//e
print(g, type(g))


# ### Zeichen in Strings einzeln ansprechen

# In[28]:


s = "Energie"
print(s[0])


# In[29]:


print(s[2])


# ### Länge eines Strings und letztes Zeichen

# In[30]:


s = "Energie"
len(s)


# In[31]:


index_last_char=len(s)-1
print(s[index_last_char])


# oder der vorletzte Buchstabe

# In[32]:


print(s[index_last_char -1])


# ### von hinten zählen

# geht elegant mit dem Minus

# In[33]:


s = "Energie"
last_character=s[-1]
print(last_character)


# Probieren Sie auch `s[-2]` und so weiter aus

# ### mit Strings arbeiten

# Zusammenfügen (`concatenate`)

# In[34]:


print("Hello"+"World")


# Wiederholt zusammenfügen mit dem `*`-Operator

# In[35]:


print("." *5)


# Indizieren

# In[36]:


print("Energie"[0])


# Scheiben herausschneiden

# In[37]:


print("Energie"[2:4])


# Länge bestimmen

# In[38]:


print(len("Energie"))


# indizierte Positionen lassen sich in python *nicht* verändern

# ### Listen ansprechen: wie Strings

# In[39]:


farben = ['rot', 'blau', 'gelb']
print(farben[0])


# In[40]:


b = ["Martha", 22, 2.79, "New York"]
print(b[3])


# ### Wenn-Dann

# In[41]:


def standby(daten):
    """Prüfen, ob ein Wert ein Standby-Verbrauch ist"""
    if daten < 1:
        sb = daten
    elif daten <10:
            sb=0
    return sb


# ### Schleifen

# In[42]:


i = 1
while i <= 10:
    print(i)
    i+=1


# In[43]:


range(10)


# In[44]:


list(range(10))


# In[45]:


list(range(4,10))


# In[46]:


list(range(4,50,5))


# ### Funktionen

# `def funktionsname(parameterliste):
#    anweisung(en)`

# In[47]:


def fahrenheit(T_in_celsius):
    """ gibt die Temperatur in Fahrenheit zurück"""
    return (T_in_celsius *9/5)+32


# In[48]:


for t in (21.3,26.4,29,8,31.7):
    print(t,": ", fahrenheit(t))


# ### Bibliotheken verwenden

# In[49]:


import numpy
print(numpy.pi)


# häufige Abkürzung `np`

# In[50]:


import numpy as np
print(np.pi)


# <div class="admonition important" style="background: #e9f6ec; padding: 10px">
# <div class="title"><b>AUFGABE</b></div>
# Welche Funktion wollen Sie als nächstes in python kennenlernen?
# </div>

# *Antwort:...*

# ## Literatur

# ```{bibliography}
# :filter: docname in docnames
# ```
