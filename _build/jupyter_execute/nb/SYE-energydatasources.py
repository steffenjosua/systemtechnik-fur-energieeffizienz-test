#!/usr/bin/env python
# coding: utf-8

# (energydatasources=)
# # Öffentlich verfügbare Energiedaten

# - [ ] TODO Zitierweise, Danksagung Förderung einfügen

# ## python konfigurieren

# ### Module importieren

# In[1]:


import numpy as np
import matplotlib
#matplotlib.use('qt5agg')
import matplotlib.pyplot as plt
from matplotlib import rcParams
import pandas as pd
import datetime as dt
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import os.path
import platform
from PIL import Image
import requests
import scipy
from scipy.optimize import curve_fit

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('platform', platform.__version__)
print('Image', Image.__version__)
print('requests', requests.__version__)
print('scipy', scipy.__version__)


# ### Grafikparameter einstellen

# In[2]:


#plt.rcParams['savefig.dpi'] = 75
#plt.rcParams['figure.autolayout'] = False
#plt.rcParams['figure.figsize'] = 10, 6
#plt.rcParams['axes.labelsize'] = 18
#plt.rcParams['axes.titlesize'] = 20
#plt.rcParams['font.size'] = 18
#plt.rcParams['lines.linewidth'] = 2.0
#plt.rcParams['lines.markersize'] = 8
#plt.rcParams['legend.fontsize'] = 18
#plt.rcParams['text.usetex'] = True
#plt.rcParams['text.latex.preamble'] = "\\usepackage{subdepth}, \\usepackage{type1cm}"
#pd.set_option('display.latex.repr', True)
#pd.set_option('display.latex.longtable', True)


# - [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

# ### Funktionen definieren

# ## Lernziele

# <div class="admonition note" style="background: #e5f1ff; padding: 10px">
# <div class="title"><b>Lernziele für diesen Abschnitt:</b></div>
# Das sollen Sie lernen:
#     <ul>
#         <li> öffentlich verfügbare Sammlungen von Energiedaten kennen </li>
#         <li> mit Datenquellen aus dem Internet arbeiten </li>
#         <li> um Energieeffizienzanalysen realistischer zu machen </li>
#     </ul>
# </div>

# ## Sammlungen mit Energiedaten

# - **[openmod](https://wiki.openmod-initiative.org/wiki/Data)** offene Energiedaten aus verschiedenen Quellen, zusammen mit Links zu Auswertescripts
# - **[open energy systems database](https://en.wikipedia.org/wiki/Open_energy_system_databases)** gesammelte, bereinigte Energiedaten mit offener Lizenz
# - **[Klimadaten des IPCC](https://tntcat.iiasa.ac.at/AR5DB/dsd?Action=htmlpage&page=welcome)** Daten, die das IPCC nutzt, um Szenarien über den Klimawandel zu entwickeln, hierzu gibt es auch jupyter notebooks für die Analyse https://data.ene.iiasa.ac.at/sr15_scenario_analysis/

# ### Energiedaten-Zeitreihen, teilweise modelliert, teilweise gemessen

# | **Name** | **Beschreibung** | **Auflösung** | **Link** | **Quelle** | 
# |-|-|-|-|-|
# | **PLEXOS-World** | 2015 Stromdaten weltweit | 1 h | https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/CBYXBY | {cite:p}`brinkerink_building_2021` |
# | **OPSD Timeseries** | 32 europ. Länder ENTSOE-Daten Last, Wind, Solar | 1 h | https://data.open-power-system-data.org/time_series/2020-10-06 | {cite:p}`muehlenpfordt_time_2020` |
# | **OPSD Weather Data** | Strahlung und Temperatur in Europa | 1 h | https://data.open-power-system-data.org/weather_data/2020-09-16 | {cite:p}`pfenninger_weather_2020` |
# | **OPSD Household Data** | Haushalte mit Photovoltaik, Handwerksbetrieb, Forschungsinstitut | 1 min. | https://data.open-power-system-data.org/household_data/2020-04-15 | {cite:p}`minde_household_2017` |
# | **OPSD When2Heat** | simulierte Wärmepumpenbedarfe | 1 h | https://data.open-power-system-data.org/when2heat/2019-08-06 | {cite:p}`ruhnau_when2heat_2019` |
# | **Region4FLEX power2heat** | Wärmebedarf und power2heat-Potenziale (simuliert) | 15 min. | https://wiki.openmod-initiative.org/wiki/Region4FLEX | {cite:p}`heitkoetter_regionalised_2019` |
# | **Region4FLEX dsm** | Lastverschiebepotenziale | 15 min. | https://wiki.openmod-initiative.org/wiki/Region4FLEX | {cite:p}`heitkoetter_assessment_2021` |
# | **EU data portal** | ? | ? | https://data.europa.eu | ? |
# | **SMARD** | deutsche Stromerzeugung nach Erzeugungsart | 15 min. | https://www.smard.de/home | {cite:p}`bundesnetzagentur_smard_2020` |
# | **Typtage** | 9 Typtage elektrische Referenzlastprofile für die Quartiersebene, für verschiedene Gebäudetypen (Schulen, Wohnhäuser, etc.) erzeugen | | https://siz-energie-plus.de/smart-grid-toolkit|  {cite:p}`kit_siz_2020`|
# | **DWD 2020** | open data des Deutschen Wetterdienstes | | https://www.dwd.de/DE/klimaumwelt/cdc/cdc_node.html | {cite:p}`deutscher_wetterdienst_dwd_wetter_2020`  |
# |**OEP** | Open Energy Platform OEP | | https://openenergy-platform.org/ | {cite:p}`open_energy_platform_oep_2021` |
# |**FFE** | opendata der Forschungsstelle für Energiewirtschaft | | https://opendata.ffe.de | |
# | **JERICHO-E** | Energieverbrauchsprofile für Heizung, Kühlung, mechanische Energie, ITK, Beleuchtung in räumlicher und zeitlicher Auflösung, differenziert nach Haushalten, Industrie, Gewerbe und Mobilität | | https://jericho-energy.de/e-usage | {cite:p}`priesmann_time_2021` | 

# ### Gemessene Verbrauchsdaten von Elektrogeräten

# | **Name** | **Beschreibung** | **Auflösung** | **Link** | **Quelle** |
# |-|-|-|-|-|
# | [BLOND](https://www.nature.com/articles/sdata201848) | elektrische Lastprofile aus einer Büroumgebung mit ca. 16 Gerätetypen | 4 µs - 156 µs | https://mediatum.ub.tum.de/1375836 | {cite:p}`kriechbaumer_blond_2017` |
# | [REFIT](https://pureportal.strath.ac.uk/en/datasets/refit-electrical-load-measurements) | 20 Haushalte in UK inkl. Gerätelastprofile | 8 s | https://pureportal.strath.ac.uk/en/datasets/refit-electrical-load-measurements | {cite:p}`murray_refit_2015` |
# | [AMPds2](https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/FIE0S4) | 1 kanadischer Haushalt Strom, Gas und Wasser, aufgelöst nach Räumen und Geräten (insgesamt 21 Leistungsmessgeräte, 2 Wasseruhren und 2 Gasmengenzähler) plus Wetterdaten | hoch | https://dataverse.harvard.edu/dataset.xhtml?persistentId=doi:10.7910/DVN/FIE0S4 | {cite:p}`makonin_ampds2_2016` |
# | [PLAID](https://www.nature.com/articles/s41597-020-0389-7) | Haushaltsgeräte, allerdings keine IT | ?| https://www.nature.com/articles/s41597-020-0389-7 | {cite:p}`medico_voltage_2020` |
# | [UK-DALE](http://europepmc.org/article/PMC/4432654) | britische Haushaltsgeräte, darunter auch ein PC | 16 kHz | https://data.ukedc.rl.ac.uk/browse/edc/efficiency/residential/EnergyConsumption/Domestic/UK-DALE-2015 | |
# | [Tracebase](https://github.com/areinhardt/tracebase) | deutsche und australische Geräte | ? | https://github.com/areinhardt/tracebase | {cite:p}`andreas_reinhardt_accuracy_2012`|
# | [WHITED](https://www.semanticscholar.org/paper/WHITED-A-Worldwide-Household-and-Industry-Transient-Kahl-Haq/766b29cd47987be0e7b2bbb9fcc9df351d5041cc) | Haushaltsgeräte | 44 kHz | https://www.semanticscholar.org/paper/WHITED-A-Worldwide-Household-and-Industry-Transient-Kahl-Haq/766b29cd47987be0e7b2bbb9fcc9df351d5041cc | {cite:p}`m_kahl_whited-worldwide_2016`|
# | [COOLL](https://arxiv.org/pdf/1611.05803.pdf) | französische Haushaltsgeräte inkl. Router| 100 kHz | https://arxiv.org/pdf/1611.05803.pdf | {cite:p}`picon_cooll_2016`|

# ## Photovoltaik-Erzeugungsdaten und solare Wetterdaten

# - [PVGIS](https://ec.europa.eu/jrc/en/pvgis) beinhaltet photovoltaische Daten für den europäischen Raum

# ## Literatur

# ```{bibliography}
# :filter: docname in docnames
# ```
