#!/usr/bin/env python
# coding: utf-8

# # Energietechnik modellieren mit python

# ## Energiemodelle mit python

# ```{warning}
# Installation auf eigene Verantwortung
# Die python-Module hier sind nicht von mir geprüft, schon gar nicht für alle möglichen Betriebssystemversionen und python-Konfigurationen. Entscheiden Sie selbst, ob Ihnen die Installation auf Ihrem System sicher erscheint. 
# ```

# | **Modul** | **Beschreibung** | 
# |-|-|
# | **[PVLIB](https://github.com/pvlib/pvlib-python)** | photovoltaische Energieerzeugung modellieren {cite:p}`f_holmgren_pvlib_2018-1` |
# | **[windlib](https://pypi.org/project/windlib/)** | Winderzeugung modellieren |
# | **[SimSES](https://www.ei.tum.de/ees/fp-ees/simses/)** | Photovoltaik-Heimspeicher und Alterung modellieren {cite:p}`naumann_simses_2017-1` |
# | **[BioSTEAM](https://biosteam.readthedocs.io/en/latest/?ref=neuenergies.com)** | Bioraffinerie-Simulation mit techno-ökonomischer Analyse |
# | **[Calliope](https://www.callio.pe/)** | Energiesysteme modellieren {cite:p}`pfenninger_calliope_2018` |
# | **[pypsa](https://pypsa.org/)** | Energiesystem-Netzwerke modellieren {cite:p}`brown_pypsa_2018` |
# | **[pandapower](http://www.pandapower.org/)** | elektrische Netze modellieren {cite:p}`thurner_pandapower_2018` |
# | **[TESPy](https://github.com/oemof/tespy)** | Thermische Energie quasistatisch {cite:p}`witte_tespy_2020` |
# | **[pyam](https://pyam-iamc.readthedocs.io/en/stable/)** | Klimamodelle des IPCC in Szenarien {cite:p}`gidden_pyam_2019` |
# | **[emobpy](https://pypi.org/project/emobpy/)** | Lastprofilgenerator für die Elektromobilität {cite:p}`gaete-morales_open_2021` |
# | **[nilmtk](https://nilmtk.github.io/)** | non intrusive load monitoring toolkit - Energiedaten analysieren {cite:p}`batra_nilmtk_2014` |

# ## Apps für Energiedatenanalyse

# ```{warning}
# Installation auf eigene Verantwortung
# Die python-Module hier sind nicht von mir geprüft, schon gar nicht für alle möglichen Betriebssystemversionen und python-Konfigurationen. Entscheiden Sie selbst, ob Ihnen die Installation auf Ihrem System sicher erscheint. 
# ```

# Der Energiecheck als App {cite:p}`energiesparkonto_energiesparen_2015` kann Zählerdaten wöchentlich oder monatlich einlesen und somit ein Haushaltslastprofil in sehr grober Auflösung erstellen. Außerdem versprechen die Autoren Infos zu Förderprogrammen für Energieeffizienz.

# ## Literatur

# ```{bibliography}
# :filter: docname in docnames
# ```
