#!/usr/bin/env python
# coding: utf-8

# # Mit Daten kritisch umgehen

# Dieses Kapitel beinhaltet Techniken zur Plausibilisierung sowohl von Zeitreihen als auch von Einzeldaten. Dies ist wichtig, denn "wer misst, misst (auch) Mist" und daher sind sowohl eigene Messungen als auch fremde Daten immer möglicherweise fehlerbehaftet und manchmal auch aus Marketing-Gründen oder anderen Interessen heraus "geschönt". Es ist daher notwendig "fake data" von "real data" gut zu unterscheiden.
