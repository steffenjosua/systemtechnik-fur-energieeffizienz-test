#!/usr/bin/env python
# coding: utf-8

# # Praktikum elektrische Energie messen

# ## python konfigurieren

# ### Module importieren

# In[1]:


import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable


print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)


# ### Grafikparameter einstellen

# In[2]:


plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)


# ### Funktionen definieren

# ## Lernziele

# <div class="admonition note" style="background: #e5f1ff; padding: 10px">
# <div class="title"><b>LERNZIELE</b></div>
#     <ul>
#         <li> Durchführungshinweise für das Praktikum kennen </li>
#         <li> mit Information über Voraussetzungen </li>
#         <li> um erfolgreich im Praktikum zu messen </li>
#     </ul>
# </div>

# ## Vorbereiten

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Arbeiten Sie das Kapitel 4 (alle Abschnitte) durch, damit Sie das, was Sie dann sehen werden schon im Jupyter Notebook "erfahren" können.
# </div>

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Wichtig: Anleitung vor dem Praktikum durcharbeiten und Versuchsplan im Projektteam abstimmen!</b></div>
# Da mehrere Projektteams an einem Versuchstermin teilnehmen, müssen wir die Zeit effizient nutzen. Im Folgenden finden Sie Felder, in denen Sie Ihre Vorbereitung eintragen können. Wenn die Versuchszeit endet und sie noch nicht alles gemessen haben, was Sie messen wollten, dann kann ich keine Verlängerung anbieten. D.h. wer unvorbereitet ist, und dann nicht alles schafft, hat Pech gehabt.
# </div>

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Notwendige Software bzw. Hardware:</b></div>
# 
# - python und jupyter notebook für Notizen mit Auswertung
# - USB-Stick, um Messdaten zu sichern - <b>sichern Sie vorher Ihre wichtigen Daten an anderer Stelle, denn es kann nicht garantiert werden, dass Daten auf dem USB-Stick beim Speichern in den Messgeräten verloren gehen </b>
# - Rohde und Schwarz Software für Leistungsanalysator und Multimeter (s.u.)
# </div>

# Das sollen Sie vorbereitet haben:
# - Diese Anleitung sowie alle Notebooks im Kapitel 4 durcharbeiten
# - Auftauchende Fragen recherchieren und notieren
# - Ideen für projektrelevante Messungen im Projektteam überlegen und im Versuchsplan dokumentieren
# - Leeren USB-Stick (es kann nicht garantiert werden, dass Daten darauf nicht verloren gehen!) und Schreibzeug sowie Taschenrechner mitbringen, möglichst auch eigenen Laptop
# - vorzubereitende Software (s.o.) auf Ihrem Gerät installieren
# - Vorgesehene Reihenfolge der Versuchsdurchführung einhalten, um Fehlmessungen und Fehlinterpretationen zu vermeiden

# Im Video LINK EINFÜGEN finden Sie die Vorgehensweise in diesem Praktikumsversuch erklärt.

# ## Sicherheitshinweise

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Wo Sie die Sicherheitshinweise finden</b></div>
# Beachten Sie die Hinweise im Kapitel 1 unter "Labor" und notieren Sie sich etwaige Fragen.
# </div>

# ## Dokumentation

# Die zugehörige Theorie kann in der mündlichen Prüfung abgefragt werden. Daher ist es sinnvoll, dass Sie das zum Versuch gehörige Jupyter Notebook mit Notizen und Messergebnissen aufbewahren bis zur mündlichen Prüfung. Bitte dokumentieren Sie so nachvollziehbar, dass die gesamte Projektgruppe bis zur Prüfung die Dokumentation nutzen kann.

# **Dateinamenskonvention**: `JJJJ-MM-TT_TeamX_SYE.ipynb` oder `JJJJ-MM-TT_TeamX_SYE_Messung_Leistung_Geraet_Hersteller_Modell.csv`, also z. B. `2005-05-08_TeamB_SYE_Kuehlschrank_Liebherr_Modellx3.csv`

# Diese Dateinamenskonvention ist angelehnt an die ISO-Norm. Dadurch werden bei alphabetischer Sortierung alle Dateien in der Reihenfolge ihres Datums angezeigt, was ziemlich praktisch ist, wenn man noch erinnert, dass das eine Messung war, die man am Tag x im Monat y gemacht hatte. Außerdem kann man so mit dem Datum für das Jupyter Notebook eine Art Versionierung festlegen.

# Im Jupyter Notebook verlinken Sie beim Einlesen und Auswerten alle Messdateien, die Sie im gleichen Verzeichnis wie das Notebook im Ordner `data` abgespeichert haben. Außerdem notieren Sie sich dort alle Metadaten, die das Messgerät nicht wissen kann. Z. B. notieren Sie sich, dass Sie einen Kühlschrank der Marke x und der Modellnummer xy gemessen haben, als er mit einem (Bild vom Innenleben hilft) bestimmten Inhalt gefüllt war. Zudem schreiben Sie alles weitere auf, was da noch relevant sein könnte (z. B. ob das Bier, das da im Kühlschrank zu sehen ist, gerade erst eingeräumt wurde, d.h. direkt vor der Messung oder schon eine Woche vorher). Diese Informationen nennen sich Metadaten. Im Jupyter Notebook lassen sie sich in komfortabel lesbarer Form in markdown-Zellen ergänzen. Diese Informationen müssen nicht unbedingt in Fließtext notiert werden. Manchmal eignen sich auch Tabellen-Ansichten oder Stichpunkte-Listen besser, weil sie übersichtlicher sein können.

# Nach den Messdaten-Infos kommen die Code-Blöcke mit der Auswertung und die zugehörigen Diagramme. Im Markdown-Feld darunter beschreiben Sie dann, was man sehen kann und was überraschend ist, was ggf. unplausibel erscheint und welche Erkenntnisse Sie aus der Messung und Auswertung ziehen.

# ## Versuchsplan

# Dokumentieren Sie hier, welche Messungen Sie im Projektteam planen. Bitte lesen Sie dafür in der Versuchsanleitung nach, welche Messgeräte zur Verfügung stehen und welche Prüflinge da sind. Wenn das Projektteam nicht geschlossen an einem Praktikumstermin teilnimmt, können Sie dieses Dokument natürlich sukzessive erweitern. **Wichtig ist, dass Sie VOR dem Praktikumstermin einen Plan haben.** Der Versuchsplan hier ist mit einem Beispiel befüllt. Sinnvoll ist es, wenn Sie hier alle für Ihr Projekt interessanten Messungen eintragen und zwar in der Reihenfolge ihrer Priorität, d.h. ganz oben die wichtigsten Messungen.

# | **Ziel der Messung** | **Messgerät(e)** | **Prüfling** | **Nutzungsart** | **Messparameter** | **Kommentar** |
# |-|-|-|-|-|-|
# | Lastprofil Wasserkochen von vorher erwärmtem Wasser | Leistungsanalysator HMC8015, Pt100-Temperatursensor, Energielogger | Wasserkocher Huawei xyz | 800 ml Wasser ($T_{vorher} = 53°C$) zum Kochen bringen | alle 2 ms eine Messung von Spannung und Strom |2017-04-01_TeamX_SYE_Wasserkocher800mlvorher53C.csv |
# |*Ihr Ziel* | *nötige Messgeräte* | *nötige Geräte* | *Art der Nutzung und Randbedingungen, die für Energieverbrauch wichtig sind* | *Einstellungen Messgerät(e)* | *Dateiname*, *Kommentare*|

# ## Professionelles Leistungsmessgerät für elektrische Verbraucher

# <div class="admonition seealso" style="background: #e9f6ec; padding: 10px">
# <div class="title"><b>Weiterführende Literatur:</b></div>
# 
# - [R&S®HMC8015 Power Analyzer Benutzerhandbuch, Ausgabe 2020](https://www.rohde-schwarz.com/de/handbuch/r-s-hmc8015-power-analyzer-benutzerhandbuch-handbuecher-gb1_78701-157004.html) {cite:p}``rohde__schwarz_gmbh__co_kg_rs_2015``
# - [HMExplorer Software Bedienhandbuch](https://www.rohde-schwarz.com/de/handbuch/hmexplorer-software-bedienhandbuch-handbuecher-gb1_78701-188611.html) {cite:p}``rohde__schwarz_gmbh__co_kg_hmexplorer_2020``
# </div>

# Im Versuch arbeiten wir mit einem Messgerät für einphasige elektrische Verbraucher. Es verfügt über eine Messsteckdose, die (bei Präsenz-Labors) auch von Nicht-Elektroingenieuren verwendet werden kann. In Industrieanwendungen ist es häufig auch so, dass man in den Unterverteilungen Messtechnik anbringt und alle drei Phasen misst. Dafür ist jedoch eine Ausbildung zur elektrotechnischen Fachkraft sowie eine zugehörige Beauftragung nötig. Dies ist im Praktikum nicht durchführbar. Jedoch lassen sich die gelernten Methoden auch übertragen.

# ![Leistungsanalysator im Praktikum](../img/leistungsanalysator.png)

# Das Leistungsmessgerät HMC8015 mit den Optionen HOC151, HOC152, HOC153 von Rohde und Schwarz steht im Versuch zur Verfügung. Es ist mit einer Messsteckdose verbunden (Adapter HZC815-DE). Einen Auszug aus den technischen Daten finden Sie in der folgenden Tabelle. Die Messgenauigkeiten aller Messbereiche und Messarten enthält das Handbuch (Link s.o.).

# | **Parameter** | **Wert** |
# |-|-|
# | Messmethode | gleichzeitiges Erfassen von Spannung und Strom |
# | Analoge Bandbreite | DC bis 100 kHz |
# | Frequenzgenauigkeit | 0,1% der Anzeige |
# | AD-Konverterauflösung | 16 bit (Spannung), 16 bit (Strom) |
# | Grundgenauigkeit | 0,05% der Anzeige |
# | Anzeigeauflösung | 5 Stellen |
# | Abtastfrequenz | 500 kHz |
# | Leistungsaufnahme | 35 W max. 15 W typ. |
# | Arbeitstemperatur | 5°C bis 40°C |
# | Aufwärmzeit | 60 Minuten |

# <div class="admonition warning" style="background: #fff9e5; padding: 10px">
# <div class="title"><b>Sicherheitshinweis!</b></div>
# Wenn Sie einen anderen Verbraucher als den am Steckdosenadapter angeschlossenen vermessen wollen, geben Sie bitte Bescheid. Das Umstecken erfolgt durch die Dozenten, denn aus versicherungstechnischen Gründen ist das Umstecken von Spannungen oberhalb der Schutzkleinspannungen eingewiesenen Fachkräften vorbehalten.
# </div>

# **Sehr kurze Kurzfassung der Bedienungsanleitung für das HMC8015**:
# - Bedienungsanleitung beachten!
# - Sichtprüfung der Kabel: wenn defekte Isolierung auffällt, kein Betrieb erlaubt!
# - Anschlussprüfung: sind die richtigen Kabel an den richtigen Stellen angeschlossen?
# - Netzschalter an der Geräterückseite auf 1
# - Spannungswählschalter an der Geräterückseite auf 230 V
# - Power Schalter an der Vorderseite einschalten
# - USB-Stick einstecken zur Datenspeicherung
# - Tasten `U`, `I` mit Pfeil nach oben und unten: Einstellung Strom-/Spannungsmessbereich
# - Taste `ACQ`: automatische Messbereichseinstellung, Einstellung Scheitelfaktor (Crest Factor) - wählen Sie beim Strom den Crest Factor 6, Einstellung Stromfehlermessung, Spannungsfehlermessung
# - Taste `VIEW`: Auswahl von numerischen Werten, Waveform (Oszilloskop-Ansicht), Frequenzanalyse oder Inrush
# - Taste `MEAS`: Loggen von Lastprofilen, Messung des Standby-Verbrauchs nach Normen, hier auch Auswahl des USB-Messverzeichnisses
# - Drehschalter mit Pfeiltasten: Auswahl von Messwerten (durch drehen und drücken)

# ## Verfügbare elektrische Verbraucher

# ### Leuchtmittel

# Zum Anschluss steht eine MAULstudy Tischleuchte mit E27 Fassung zur Verfügung. Drei verschiedene Leuchtmittel können eingesetzt werden, im Folgenden die Datenblattangaben aus den Verpackungen:

# |**Typ** | **Glühlampe** | **Halogen** | **Leuchtstoff** | **LED-Lampe** |
# |-|-|-|-|-|
# | **$P_N$** | 60 W | 57 W | - | 7 W |
# | **$U_N$** | 235 V | 230 V | - | - |
# | **$I_N$** | - | - | - | 55 mA |
# | **$P_{\text{äquivalent,Glühlampe}}$** | - | 70 W | - | 60 W |
# | **Lichtstrom** | 806 lm | 915 lm | - | - |
# | **Farbtemperatur** | - | 2800 K | - | 2700 K |
# | **Verbrauchsangabe** | 60 kWh/1000 h |  - | - | 7,0 kWh/1000 h|
# | **Lebensdauerangabe** | 5000 h | - | - | 2000 h, 2 Jahre, 50000 Schaltzyklen|
# | **Marke /Hersteller** | Barthelme | Osram | IKEA | SEGULA |
# | **Energieeffizienzklasse** | E | D | - | A++|
# | **Sonstiges** | klar | ohne Quecksilber |  - | matt |

# Beginnen Sie im Praktikum damit, die energieeffizienteste Lampe zu vermessen, da diese sich am wenigsten erhitzt und sich somit leichter tauschen lässt.

# ### Bildschirm (Monitor)

# Im Labor handelt es sich um einen Fujitsu B24-8TS-PRO. Der Verbrauch laut dem beiligenden australischen Energielabel energyrating.gov.au beträgt 85 kWh/Jahr. Auf der [Herstellerseite](https://www.fujitsu.com/de/products/computing/peripheral/displays/b24-8-ts-pro/) finden Sie weitere Label-Ratings.

# ### Computer

# Der Computer im Labor hat die folgenden Rahmendaten und den TH Köln Desktop mitsamt zugehöriger Software installiert.

# | **Parameter** | **Wert** |
# |-|-|
# | Hersteller | Dell |
# | Bezeichnung  | OptiPlex 3050 Minitower XCTO |
# | Prozessor | Intel Core i7-6700 (QC8 MB8, T3,4 GHz 65 W) der 6. Generation, unterstützt Windows und Linux |
# | Arbeitsspeicher | 4 GB DDR4-Speicher (1 x 4 GB, 2.400 MHz, kein ECC) |
# | Festplatte | SATA-SSD-Festplatte (Klasse 20), 2,5 Zoll, 256 GB |
# | Grafikkarte | integrierte Intel-Grafiklösung |
# | Gehäuse-Alarmschalter | ohne |
# | Festplattenkonfiguration | kein RAID |
# | Optische Laufwerke | optisches DVD-ROM-Laufwerk, 8x (9,5 mm) |
# | Wireless-Netzwerke | keine Wireless-Funktion | 
# | Kabel | Netzkabel-EU |
# | Tastatur | Dell KB216 Multimedia-Tastatur, schwarz - Deutsch (QWERTZ) |
# | Maus | Dell MS116 Maus, kabelgebunden, schwarz |
# | Gehäuseoptionen | Netzteil für OptiPlex 3050 MT (Bronze) |
# | Energy Star | ESTAR 6.1 und TCO 5.0-konformer Treiber, Serviceinstallationsmodul |
# | Weiteres Zubehör | erstes 8 GB RAM (Umbau von 1x 4 GB auf 1x 8 GB) - 1RX8-UDIMM mit 2.400 MHz |
# | Festplatte Software | ohne Intel Smart-Response-Technik |

# ### Weitere verfügbare Geräte

# Im Labor können Sie Ihre eigenen Geräte vermessen, falls diese elektrisch in Ordnung sind. Bitte melden Sie sich **zu Beginn** des Versuchstermins, um zu klären, ob das gewünschte Gerät vermessen werden kann.

# ### Messgerät kennenlernen mit Beispielmessungen

# Notieren Sie sich während der Versuchsdurchführung Stichpunkte zu Ihren Messungen (was wurde gemessen, was ist aufgefallen, welche Parameter wurden beobachtet oder eingestellt). Notieren Sie sich ebenfalls die Namen der Messdateien sowie die Metadaten zur Messung (z. B. welche Einstellung das Gerät bei der Messung hatte).

# **Messbereich einstellen am Leistungsanalysator**:
# - Falls Messwerte stark schwanken, können Sie die Anzeige des Leistungsanalysators einfrieren mit der Taste `HOLD`. Wenn Sie dann erneut `HOLD` drücken, zeigt die Anzeige wieder die aktuellen Messwerte an.
# - Stellen Sie den Messbereich so klein wie möglich ein: `VIEW`, Auswahl `Numeric`: alle Balken sollten grün sein ohne rote Bereiche.
# - Warum ist es besser den kleinstmöglichen Messbereich zu wählen? Was passiert, wenn der Bereich zu groß oder zu klein gewählt wird?

# **Messbereich so klein wie möglich und so groß wie nötig wählen**
# - Warum ist es besser den kleinstmöglichen Messbereich zu wählen? Was passiert, wenn der Bereich zu groß oder zu klein gewählt wird?

# *Antwort: Wenn man den richtigen Messbereich auswählt, dann ...*

# **Screenshots machen am Leistungsanalysator**: 
# Einige Anzeigen des HMC8015 lassen sich nur als Screenshots speichern:
# 1. USB-Stick einstecken
# 2. Taste `SAVE RECALL`
# 3. Screenshot `STORAGE Front USB`
# 4. `FILE NAME` hier max. 8 Zeichen eingeben, die Ihnen helfen, zu wissen, was Sie gemessen haben
# 5. `ACCEPT`
# 6. `COLOR & FORMAT BMP`
# 7. `SAVE RECALL` lang drücken um zu speichern

# **Signalform**:
# Speichern Sie einen Screenshot der Signalform ab:
# 
# 1. `VIEW`
# 2. `Waveform`
# 3. Messbereiche ggf. nachjustieren (so, dass Bildschirm genutzt wird, ohne dass Messbereiche zu klein werden)
# 4. Menü im Display mit `RETURN` wegklicken, so dass Menü nicht auf Screenshot
# 5. Screenshot siehe voriger Absatz

# **Oberwellen**:
# Speichern Sie einen Screenshot der Frequenzanteile ab:
# 1. `VIEW`
# 2. `Harmonics`
# 3. `SOURCE I` (Strom)
# 4. `SCALING` Prozent
# 5. `SUBSET` `Even Odd` beide gelb = ausgewählt
# 6. `NUMBER` 50

# *Die Screenshots zeigen Messungen von ... Verhalten des Verbrauchers. Sie sind gespeichert und können hier angezeigt werden mit `![was das zeigen soll](img/jjjj-mm-tt-dateiname-messung.bmp]`, wenn Sie im Ordner `img/` im Verzeichnis vom jupyter notebook liegen. Der Zusammenhang zwischen Oberwellenanteil und Signalform ist ... Der Parameter ... im Menü `VIEW` $\to$ `Numeric` in der Ansicht ... ist ein Maß für den Oberwellenanteil. Interessanter ist der Strom (?) oder die Spannung (?), weil ... Ergänzen Sie auch Ihre Kommentare, Erkenntnisse, etc. hier im Jupyter Notebook.*

# **Arbeitspunkte (Betriebszustände) unterscheiden:**
# Messen Sie für Ihr Gerät unterschiedliche Arbeitspunkte:
# - **Standby**: angesteckt und aus
# - **Ruhemodus**: an, keine Aktivität
# - **Aktivität**: an, notieren Sie die Aktivität (z. B. youtube Video xy), ggf. ist es sinnvoll Aktivität 1, 2, 3, etc. getrennt zu messen
# - **hohe Auslastung**: z. B. Leistungstest PC

# **Arbeitspunkte (Betriebszustände) in einer Tabelle vergleichen:**
# Erstellen Sie eine Tabelle und ergänzen Sie dort die gewonnenen Erkenntnisse:

# | **Messung** | **Gerät 1** | **Gerät 2** |
# |-|-|-|
# | **Signalform Standby** | ... | ... |
# | **$P$ [W]** | ... |... |
# | **$S$ [VA]** | ... | ... |
# | **$I_{THD}$ [%]** | ... | ... |
# | **$\lambda$** | ... | ... |
# | **Signalform Ruhemodus** | ... | ... |
# | **$P$ [W]** | ... |... |
# | **$S$ [VA]** | ... | ... |
# | **$I_{THD}$ [%]** | ... | ... |
# | **$\lambda$** | ... | ... |

# <div class="admonition important" style="background: #e9f6ec; padding: 10px">
# <div class="title"><b>AUFGABE</b></div>
# Welchen Zusammenhang können Sie zwischen dem Oberwellenanteil und der Signalform feststellen? Wie hängen die notierten Parameter zusammen?
# </div>

# *Antwort:...*

# **Zahlenwerte mit hoher Auflösung erfassen mit dem Leistungsanalysator:**
# Um die Signalformen nicht nur als Screenshot, sondern auch im Zahlenformat als csv-Datei speichern zu können, gehen Sie folgendermaßen vor: `VIEW`, `Inrush`, `SET Acquisition time`, `RETURN`, `ACT`, `SAVE`, Namen der .csv-Datei notieren!

# *Stellen Sie hier Ihre Beispielmessung dar und notieren Sie Ihre Beobachtungen und Erkenntnisse.*

# **Aufnahme von kurzen Lastprofilen:**
# Für längere Lastprofile im Bereich von mehreren Stunden reicht die Zeit im Praktikum nicht. Diese können Sie nur mit dem Energielogger aufnehmen. Jedoch kann im Praktikum durchaus ein kürzeres hochaufgelöstes Profil aufgenommen werden, das zeigt, wie unterschiedliche Betriebsmodi sich verhalten und wie der Übergang dazwischen wirkt. Um ein Lastprofil im HMC8015 aufzunehmen, gehen Sie wie folgt vor. Tragen Sie dann das Lastprofil in einem Diagramm auf und vergleichen Sie es mit einer Messung mit dem Energielogger. Notieren Sie danach auch im Jupyter Notebook, wie Abweichungen begründet werden könnten und wie gut die Profile übereinstimmen. Wie viel Energie benötigt der Energielogger in verschiedenen Lastpunkten zum Messen?

# **Am Leistungsanalysator:**
# - USB-Stick eingesteckt
# - Verbraucher an, auf höhere Leistung und Messbereiche Strom und Spannung einstellen (keine roten Balken)
# - `MEAS`, `Integrator ACTIVATE On`, `Reset`, `RETURN`
# - `Logging`, `Storage Front USB`, `FILE NAME` auswählen und eingeben, `Interval 0.1 s`, `MODE Unlimited`, `ACTIVATE Logging On`
# - Logging jeweils beenden mit `MEAS`, `Logging`, `Activate OFF`, es wird automatisch die Datei gespeichert

# In[3]:


# Hier muss Ihr eigener Dateiname stehen, damit Sie Ihre erste Messung einlesen können
#testmessung = pd.read_csv('../data/2020_11_14_164426.csv',sep=';',decimal=',')
#testmessung.head()


# In[4]:


#testmessung.plot(x='SystemTime', y='IRMS')


# # Literatur

# ```{bibliography}
# :filter: docname in docnames
# ```
