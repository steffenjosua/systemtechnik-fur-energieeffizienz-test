---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Klimaneutralität bewerten

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rcParams
import pandas as pd
import datetime as dt
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import os.path
import platform
from PIL import Image
import requests
import scipy
from scipy.optimize import curve_fit
from distutils.spawn import find_executable

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('platform', platform.__version__)
print('Image', Image.__version__)
print('requests', requests.__version__)
print('scipy', scipy.__version__)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
    #plt.rcParams['text.latex.preamble'] = "\\usepackage{subdepth}, \\usepackage{type1cm}"
```

- [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

+++

### Funktionen definieren

+++

## Lernziele

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
    <ul>
        <li> was </li>
        <li> womit </li>
        <li> wozu </li>
    </ul>
</div>

+++

## Motivationen

+++

- vorgeschrieben ...( wo?)

+++

- Kundenanforderung: tw. bekommt man den Auftrag nicht, wenn man das nicht nachweisen kann

+++

## Emissionen

+++

Scopes

+++

https://www.beuth.de/de/norm/din-en-iso-14064-1/291289049

+++

https://ghgprotocol.org/standards

+++

## Sichtweisen

+++

### Klimaneutralität auf Unternehmensebene

+++

- Unternehmensebene. PAS 2060 Standard und ISO 14068 Standard Klimaneutralität

+++

### Product Carbon Footprint (PCF)

+++

- [ ] TODO Kreisdiagramm zeichnen Raw Materials -> ... Landfill

+++

Greenhouse Gas Protocol The Product Life Cycle

+++

ISO-Norm

+++

Erste Unternehmen, wie z. B. [FESTO](https://www.festo.com) bieten zu Ihren Produkten Infos zum PCF an.

+++

**AUFGABE** Gehen Sie auf die Webseite von festo und recherchieren Sie den PCF eines Produkts. Vergleichen Sie mit anderen Produkten und diskutieren Sie im Team, woher die Werte kommen könnten und wie plausibel sie erscheinen.

+++

*Antwort:*

+++

#### Sichtweisen

+++

cradle-to gate: Rohstoff -> Produktion -> Distribution

+++

cradle-to-grave: Rohstoff -> Produktion -> Distribution -> Einkauf/NUtzung -> Entsorgung

+++

cradle-to-cradle

+++

- [ ] TODO Grafik erstellen

+++

https://www.energieagentur.nrw/klimaschutz/ccf-navi

+++

https://www.plattform-kvk.de

+++

## Maßnahmen zur CO$_2$-Reduzierung

+++

1. CO$_2$-Bilanz erstellen
2. Vermeiden
3. Reduzieren
4. Kompensieren

+++

Hauptverursacher aus CO$_2$-Bilanz ermitteln und entsprechende Maßnahmen auswählen, die darauf zielen

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
