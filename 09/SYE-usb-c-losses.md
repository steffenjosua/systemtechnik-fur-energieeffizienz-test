---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Nennenswerte Verluste: USB-C

+++

`{only} html [![DOI](https://zenodo.org/badge/DOI/10.0000/zenodo.0000000.svg)](https://doi.org/10.0000/zenodo.0000000)`

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen - auch im pdf!!

+++

- [ ] TODO Version xy auf zenodo hochladen, DOI erhalten und dann hier Zitat angeben

+++

- [ ] TODO in JOSE publizieren und dann ebenfalls Zitat hier angeben

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable

print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

- [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

+++

### Funktionen definieren

+++

## Lernziele

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
Das sollen Sie lernen:
    <ul>
        <li> was </li>
        <li> womit </li>
        <li> wozu </li>
    </ul>
</div>

+++

## Leitungsverluste berechnen

+++

- [ ] TODO kann man das als "Hintergrund" oder "Wiederholung der Theorie" taggen und entsprechend formatieren?

+++

Kabel beinhalten elektrische Leitungen. Die einfachste Variante besteht aus einer einzelnen Leitung (einadrige Kabel). Für den Anschluss an Steckdosen und standardisierte Buchsen beinhalten Kabel jedoch zumeist mindestens einen Hin- und einen Rückleiter (mind. zweiadrig), so dass der Stromkreis mit einem einzelnen Kabel geschlossen werden kann.

+++

Daraus resultiert der **Faktor 2 für Hin- und Rückleiter**.

```{code-cell} ipython3
twoway = 2 # 2 für Hin- und Rückleiter, 1 für einfache Leiter
```

Die meisten Leitungen werden zudem aus Kupfer hergestellt, dessen spezifische Leitfähigkeit typischerweise bei $\vartheta=20..25^\circ\text{C}$ einen Wert von $\kappa_{\text{Cu},20}=58\cdot10^6 \text{ S/m}$ hat {cite:p}`wikipedia_elektrische_2021`.

```{code-cell} ipython3
kappacu20 = 58e6 # S/m
```

Erwärmt sich eine Leitung *nicht nennenswert* während der Nutzung, kann man diesen Wert so annehmen wie er ist. Falls die Erwärmung nennenswert ist, verändert sich die Leitfähigkeit: Sie sinkt mit steigender Temperatur.

+++

Je nach gewünschter Genauigkeit und Temperaturbereich beschreibt eine lineare oder eine polynomische Kennlinie dieses Verhalten, gezeigt ist hier die lineare Kennlinie (für die polynomische bitte in der Literatur nachlesen):

+++

\begin{equation}
R(\vartheta)=R_{20}\cdot \left[1+\alpha_{20}\cdot\left(\vartheta-20^\circ\text{C}\right)\right] 
\end{equation}

+++

Der lineare Temperaturkoeffizient beträgt für Kupfer $\alpha_{\text{Cu},20}=3,93\cdot10^{-3}\text{ K}^{-1}$ {cite:p}`wikipedia_temperaturkoeffizient_2021`.

```{code-cell} ipython3
alphacu20 = 3.93e-3 # 1/K
```

Der Widerstand $R_{20}$ hängt zudem von der Geometrie der Leitung ab:

+++

\begin{equation}
R_{20} = \frac{1}{\kappa_{20}}\cdot \frac{l}{A}
\end{equation}

+++

Die Gleichung zeigt: je länger die Länge $l$ und je geringer der Querschnitt $A$ einer Leitung ist, desto höher ist der Widerstand $R_{20}$.

+++

Somit hat eine Leitung bei $\vartheta=50^\circ\text{C}$ einen um einen Faktor $a$ höheren Widerstand als bei $\vartheta=20^\circ\text{C}$:

```{code-cell} ipython3
rhocu20 = 1/kappacu20 # spezifischer Widerstand
T2 = 50 # höhere Temperatur
rhocu50 = rhocu20*(1+alphacu20*(T2-20))
a = rhocu50/rhocu20
print('Der Widerstand ist bei 50°C um den Faktor', str(np.round(a,2)), 'höher als bei 20°C.')
```

Die Leitungsverluste ergeben sich nun aus dem fließenden Strom $I$ und dem Leitungswiderstand:

+++

\begin{equation}
P_{\text{V, Leitung}} = 2\cdot \frac{1}{\kappa_{\text{Cu}, 20}}\cdot \frac{l}{A} \cdot I^2  
\end{equation}

+++

Den Nennstrom kann man aus Nennspannung $U_N$ und Nennleistung $P_N$ des Verbrauchers ermitteln. Genauer wird es, wenn man den Strom misst, da häufig andere Ströme fließen als der Nennstrom.

+++

\begin{equation}
I_N = P_N/U_N
\end{equation}

+++

Beispiel: ein 100 W USB-C-Netzteil bei einer Nennspannung von 5 Volt:

```{code-cell} ipython3
PN = 100 # W
UN = 5 # V
IN = PN/UN
print('Der Nennstrom des USB-C-Netzteils beträgt', str(IN), 'A.')
```

## Grenzen der Berechnung von Leitungsverlusten

+++

Wie heiß eine Leitung wird, lässt sich nicht so leicht berechnen, da die Wärmeabfuhr von der Beschaffenheit (Wärmeleitung, etc.) des Kabels abhängt und davon, wie viele stromführende (d.h. erwärmende) Adern darin sich gegenseitig erwärmen.

+++

Es gibt neben der Energieeffizienz auch weitere Gründe, diese Erwärmung zu begrenzen: 
- mechanische Verbindungen (Stecker, Buchsen, Leiterbahnen auf/in Leiterplatten) "arbeiten" bei Temperaturänderungen - sie dehnen sich bei Erwärmung aus und ziehen sich bei Abkühlung zusammen - dieser sog. Temperaturstress belastet mechanische Verbindungen und kann dazu führen, dass schließlich die elektrische Verbindung zu dünn wird (und dadurch noch heißer) oder ganz abreißt ($\to$ Defekt)
- Isoliermaterialien sollen nicht über Gebühr erwärmt werden, weil ihre elektrische Isolationswirkung ab einer zu hohen Temperatur abnimmt und weil die Gefahr, dass Elektrobrände entstehen, steigt.

+++

Für die unterschiedlichen Anwendungsgebiete (Gebäudetechnik, IT, etc.) existieren unterschiedliche zulässige Höchsttemperaturen (kurzzeitig erlaubte Temperaturen) und unterschiedliche zulässige Dauertemperaturen (dauerhaft zulässige Temperaturen).

+++

Deren Werte finden sich in zugehörigen Normen. Die VDE-Normen können Sie online (im VPN bzw. TH-Netz) über die Normenbibliothek finden.

+++

Eine mögliche Annahme, um Leitungsverluste genauer zu berechnen, ist es also, die zulässige Maximaltemperatur anzusetzen, um den Widerstandswert realistischer zu berechnen.

+++

## Literaturwerte für lange USB-C-Kabel

+++

Wie oben beschrieben, geht der Widerstandswert und damit die Verluste einer Leitung linear mit der Länge nach oben.

+++

{cite:p}`andrijan_mocker_fernbestromung_2021` zeigt Verluste von USB-C-Kabeln ab einer Länge von drei Metern. Unterschiedliche Fabrikate wiesen Verluste zwischen 2% und 38% auf. Dies zeigt, dass eine Messung sich lohnen kann!

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
