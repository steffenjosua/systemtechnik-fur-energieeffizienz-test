---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Energieeffiziente Software

+++

`{only} html [![DOI](https://zenodo.org/badge/DOI/10.0000/zenodo.0000000.svg)](https://doi.org/10.0000/zenodo.0000000)`

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen - auch im pdf!!

+++

- [ ] TODO Version xy auf zenodo hochladen, DOI erhalten und dann hier Zitat angeben

+++

- [ ] TODO in JOSE publizieren und dann ebenfalls Zitat hier angeben

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable


print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

- [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

+++

### Funktionen definieren

+++

## Lernziele

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
    <ul>
        <li> was </li>
        <li> womit </li>
        <li> wozu </li>
    </ul>
</div>

+++

## Was hat Software mit Energieeffizienz zu tun?

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Welche Apps auf Ihrem Smartphone sorgen dafür, dass Ihr Akku besonders schnell leer wird? Warum könnte das so sein?
</div>

+++

*Antwort:...*

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Sie sind auf Dienstreise und stellen fest, dass Sie für Ihr Dienst-Laptop kein Netzteil dabei haben. Wie nutzen Sie Ihr Gerät so, dass es möglichst lange durchhält? Welche Software ist besonders empfehlenswert?
</div>

+++

*Antwort:...*

+++

<div class="admonition important" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>AUFGABE</b></div>
Geben Sie Empfehlungen ab, die dazu führen, dass der ökologische Fußabdruck der IT-Nutzung minimiert wird.
</div>

+++

*Antwort:*...

+++

```{margin}
<div class="admonition seealso" style="background: #e9f6ec; padding: 10px">
<div class="title"><b>SIEHE AUCH:</b></div>
Zum Weiterlesen: {cite:p}`kern_sustainable_2018`
</div>
```

+++

## Label für energieeffiziente Software

+++

Seit 2019 gibt es ein Blauer Engel Label für Software {cite:p}`cornelius_schumacher_blue_2021`, im Januar 2021 noch ohne gelistete Software, die dieses Label erhalten hätten. Im Bereich Open Source Software arbeitet das KDE Projekt (eine Desktop-Oberfläche für Linux mit vielen windows-ähnlichen Applikationen) daran, dieses Label für einige seiner Applikationen zu erhalten {cite:p}`cornelius_schumacher_blue_2021`.

+++

Essentiell dafür ist es, Energieverbrauch von Software zu messen und zu bewerten {cite:p}`joseph_p_de_veaugh-geiss_be4foss_2021`. Dafür erstellt das Projekt *Nutzungsszenarien*, *Messanleitungen*, *automatisierte Testläufe* und publiziert *Messergebnisse* {cite:p}`cornelius_schumacher_feep_2021`.

+++

- [ ] TODO Grafik ähnlich fig. 4 aus {cite:p}`kern_sustainable_2018` (Link: https://www.umwelt-campus.de/fileadmin/Umwelt-Campus/Greensoft/1-s2.0-S0167739X17314188-main.pdf) hier in draw.io skizzieren

+++

Vorgaben des Blauen Engels zu energieeffizienter Software {cite:p}`ral_ggmbh_blue_2020`:
- *base load* - Grundlast
- *idle mode* - wenn das Gerät an ist, aber nicht verwendet wird und nichts aktiv tut: 
 - Prozessorauslastung (%)
 - Arbeitsspeicherauslastung (MByte)
 - dauerhafte Arbeitsspeicherauslastung (MByte/s)
 - durchschnittliche zusätzliche Bandbreitennutzung für Netzwerkzugang (Mbit/s)
 - durchschnittlicher elektrischer Leistungsbedarf (W)
- *Standard-Nutzungs-Szenario* - nach Richtlinien im o.g. Dokument definiert:
 - Prozessor-Nutzung (%$\cdot$s)
 - Arbeitsspeicher-Ausnutzung (MByte$\cdot$s)
 - dauerhafte Arbeitsspeicher-Nutzung (lesen + schreiben) (MByte/s$\cdot$s)
 - transferiertes Datenvolumen für Netzwerkzugriff (Mbit/s$\cdot$s)
 - durchschnittlicher Energiebedarf (Wh)
- Unterstützung des vorhandenen Energiemanagementsystems (EMS): 
 - Software darf nicht durch EMS gebremst werden (oder gar Daten verlieren)
 - Software darf in EMS nicht eingreifen
- keine Verkürzung der Hardware-Lebensdauer durch gestiegene Performanz-Anforderungen
- Rückwärts-Kompatibilität mindestens 5 Jahre
- Nutzer-Autonomie: 
 - interoperable Datenformate
 - offene Schnittstellen (APIs)
 - lange Zeit verwendbare Software (Kontinuität) mind. 5 Jahre
 - Deinstallierbarkeit
 - Offline Nutzbarkeit
 - Modularität (Möglichkeit, einzelne Komponenten zu deaktivieren)
 - Freiheit von Werbung
 - Dokumentation
- Energieverbrauch darf nicht um mehr als 10% steigen durch Updates

+++

Aufgrund der großen Auswahl an Desktop-Oberflächen, lässt sich Linux so konfigurieren, dass es auch stabil und gut mit älterer (weniger ressourcenhungriger) Hardware arbeitet. Dies ist eine Möglichkeit, Hardware weiter zu verwenden, die mit neueren Windows-Systemen und -Programmen bereits überfordert wäre.

+++

## Software-Energieverbrauch messen

+++

Der voltwerk Energielogger kommt mit seiner Messgenauigkeit an Grenzen, wenn es darum geht, Software zu vergleichen. 
- Minuten-Mittelwerte häufig zu grob
- kleine Leistungswerte (< 5 W) sehr ungenau

+++

Der Leistungsanalysator aus dem Praktikum elektrische Energie messen (LINK EINFÜGEN) ist sehr viel genauer.

+++

Weitere Alternativen {cite:p}`cornelius_schumacher_measurement_setupmd_2021`:
- GUDE Expert Power Control 1202 (misst Spannung, Strom, Frequenz, Phase, Wirkleistung, Blindleistung, Scheinleistung, Temperatur und Feuchte) mit [REST API](https://wiki.gude-systems.com/EPC_HTTP_Interface) zum automatischen Auslesen über LAN
- (kostengünstigen) Gosund SP111 WLAN Steckdosenadapter hacken {cite:p}`volker_krause_cheap_2020` - falls die gegebene Anleitung (noch) funktioniert)

```{code-cell} ipython3

```

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
