---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Fallstudie energieeffiziente IT

+++

Die Informations- und Kommunikationstechnik benötigt immer mehr Energie, da mit der zunehmenden Digitalisierung mehr Geräte und mehr Arbeiten mit IT erledigt werden.

+++

In diesem Kapitel geht es darum, welche Aspekte hierfür wichtig sind.
