---
jupytext:
  formats: md:myst,ipynb
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Sind Gleichstromnetze in Gebäuden klimafreundlicher?

+++

`{only} html [![DOI](https://zenodo.org/badge/DOI/10.0000/zenodo.0000000.svg)](https://doi.org/10.0000/zenodo.0000000)`

+++

- [ ] TODO Zitierweise, Danksagung Förderung einfügen - auch im pdf!!

+++

- [ ] TODO Version xy auf zenodo hochladen, DOI erhalten und dann hier Zitat angeben

+++

- [ ] TODO in JOSE publizieren und dann ebenfalls Zitat hier angeben

+++

## python konfigurieren

+++

### Module importieren

```{code-cell} ipython3
:tags: [hide-input]

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import datetime as dt
import holidays
import seaborn as sns
import plotly
import plotly.graph_objects as go
import sys
import os
import locale
from distutils.spawn import find_executable


print('Versionen der verwendeten python-Module: ')
print('numpy', np.__version__)
print('matplotlib', matplotlib.__version__)
print('pandas', pd.__version__)
print('datetime', dt)
print('holidays', holidays.__version__)
print('seaborn', sns.__version__)
print('plotly', plotly.__version__)
print('sys', sys.version)
print('os', os)
print('locale', locale)
```

### Grafikparameter einstellen

```{code-cell} ipython3
:tags: [hide-input]

plt.rcParams['savefig.dpi'] = 75
plt.rcParams['figure.autolayout'] = False
plt.rcParams['figure.figsize'] = 10, 6
plt.rcParams['axes.labelsize'] = 18
plt.rcParams['axes.titlesize'] = 20
plt.rcParams['font.size'] = 18
plt.rcParams['lines.linewidth'] = 2.0
plt.rcParams['lines.markersize'] = 8
plt.rcParams['legend.fontsize'] = 18
locale.setlocale(locale.LC_ALL, '')
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16

if find_executable('latex'):
    plt.rcParams['text.usetex'] = True
    pd.set_option('display.latex.repr', True)
    pd.set_option('display.latex.longtable', True)
```

- [ ] TODO am Anfang immer die gleichen Layoutparameter laden -> in allen notebooks aktualisieren

+++

### Funktionen definieren

+++

## Lernziele

+++

<div class="admonition note" style="background: #e5f1ff; padding: 10px">
<div class="title"><b>LERNZIELE</b></div>
Das sollen Sie lernen:
    <ul>
        <li> was </li>
        <li> womit </li>
        <li> wozu </li>
    </ul>
</div>

+++

## *microgrids* mit integrierter erneuerbarer Erzeugung

+++

*Definition*: ein *microgrid* mit integrierter erneuerbarer Erzeugung umfasst einen Netzabschnitt zur Versorgung eines oder mehrerer in räumlicher Nähe gelegener Gebäude sowie erneuerbare Erzeugung. Manche microgrids lassen sich netzunabhängig betreiben. Stand der Technik ist es, microgrids mit Wechselstrom zu betreiben (230 V bzw. 400 V). Forschungsdemonstratoren zeigen, dass auch Gleichstrom für solche microgrids nutzbar ist.

+++

## Klimawirkung von Gleichstrom- vs. Wechselstrom-*microgrids*

+++

{cite:p}`kockel_reducing_2019` hat dies für Bürogebäude mit Photovoltaik an Fassade und Dach verglichen.

+++

Dadurch, dass Netzteile nun nicht mehr von Wechselspannung auf Gleichspannung umsetzen müssen, sondern nur noch von einem Gleichspannungniveau auf ein anderes, spart man Komponenten ein. Dies geschieht analog bei den Frequenzumrichtern für die Lüftermotoren, die durch Wechselrichter ersetzt werden können. {cite:p}`kockel_reducing_2019` ermittelt damit neben einer Reduzierung der herstellungsrelevanten Treibhausgase auch eine erhebliche Verbesserung der Ressourceneffizienz durch Materialeinsparungen.

+++

Auch die photovoltaische Energieerzeugung verbessert deutlich den Fußabdruck der Nutzenergie über eine angenommene Lebensdauer in einem Beispielsystem:

```{code-cell} ipython3
:tags: [hide-input]

energy_from_public_grid = 125 # t CO2 eq
removed_components = 37 # t CO2 eq
additional_components = -4 # t CO2 eq
total = energy_from_public_grid+removed_components+additional_components
labels = ('Netzbezug', 'eingesparte Komponenten', 'zusätzliche Komponenten für Gleichstrom', 'zusammen')
y_pos = np.arange(len(labels))
co2eq = [energy_from_public_grid, removed_components, additional_components, total]
plt.bar(y_pos, co2eq, align='center', alpha=0.5)
plt.xticks(y_pos,labels,rotation=90)
plt.ylabel('Einsparung [t CO$_2$ Äquivalent]')
plt.title('Beispiel Gleichstrom microgrid mit 250 Computern in einem Bürogebäude\nZahlenwerte: Kockel et.al. 2019')
```

Ökonomisch sieht dies aktuell noch anders aus, da Gleichstrom-*microgrids* nicht sehr verbreitet sind. Dadurch sind Komponenten teurer, weil die Stückzahl nicht so hoch ist - obwohl sie weniger Material benötigen.

+++

Zu beachten ist zudem, dass die Installateure für Gleichstrom-*microgrids* speziell geschult sein müssen. Da sie ohnehin für Photovoltaik und Elektromobilität inzwischen mehr mit Gleichstrom zu tun haben, erscheint dies kein weiter Weg mehr zu sein.

+++

## Literatur

+++

```{bibliography}
:filter: docname in docnames
```
