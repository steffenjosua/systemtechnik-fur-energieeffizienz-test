---
jupytext:
  formats: ipynb,md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.10.3
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Anforderungen und Systemgrenzen definieren

+++

Dieses Kapitel beschreibt detaillierter, worauf es bei der Klärung von Anforderungen ankommt und wie man sie so dokumentiert, dass man rasch sehen kann, wie vertrauenswürdig und plausibel die zugehörigen Daten sind. Sie können dies dann direkt beginnen, auf Ihre Projekte anzuwenden.
